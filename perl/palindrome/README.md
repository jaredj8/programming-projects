# Palindromes
- ### Program Overview
    - takes input from user
    - puts each character in an array
    - reverses the array into new array
    - matches the characters in each index

- ### Program screenshots 
    - #### Terminal Run
        - ![program](output.png)