#!/usr/bin/env perl
use strict;
use warnings;


my $flag=1;

do {
	my $input="";
	my $reverseInput="";
	my @reverseArray=();
	my @charArray=();
	my $inputLength=0;
	my $j=0;

	print "Please input a string: ";
	chomp($input = <STDIN>);


	if($input eq "quit"){
		$flag=0;
	}else{
		$inputLength = length($input)-1;
		my $toLower = lc($input);


		@charArray=split("",$toLower);

		for(my $i=$inputLength; $i>=0; $i--){
			$reverseArray[$j]=$charArray[$i];
			$j++;
		}

		for(my $i=0; $i<=$inputLength; $i++){
			$reverseInput.=$reverseArray[$i];
		}


		if($toLower eq $reverseInput){
			print "\"$input\" is a palindrome.";
			print "\n";
		}else{
			print "\"$input\" is not a palindrome.";
			print "\n";
		}
	}
	
}while($flag != 0);