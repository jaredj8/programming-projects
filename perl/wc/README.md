# Statistics of a file
- ### Program Overview
    - finds amount of characters, words and lines
    - use of arrays and loops
    - looping through array to find matches 

- ### Program screenshots 
    - #### Input Text
        - ![Input](input.png)
    - #### Output
        - ![Output](output.png)