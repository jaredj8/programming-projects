#!/usr/bin/env perl
use strict;
use warnings;

my $i=0;
my $wordString="";
my @finalArray=();

my $test=$ARGV[0];
open(FILE, $test) or die "could not open";

my($C, $W, $L)=(0,0,0);

while(<FILE>){
	$L++;
	$C+=length();
	$W+=scalar(split(/\s+/, $_));
	$wordString.=join(" ", split(/\s+/, $_));
	$wordString.=" ";
}

my @stringArray=split(/\s+/,$wordString);

#create a counter variable to keep track of how many occurences ////////
#take one word and match to all of words 
for(my $j=0; $j < $W; $j++){
	my $counter=0;
	for(my $k=0; $k<$W; $k++){
		if($stringArray[$j] eq $stringArray[$k]){
			$counter++;
			#if counter goes above one that means there is more than one occurence
			$finalArray[$j]=$stringArray[$j].": ".$counter;

		}
	}
}

my @sortedArray=sort @finalArray;


#if match is found ++ to the counter var and add a colon to the end of the word
#else add a colon and and put the var down 

print "Number of characters: ".$C."\n";
print "Number of words: ".$W."\n";
print "Number of lines: ".$L."\n";
print "Frequency of words in the file:\n";
print "------------------------------\n";

foreach(@sortedArray){
	print $_."\n";
}