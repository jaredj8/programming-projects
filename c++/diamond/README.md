# Creation of diamond application
- #### Started with creating a header file to store my declarations
    - For this project I wanted to be able to manipulate the diamond as I pleased
    - I created setters to change the data of the diamond and getters to retrieve the data of the diamond
    - ![Diamond header file](../img/diamondImg/diamondHeader.png)  
- #### I then created my definitions in my diamond cpp file  
    - | Diamond.cpp 1 | Diamond.cpp 2 | Diamond.cpp 3 |
        | ----------- | ----------- | ------------- |
        | [![diamond](../img/diamondImg/diamondCpp1.png)]( ../img/diamondImg/diamondCpp1.png ) | [![diamond](../img/diamondImg/diamondCpp2.png)](../img/diamondImg/diamondCpp2.png) | [![diamond](../img/diamondImg/diamondCpp3.png)](../img/diamondImg/diamondCpp3.png) |
- #### Finally I created my main file to put it all together
    - | Main.cpp 1 | Main.cpp 2 |
    | ----------- | ----------- |
    | [![diamond](../img/diamondImg/mainCpp1.png)]( ../img/diamondImg/mainCpp1.png ) | [![diamond](../img/diamondImg/mainCpp2.png)](../img/diamondImg/mainCpp2.png) | 
- #### Test run with previous code and modified code
    | Terminal runs |                  |
       | --------------------- | ------------ |
       | Previous code | ![previous](../img/diamondImg/diamond.png) |
       |                            |                       |
       |                            |                       |
       |                            |                       |
       | Modified code | ![modified](../img/diamondImg/modified1.png) ![modified](../img/diamondImg/modified2.png) |
       




