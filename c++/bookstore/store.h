//NAME - Jared Jaskolski
//SECTION - 3
#include <iostream>
#include <cstring>
#include <cctype>
#include "book.h"

using namespace std;

class Store{

friend ostream& operator<< (ostream& s, const Book& b);
friend double SetAmount(Store& s, double& p);

public:
	Store();
	~Store();

	void NewBook(char* t, char* a, Genre h, double &p);
	void SearchAuthor(char* a);
	int SearchTitle(char* t);
	void SearchGenre(Genre g);
	void Sell(char* t);

	int ReturnSearchTitle(char* t) const;
	void PrintHeader(int i) const;
	void Display() const;
	void Exit() const;


private:
	double amount;
	Book* bookList;
	int maxSize, currentSize;
	int books;
	double cost;
  	int matches;
  	void Shrink();
  	void Grow();
 
};