# Projects by language
- ### Python
    - [Sort list of words by their vowels](/python/README.md)  
    - [HTML Security+ Test Parser](/python/htmlParse/README.md)
    
- ### C++
    - [Basics of C++ OOP](/c++/diamond/README.md)
    - [Bookstore OOP](/c++/bookstore/README.md)
    - [Student Database](/c++/student/README.md)

- ### C\#/MySql/.NET Core
    - [Pet Store System](csharp/petBack/README.md)
    - [Pet Store System (Lucky Whiskers)](csharp/PetFront/README.md)
    
- ### Java 
    - [Dungeon Mayhem / Game Recreation](java/dungeonMayhem/README.md)
    - [Simple Temperature Conversion](java/tempConversion/README.md)
    - [Simple Number Array](java/numArray/README.md)
    - [Petstore w/ MySql](java/petstore/README.md)
    - [Tic-Tac-Toe](java/ticTacToe/README.md)
    
- ### HTML/CSS/JavaScript
    - [ShoppingTally](https://shoppingtally.com)
    - [My Portfolio](https://jjaskolski.com)  

- ### Android Studio / Java
    - [Simple text view and button programming](/android/androidBasics/README.md)
    - [DASH/AGILE](/android/dash/README.md)

- ### PERL
    - [Testing for palindromes](perl/palindrome/README.md)
    - [Calculating counts on a file](perl/wc/README.md) 

- ### BASH
    - [Rename files using a script](/bash/README.md)

- ### Angular
    - [REST API StockApp](/csharp/StockApp/README.md)

# Other languages, frameworks, libraries used w/o projects
- #### Swift 
- #### SED
- #### AWK
- #### Node
- #### Express
- #### React
- #### PHP

# Database knowledge
- #### MYSQL
- #### Oracle
- #### MS Server

# Certificates
- #### JIRA
- #### Cloud Computing
- #### Selenium
- #### DevOps Foundations
- #### Scrum
- #### Java Data Structures / Algorithms 
- #### Cloud Migration   
