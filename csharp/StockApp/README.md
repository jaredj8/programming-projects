# ANGULAR / RXJS / REST API 
- ### Project Details
    - use of observables and operators
    - bootstrap and css inclusion
    - use of angular services

- ### 1st API Call
    - ![apiCall1](img/apiCall1.png)

- ### 2nd API Call
    - ![apiCall2](img/apiCall2.png)
    

