export interface Result {
  T: string
  v: number
  vw?: number
  o: number
  c: number
  h: number
  l: number
  t: number
  n?: number
}
