import { Result } from './resultInterface';

export interface ApiInterface {
  queryCount: number
  resultsCount: number
  adjusted: boolean
  results: Result[]
  status: string
  request_id: string
  count: number
}
