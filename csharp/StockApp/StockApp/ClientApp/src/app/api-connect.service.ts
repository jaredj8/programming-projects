import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiInterface } from './interface';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Result } from './resultInterface';


@Injectable({
  providedIn: 'root'
})
export class ApiConnectService {
  stock: String = "";
  url: string = "https://api.polygon.io/v2/aggs/grouped/locale/us/market/stocks/2020-10-14?adjusted=true&apiKey=_IX6f0zpRhzOfWDvVTup0xoyil5IzGkV";
  constructor(private http: HttpClient) {
    
  }

  getData(): Observable<Result[]>{
    return this.http.get<ApiInterface>(this.url).pipe(map((e) => e.results.filter((e) => e.T === this.stock)));
  }

  setStock(data:string) {
    this.stock = data;
  }
}
