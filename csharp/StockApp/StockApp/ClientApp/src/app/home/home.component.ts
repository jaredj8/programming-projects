import { Component, OnInit } from '@angular/core';
import { ApiConnectService } from '../api-connect.service';
import { ApiInterface } from '../interface';
import { Result } from '../resultInterface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  test: Result[];
  stock: string;
  constructor(private service: ApiConnectService) {
   
  }


  sendInput(data: string) {
    this.service.setStock(data);
    this.service.getData().subscribe(
      (data: Result[]) => this.test = data,
      (error: any) => console.log(error),
      () => console.log("Done")
    );
  }

  

  ngOnInit() {
    
  }

  ngAfterViewInit() {
    
  }

}
