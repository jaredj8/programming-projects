# Lucky Whiskers Website - .NET Core/C\#
- ### Project Timeline
    - 2 days

- ### Project Details
    - MVC file layout
    - bootstrap and pure css inclusion
    - sql server connected
    - .NET Core 6
    - home page layout finished but a rough draft

- ### Future Updates
    - pets page controller
    - purchase pets
    - sort available animals by breed and type
    - create a login system
    - user settings and past purchases
    - setup services
    - company reviews / testimonials

- ### Homepage Layout / Thoughts
    - #### Home
        ![home](media/home.png)
        - inviting feel from video
        - clean navbar with logo
        - slogan to draw in customer
    - #### Meet Some Whiskers
        ![meet](media/meet.png)
        - showcase animals
        - more of an inviting feel
        - good flow with the page
        - videos show the animal
    - #### Services
        ![service](media/services.png)
        - showcase any services for marketing
        - card deck animation
        - hover over causes card to rise with bigger shadow
        - card images will be changed to something relevant
    - #### Staff
        ![staff](media/staff.png)
        - provide info on staff and love for animals
        - make it a family of animal lovers 
        - possible chance for customer compliments on employees in the future
    - #### Message
        ![message](media/message.png)
        - chance for customers to give feedback
- ### Bootstrap Responsive Page
    - #### Small Page
    ![small](media/bootSm.png)
    - #### Large Page
    ![large](media/bootLg.png)
- ### Homepage HTML
    [html](PetFront/Views/Home/Index.cshtml)
