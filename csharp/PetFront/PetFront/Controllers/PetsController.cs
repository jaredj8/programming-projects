﻿using Microsoft.AspNetCore.Mvc;


namespace PetFront.Controllers.Pets
{
    public class PetsController : Controller
    {
        private readonly PetDbContext _context;

        public PetsController(PetDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            var data = _context.Pets.ToList();
            return View(data);
        }
    }
}
