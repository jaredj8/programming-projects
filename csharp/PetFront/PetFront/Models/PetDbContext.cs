﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using PetFront.Models;

namespace PetFront
{
    public partial class PetDbContext : DbContext
    {
        public PetDbContext()
        {
        }

        public PetDbContext(DbContextOptions<PetDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AdoptionAgency> AdoptionAgencies { get; set; } = null!;
        public virtual DbSet<Person> People { get; set; } = null!;
        public virtual DbSet<Pet> Pets { get; set; } = null!;
        public virtual DbSet<PetFavorite> PetFavorites { get; set; } = null!;
        public virtual DbSet<PetHist> PetHists { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=eTickets;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdoptionAgency>(entity =>
            {
                entity.Property(e => e.AgyState).IsFixedLength();

                entity.Property(e => e.PetId).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Pet)
                    .WithMany()
                    .HasForeignKey(d => d.PetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("adoption_agency$adoption_agency_ibfk_1");
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.HasKey(e => e.Pid)
                    .HasName("PK_people_pid");
            });

            modelBuilder.Entity<Pet>(entity =>
            {
                entity.HasOne(d => d.PidNavigation)
                    .WithMany(p => p.Pets)
                    .HasForeignKey(d => d.Pid)
                    .HasConstraintName("pet$pet_ibfk_1");
            });

            modelBuilder.Entity<PetFavorite>(entity =>
            {
                entity.HasOne(d => d.Pet)
                    .WithMany()
                    .HasForeignKey(d => d.PetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("pet_favorites$pet_favorites_ibfk_2");

                entity.HasOne(d => d.PidNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.Pid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("pet_favorites$pet_favorites_ibfk_1");
            });

            modelBuilder.Entity<PetHist>(entity =>
            {
                entity.Property(e => e.PetId).ValueGeneratedOnAdd();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
