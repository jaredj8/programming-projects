﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PetFront.Models
{
    [Keyless]
    [Table("adoption_agency", Schema = "person")]
    [Index("PetId", Name = "pet_id")]
    public partial class AdoptionAgency
    {
        [Column("pet_id")]
        public int PetId { get; set; }
        [Column("agy_address")]
        [StringLength(30)]
        public string AgyAddress { get; set; } = null!;
        [Column("agy_city")]
        [StringLength(30)]
        public string AgyCity { get; set; } = null!;
        [Column("agy_state")]
        [StringLength(2)]
        public string AgyState { get; set; } = null!;
        [Column("agy_zip")]
        public int AgyZip { get; set; }
        [Column("agy_phone")]
        public long AgyPhone { get; set; }

        [ForeignKey("PetId")]
        public virtual Pet Pet { get; set; } = null!;
    }
}
