﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PetFront.Models
{
    [Keyless]
    [Table("pet_hist", Schema = "person")]
    [Index("PetId", Name = "pet_id")]
    public partial class PetHist
    {
        [Column("pet_id")]
        public int PetId { get; set; }
        [Column("ph_age_placed")]
        public int PhAgePlaced { get; set; }
        [Column("ph_date_placed", TypeName = "date")]
        public DateTime PhDatePlaced { get; set; }
        [Column("ph_date_adopted", TypeName = "date")]
        public DateTime? PhDateAdopted { get; set; }
        [Column("ph_weight")]
        public int PhWeight { get; set; }
        [Column("ph_heartworm_med")]
        [StringLength(1)]
        public string PhHeartwormMed { get; set; } = null!;
        [Column("ph_rabies_med")]
        [StringLength(1)]
        public string PhRabiesMed { get; set; } = null!;
        [Column("ph_aggression")]
        public int PhAggression { get; set; }
        [Column("ph_chip_id")]
        public long? PhChipId { get; set; }
    }
}
