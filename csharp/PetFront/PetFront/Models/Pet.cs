﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PetFront.Models
{
    [Table("pet", Schema = "person")]
    [Index("Pid", Name = "pid_fk")]
    public partial class Pet
    {
        [Key]
        [Column("pet_id")]
        public int PetId { get; set; }
        [Column("pet_type")]
        [StringLength(10)]
        public string PetType { get; set; } = null!;
        [Column("pet_breed")]
        [StringLength(20)]
        public string PetBreed { get; set; } = null!;
        [Column("pet_name")]
        [StringLength(20)]
        public string PetName { get; set; } = null!;
        [Column("pid")]
        public int? Pid { get; set; }

        [ForeignKey("Pid")]
        [InverseProperty("Pets")]
        public virtual Person? PidNavigation { get; set; }
    }
}
