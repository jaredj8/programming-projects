﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PetFront.Models
{
    [Table("people", Schema = "person")]
    public partial class Person
    {
        public Person()
        {
            Pets = new HashSet<Pet>();
        }

        [Key]
        [Column("pid")]
        public int Pid { get; set; }
        [Column("fname")]
        [StringLength(30)]
        public string Fname { get; set; } = null!;
        [Column("lname")]
        [StringLength(30)]
        public string Lname { get; set; } = null!;
        [Column("university")]
        [StringLength(30)]
        public string University { get; set; } = null!;
        [Column("age")]
        public int Age { get; set; }
        [Column("major")]
        [StringLength(20)]
        public string? Major { get; set; }
        [Column("position")]
        [StringLength(20)]
        public string? Position { get; set; }
        [Column("phone")]
        public long Phone { get; set; }
        [Column("access")]
        [StringLength(1)]
        public string Access { get; set; } = null!;

        [InverseProperty("PidNavigation")]
        public virtual ICollection<Pet> Pets { get; set; }
    }
}
