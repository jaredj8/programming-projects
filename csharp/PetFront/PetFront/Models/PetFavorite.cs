﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PetFront.Models
{
    [Keyless]
    [Table("pet_favorites", Schema = "person")]
    [Index("PetId", Name = "pet_id")]
    [Index("Pid", Name = "pid")]
    public partial class PetFavorite
    {
        [Column("pid")]
        public int Pid { get; set; }
        [Column("pet_id")]
        public int PetId { get; set; }
        [Column("availability")]
        public int Availability { get; set; }

        [ForeignKey("PetId")]
        public virtual Pet Pet { get; set; } = null!;
        [ForeignKey("Pid")]
        public virtual Person PidNavigation { get; set; } = null!;
    }
}
