import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './home/nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { UserListComponent } from './home/user-list/user-list.component';
import { MainContainerComponent } from './home/main-container/main-container.component';
import { NavMenuLinkComponent } from './home/nav-menu/nav-menu-link/nav-menu-link.component';
import { ListComponent } from './list/list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TestimonialsComponent } from './home/testimonials/testimonials.component';
import { WhyShoppingTallyComponent } from './home/why-shopping-tally/why-shopping-tally.component';
import { HoverDirective } from './hover.directive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HowItWorksComponent } from './home/how-it-works/how-it-works.component';



@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    UserListComponent,
    MainContainerComponent,
    NavMenuLinkComponent,
    ListComponent,
    TestimonialsComponent,
    WhyShoppingTallyComponent,
    HoverDirective,
    HowItWorksComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'list', component: ListComponent, pathMatch: 'full' }
    ]),
    BrowserAnimationsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
