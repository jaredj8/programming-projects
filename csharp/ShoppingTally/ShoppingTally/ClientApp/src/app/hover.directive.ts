import { Directive, ElementRef, EventEmitter, HostBinding, HostListener, Output, Renderer2 } from '@angular/core';
@Directive({
  selector: '[appHover]'
})
export class HoverDirective {
  @Output() onHover: EventEmitter<boolean> = new EventEmitter<false>();
  constructor(private el: ElementRef, private renderer: Renderer2) { }

  @HostListener("mouseover", ["$event"])
  onListenOver(event): void {
    this.onHover.emit(true)
  }

  @HostListener("mouseout", ["$event"])
  onListenOut(event): void {
    this.onHover.emit(false)
  }

  
}
