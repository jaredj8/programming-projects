import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Component, OnInit } from '@angular/core';
import { NavMenuLinkService } from './nav-menu-link/nav-menu-link.service';
import {Links} from './nav-menu-link/links'




@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
  
})
export class NavMenuComponent {
  route: string = "";
  links: Links[] = [];
  constructor(private service: NavMenuLinkService) {
    this.service.getCurrentRoute().subscribe(res => this.route = res.urlAfterRedirects.toString());
    
  }

  ngOnInit(): void {
    this.links = this.service.getCurrentLinks(this.route);
   

  }

  isExpanded = false;

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}


