import { AfterViewInit, Component,ElementRef,HostListener,Input, OnInit, Renderer2, ViewChild} from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Links } from './links';






@Component({
  selector: 'app-nav-menu-link',
  templateUrl: './nav-menu-link.component.html',
  styleUrls: ['./nav-menu-link.component.css'],
  animations: [
    trigger('test', [
      state('noSee', style({'transform': 'scaleX(0)'})),
      state('see', style({ 'transform': 'scaleX(1)' })),
      transition('see <=> noSee', [
        animate(150)
      ])
    ])
  ]
})
export class NavMenuLinkComponent implements AfterViewInit {
  @Input() hello: Links[] = [];
  constructor() {

  }
  ngAfterViewInit(): void {

  }

  ngOnInit(): void {

  }

  test(index: number) {
    this.hello[index].state = true;
  }

  test2(index: number) {
    this.hello[index].state = false;
  }

}
