import { Injectable } from '@angular/core';
import { Router, NavigationEnd} from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators'
import { Links } from './links'
@Injectable({
  providedIn: 'root'
})
export class NavMenuLinkService {
  homeLinks: Links[] = [{ linkName: "Why Shopping Tally?", state: false }, { linkName: "Testimonials", state: false }, { linkName: "How it works", state: false }, { linkName: "What's new", state: false }, { linkName: "Available Stores", state: false }, { linkName: "Weekly BOGOS", state: false }, { linkName: "About", state: false }, { linkName: "Contact", state: false }, {linkName: "Payment", state:false}];
  listLinks: Links[] = [{ linkName: "Create a list", state: false }, { linkName: "Review past list", state:false}];
  scheduleLinks: Links[] = [{ linkName: "Check schedule", state: false }, { linkName: "Leave a message",state:false}];
  response: string;

  constructor(private router: Router) {
    this.response = "";
  }



  getCurrentRoute():Observable<NavigationEnd>{
    return this.router.events.pipe( filter((event): event is NavigationEnd => event instanceof NavigationEnd))  
  }

  getCurrentLinks(route:string):Links[]{
    switch (route) {
      case "/": {
        return this.homeLinks;
        break;
      }
      case "/list": {
        return this.listLinks;
        break;
      }
      case "/schedule": {
        return this.scheduleLinks;
        break;
      }
    }
    return [];
  }

}
