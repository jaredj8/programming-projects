import { TestBed } from '@angular/core/testing';

import { NavMenuLinkService } from './nav-menu-link.service';

describe('NavMenuLinkService', () => {
  let service: NavMenuLinkService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavMenuLinkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
