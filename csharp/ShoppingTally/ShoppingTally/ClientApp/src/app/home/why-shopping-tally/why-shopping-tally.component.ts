import { Component, Directive, ElementRef, HostListener, OnInit, Renderer2, ViewChild, AfterViewInit, QueryList, ViewChildren} from '@angular/core';
import AOS from "aos";
import { WhyShoppingTallyService } from './why-shopping-tally.service';
import { Why } from './why-class';
import { NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-why-shopping-tally',
  templateUrl: './why-shopping-tally.component.html',
  styleUrls: ['./why-shopping-tally.component.css']
})
export class WhyShoppingTallyComponent implements OnInit {
  pauseOnHover = true;
  

  @ViewChild('carousel', { static: true }) carousel: NgbCarousel;

 

  descriptions: Why[] = [];
  constructor(private service: WhyShoppingTallyService, private el: ElementRef, private renderer: Renderer2) {
   
  }


  ngOnInit(): void {
    AOS.init();
    this.descriptions = this.service.getDescriptions();

  }

  

}



