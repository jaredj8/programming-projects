import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WhyShoppingTallyComponent } from './why-shopping-tally.component';

describe('WhyShoppingTallyComponent', () => {
  let component: WhyShoppingTallyComponent;
  let fixture: ComponentFixture<WhyShoppingTallyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WhyShoppingTallyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WhyShoppingTallyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
