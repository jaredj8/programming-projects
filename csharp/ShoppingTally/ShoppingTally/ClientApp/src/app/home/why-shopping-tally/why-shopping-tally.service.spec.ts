import { TestBed } from '@angular/core/testing';

import { WhyShoppingTallyService } from './why-shopping-tally.service';

describe('WhyShoppingTallyService', () => {
  let service: WhyShoppingTallyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WhyShoppingTallyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
