import { Injectable } from '@angular/core';
import { Why } from './why-class';

@Injectable({
  providedIn: 'root'
})
export class WhyShoppingTallyService {
  descriptions: Why[] = [
    {
      image: "../../assets/img/facebook.png",
      description: "My pricing is on average 8 to 12% less expensive than the national competition."
    },
    {
      image: "test",
      description: "I do NOT charge an Annual Fee."
    },
    {
      image: "test",
      description: "You can order alcohol on the same list without having to do a separate store request."
    },
    {
      image: "test",
      description: "National competition does not honor all Bogo's that are in the store."
    },
    {
      image: "test",
      description: "The competition doesn't have everything available in the store due to a limited online inventory."
    },
    {
      image: "test",
      description: "You will always have the same safe and trustworthy shopper with my five star rated experience."
    }
  ];
  constructor() {
    
  }

  getDescriptions(): Why[] {
    return this.descriptions;
  }



}
