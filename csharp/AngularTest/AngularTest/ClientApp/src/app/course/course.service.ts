import { Observable } from 'rxjs';
import { Task } from './course.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  apiUrl = 'http://localhost:5000/tasks'

  constructor(private http: HttpClient) { }

  getCourse(): Observable<Task[]> {
    return this.http.get<Task[]>(this.apiUrl);
  }
}
