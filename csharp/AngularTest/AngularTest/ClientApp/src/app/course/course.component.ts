import { Component, OnInit } from '@angular/core';
import { CourseService } from './course.service';
import { Task } from './course.interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  title = "List of courses";
  tasks: Task[] = [];
  constructor(private service: CourseService) {
   
  }

  ngOnInit(): void {
    this.service.getCourse()
      .pipe(map(tasks => tasks.filter(task => task.id == 1)))
      .subscribe((obs) => this.tasks = obs);
  }

}
