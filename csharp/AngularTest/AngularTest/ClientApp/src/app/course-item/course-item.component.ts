import { Component, Input, OnInit } from '@angular/core';
import { Task } from '../course/course.interface';
@Component({
  selector: 'app-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.css']
})
export class CourseItemComponent implements OnInit {
  @Input() taskB: Task;
  constructor() { }

  ngOnInit(): void {
  }

  doSomething() {
    alert("hello");
  }

}
