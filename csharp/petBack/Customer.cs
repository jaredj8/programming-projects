using System;
using MySql.Data.MySqlClient;


namespace Intro
{
    class Customer{

        //existing customer constructor
        public Customer(int p, string n){
            pid = p;
            fname = n;
        }

        //new customer constructor
        public Customer(){
            fname = "";
            lname = "";
            university = "";
            age = "";
            major = "";
            position = "";

        }

        public void createCustomer(ref MySqlCommand command){
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "insert into people (fname,lname,university,age,major,position,phone,access) VALUES " +
                                "('" + fname + "', '" + lname + "', '" + university + "', " + ageToInt + ", '" + major + "', '" + position + "', " + phoneNum + ", '" + access + "')";
            MySqlDataReader reader = command.ExecuteReader();
            reader.Close();

            populatePID(command);
        }

        public void populatePID(MySqlCommand command){
            command.CommandText = "select pid from people where fname = '" + fname + "' and lname = '" + lname + "'";
            MySqlDataReader reader = command.ExecuteReader();
            if(reader.HasRows){
                while(reader.Read()){
                    pid = reader.GetInt32(0);
                }
            }else{
                Console.WriteLine("empty");
            }
            reader.Close();
        }

        public void customerMenu(){
            Console.WriteLine("\nHello, " + fname + "! What would you like to do today?\n");
            Console.WriteLine("1.) Show available animals.");
            Console.WriteLine("2.) Purchase an animal.");
            Console.WriteLine("3.) Show animal history.");
            Console.WriteLine("4.) Sort available animals by type.");
            Console.WriteLine("5.) Sort available animals by breed.");
            Console.WriteLine("6.) Show previous shelter agency.");
            Console.WriteLine("7.) Show favorited animals and their availability.");
            Console.WriteLine("8.) Show full menu.");
            Console.WriteLine("9.) Exit.\n");
        }

        public int getCustomerChoice(ref MySqlCommand command){
            string choice = "";
            choiceToInt = 0;
            Console.Write("Choose your option: ");
            choice = Console.ReadLine();
            choiceToInt = Int16.Parse(choice);

            switch(choiceToInt){
                case 1: showAnimals(command);
                break;
                // case 2: purchaseAnimal();
                // break;
                case 3: showAnimalHist(command);
                break;
                case 4: sortAnimalType(command);
                break;
                case 5: sortAnimalBreed(command);
                break;
                // case 6: showPreviousAgency();
                // break;
                case 7: showFavoritedAnimals(command);
                break;
                // case 8: customerMenu();
                // break;

            }
            
            return choiceToInt;
        }

        public void showFavoritedAnimals(MySqlCommand command){
            //get the count of animals that are favorited with the current pid 
            command.CommandText = "select count(*) from pet_favorites where pid = " + pid;
            MySqlDataReader reader = command.ExecuteReader();
            int count = 0;
            if(reader.HasRows){
                while(reader.Read()){
                    count = reader.GetInt32(0);
                }
            }
            reader.Close();

            //store pet_id's in array
            int[] data = new int[]{};
            command.CommandText = "select pet_id from pet_favorites where pid = " + pid;
            reader = command.ExecuteReader();
            if(reader.HasRows){
                while(reader.Read()){
                    data = data.Concat(new int[] {reader.GetInt32(0)}).ToArray();
                }
            }
            reader.Close();
            
            //show animal if it is favorited and available
            command.CommandText = "SELECT p.pet_id,p.pet_breed,p.pet_type,p.pet_name from pet p,pet_favorites pf where p.pet_id = pf.pet_id and pf.availability = 1 and pf.pid = " + pid;
            reader = command.ExecuteReader();
            string displayResults = "[ID]\t[BREED]\t[TYPE]\t[NAME]\n";
            if(reader.HasRows){
                while(reader.Read()){
                    displayResults += reader.GetInt32(0) + "\t" + reader.GetString(1) + "\t" + reader.GetString(2) + "\t" + reader.GetString(3) + Environment.NewLine;
                }
                Console.WriteLine(displayResults);
            }
            reader.Close();
        }
        public void saveAnimalLikes(MySqlCommand command){
            int flag = 1;
            
            do{
                //get user option to add animal
                Console.Write("Would you like to save an animal from the list? (y / n): ");
                string saveAnswer = Console.ReadLine();

                if(saveAnswer == "y"){
                    Console.Write("Select the animal ID you would like to save: ");
                    int petId = Int32.Parse(Console.ReadLine());
                    command.CommandText = "insert into pet_favorites(pid,pet_id,availability) values ('" + pid + "'" + ",'" + petId + "', 1)";
                    MySqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                }else if(saveAnswer == "n"){
                    flag = 0;
                }
            }while(flag == 1);
            
            
        }

        public void sortAnimalBreed(MySqlCommand command){
            Console.Write("What animal breed are you looking for?: ");
            string breed = Console.ReadLine();
            command.CommandText = "SELECT pet_id,pet_breed,pet_type,pet_name from pet where pid IS NULL AND pet_breed = '" + breed + "'";
            MySqlDataReader reader = command.ExecuteReader();

            string data = "[ID]\t[BREED]\t[TYPE]\t[NAME]\n";
            if(reader.HasRows){
                while(reader.Read()){
                    data += reader.GetInt32(0) + "\t" + reader.GetString(1) + "\t" + reader.GetString(2) + "\t" + reader.GetString(3) + Environment.NewLine;
                }
                Console.WriteLine(data);
            }
            reader.Close();

            saveAnimalLikes(command);
        }

        public void sortAnimalType(MySqlCommand command){
            Console.Write("What animal type are you looking for? (Cat, Dog, Bird): ");
            string type = Console.ReadLine();
            command.CommandText = "SELECT pet_id,pet_type,pet_breed,pet_name from pet where pid IS NULL AND pet_type = '" + type + "'";
            MySqlDataReader reader = command.ExecuteReader();

            string data = "[ID]\t[TYPE]\t[BREED]\t[NAME]\n";
            if(reader.HasRows){
                while(reader.Read()){
                    data += reader.GetInt32(0) + "\t" + reader.GetString(1) + "\t" + reader.GetString(2) + "\t" + reader.GetString(3) + Environment.NewLine;
                }
                Console.WriteLine(data);
            }
            reader.Close();
            saveAnimalLikes(command);
        }

        public void showAnimals(MySqlCommand command){
            command.CommandText = "SELECT pet_id,pet_type,pet_breed,pet_name from pet where pid IS NULL";
            MySqlDataReader reader = command.ExecuteReader();

            string data = "[ID]\t[TYPE]\t[BREED]\t[NAME]\n";
            if(reader.HasRows){
                while(reader.Read()){
                    data += reader.GetInt32(0) + "\t" + reader.GetString(1) + "\t" + reader.GetString(2) + "\t" + reader.GetString(3) + Environment.NewLine;
                }
                Console.WriteLine(data);
            }
            reader.Close();
        }

        public void showAnimalHist(MySqlCommand command){
            command.CommandText = "select a.pet_id,a.pet_name,YEAR(b.ph_date_placed) FROM pet a, pet_hist b WHERE a.pet_id = b.pet_id AND ph_date_adopted IS NULL";
            MySqlDataReader reader = command.ExecuteReader();

            string data = "[ID]\t[NAME]\t[DATE PLACED]\n";
            if(reader.HasRows){
                while(reader.Read()){
                    data += reader.GetInt32(0) + "\t" + reader.GetString(1) + "\t" + reader.GetString(2) + Environment.NewLine;
                }
                Console.WriteLine(data);
            }
            reader.Close();
        }

        public void getNewCustomerData(long num){
            Console.WriteLine("It looks like you dont have an account with us! Please create an account.\n");
                Console.Write("First name: ");
                fname = Console.ReadLine();
                Console.Write("Last name: ");
                lname = Console.ReadLine();
                Console.Write("University: ");
                university = Console.ReadLine();
                Console.Write("Age: ");
                age = Console.ReadLine();
                Console.Write("Major: ");
                major = Console.ReadLine();
                Console.Write("Position: ");
                position = Console.ReadLine();
                ageToInt = Int16.Parse(age);
                access = 'c';
                phoneNum = num;
        }

        public void fetchCustomerData(long num, ref MySqlCommand command){
            command.CommandText = "select pid,fname,lname,university,age,major,position from people where phone = " + num;
            MySqlDataReader reader = command.ExecuteReader();
            
            //run reader on query string
            if(reader.HasRows){
                while(reader.Read()){
                    pid = reader.GetInt16(0);
                    fname = reader.GetString(1);
                    lname = reader.GetString(2);
                    university = reader.GetString(3);
                    ageToInt = reader.GetInt32(4);
                    major = reader.GetString(5);
                    position = reader.GetString(6);
                }
                reader.Close();
            }else{
                reader.Close();
            }
        }



        int pid;
        string fname;
        string lname;
        string university;
        int ageToInt;
        string age;
        string major;
        string position;
        long phoneNum;
        char access;
        int choiceToInt;
        MySqlDataReader reader;
    

    }
}