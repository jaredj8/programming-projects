using System;
using MySql.Data.MySqlClient;



namespace Intro
{
    class Test
    {
        static void Main(string[] args)
        {
            SqlConnector user = new SqlConnector(getNumber());
            //ask for phone number
            user.getConnection();
            //match phone number in db
            user.customerValidation();
             
        }

        static public string getNumber(){
            int flag = 1;
            //ask for phone number
            Console.WriteLine("Welcome!");
            Console.Write("Please enter your 10 digit phone number: ");
            string phone = Console.ReadLine();
            while(flag != 0){
                if(phone.Length != 10){
                    Console.Write("Please try again, enter your phone number: ");
                    phone = Console.ReadLine();
                }else{
                    flag = 0;
                }
            }

            return phone;
        }
    }
}