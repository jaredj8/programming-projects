using System;
using MySql.Data.MySqlClient;


namespace Intro
{
    class SqlConnector
    {
        public SqlConnector(string p){
            connectionString = "server=localhost;port=3306;uid=pAdmin;pwd=1306;database=person;charset=utf8;sslMode=none;allow user variables = true";
            conn = new MySqlConnection();
            phone = p;
            command = new MySqlCommand();
            phoneToInt = Convert.ToInt64(p);
        }

        public void getConnection(){
            try{
                conn.ConnectionString = connectionString;
                conn.Open();
            }catch(MySqlException ex){
                Console.WriteLine(ex);
            }
        }

        public void customerValidation(){
            int pid = 0;
            string fname = "";
            int flag = 1;

            //create command to see if phone exists
            command = conn.CreateCommand();
            command.CommandText = "select pid, fname from people where phone = " + phoneToInt;
            MySqlDataReader reader = command.ExecuteReader();
            
            //run reader on query string
            if(reader.HasRows){
                while(reader.Read()){
                    pid = reader.GetInt16(0);
                    fname = reader.GetString(1);
                }
                flag = 1;
                reader.Close();
            }else{
                flag = 0;
                reader.Close();
            }

            //if query string comes back empty create new user / user does not exist yet
            //else get user info in db
            if(flag == 0){
                //create customer
                Customer c = new Customer();
                c.getNewCustomerData(phoneToInt);
                c.createCustomer(ref command); 

                //customer menu / choice loop
                int choice;
                do{
                    c.customerMenu();
                    choice = c.getCustomerChoice(ref command);
                }while(choice != 10);

            }else{
                Customer c = new Customer();
                c.fetchCustomerData(phoneToInt, ref command);
                int choice;
                do{
                    c.customerMenu();
                    choice = c.getCustomerChoice(ref command);
                }while(choice != 10);
            }
        }

        MySqlCommand command;
        MySqlConnection conn;
        string connectionString;
        string phone;
        long phoneToInt;
    }


    
}