# Pet Store System

### Project Summary
- a system that stores people and pet data in mysql and displays the data through the terminal

### Project Details
- 1 month time frame(in progress)
- Solo
- C#/MySql
- potential for a website

### Database Structure
- Tables
    - [adoption_agency](img/adoptTable.png)
    - [people](img/peopleTable.png)
    - [pet](img/petTable.png)
    - [pet_favorites](img/favTable.png)
    - [pet_hist](img/histTable.png)
- Table Descriptions
    - pet is the source table, the table stores the pet id and is used in all the other tables
    - people table stores the users data 
    - adoption_agency table stores a pets previous location
    - pet_favorites table stores a list of users and their favorited pets
    - pet_hist table stores the history of all the pets

### Details sorted by function / image links
- [Main Menu](img/main.png)
    - basic menu to display options to the user
    - takes in phone number as authentication and matches to users in DB
- [Show available animals](img/show.png)
    - writes a query using a select statement and returns the result
    - the query makes sure that the pet is not already purchased 
- [Purchase an animal (IN PROGRESS)](img/purchase.png)
    - still working on how I want the payment system to function
    - some thoughts would be to practice my encryption techniques in MySql and pull that data into the program
- [Show animal history](img/history.png)
    - a select query targets the pet_hist table with the given pet_id
- [Sort available animals by type](img/type.png)
    - a select query is given to match for whether the pet is available and if the type is correct
    - only the available animals with that type are returned and displayed
- [Sort available animals by breed](img/breed.png)
    - a select query is given to match by breed and if that specific pet is available in the pet table
    - the available animals are returned with that breed type
- [Show previous shelter agency (IN PROGRESS)](img/agency.png)
    - a select statement is used to return a list of available animals and their previous location
- [Show favorited animals and their availability](img/fav.png)
    - the select statement looks for the current users id and all of the pet_ids the user has favorited
    - returns the users previous favorited animals and the data is stored in the pet_favorites table

### Files w/ Code
- #### Main Functionality
	- [main](intro.cs)
    - [mySqlConnector](SqlConnector.cs)
    - [customer](Customer.cs)

### Future functionality
- add a new employee class
    - separate customer from employee
    - give the employee a menu that will increase productivity (DevOps)
- work on payment system
    - probably a table to store card info
    - practice my security in MySql
- create a website
    - possible chance of learning ASP.NET

