library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity XorGate is
port
(
INPUT_A, INPUT_B, INPUT_C : IN STD_LOGIC;
OUTPUT : OUT STD_LOGIC
);
end XorGate;

Architecture behavorial of XorGate is
begin

OUTPUT <= INPUT_A XOR INPUT_B XOR INPUT_C;

end behavorial;