-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 20.1.1 Build 720 11/11/2020 SJ Lite Edition"

-- DATE "06/12/2021 18:31:54"

-- 
-- Device: Altera 5CGXFC9C7F23C8 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	ALU IS
    PORT (
	A : IN std_logic_vector(7 DOWNTO 0);
	B : IN std_logic_vector(7 DOWNTO 0);
	OP : IN std_logic_vector(2 DOWNTO 0);
	Result : OUT std_logic_vector(7 DOWNTO 0);
	Carryout : OUT std_logic
	);
END ALU;

-- Design Ports Information
-- Result[0]	=>  Location: PIN_U16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Result[1]	=>  Location: PIN_U20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Result[2]	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Result[3]	=>  Location: PIN_V21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Result[4]	=>  Location: PIN_U22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Result[5]	=>  Location: PIN_Y21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Result[6]	=>  Location: PIN_P14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Result[7]	=>  Location: PIN_V18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Carryout	=>  Location: PIN_V19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[1]	=>  Location: PIN_R15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[1]	=>  Location: PIN_P17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OP[2]	=>  Location: PIN_R14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OP[1]	=>  Location: PIN_T18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OP[0]	=>  Location: PIN_T19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[7]	=>  Location: PIN_U17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[2]	=>  Location: PIN_R16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[2]	=>  Location: PIN_P16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[6]	=>  Location: PIN_R17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[3]	=>  Location: PIN_T20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[3]	=>  Location: PIN_T15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[5]	=>  Location: PIN_P18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[4]	=>  Location: PIN_T17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[4]	=>  Location: PIN_T22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[5]	=>  Location: PIN_R21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[6]	=>  Location: PIN_R22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[7]	=>  Location: PIN_P22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[0]	=>  Location: PIN_U21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[0]	=>  Location: PIN_P19,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF ALU IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_A : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_B : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_OP : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_Result : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_Carryout : std_logic;
SIGNAL \Mult0~8_AX_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \Mult0~8_AY_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \Mult0~8_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \Mult0~16\ : std_logic;
SIGNAL \Mult0~17\ : std_logic;
SIGNAL \Mult0~18\ : std_logic;
SIGNAL \Mult0~19\ : std_logic;
SIGNAL \Mult0~20\ : std_logic;
SIGNAL \Mult0~21\ : std_logic;
SIGNAL \Mult0~22\ : std_logic;
SIGNAL \Mult0~23\ : std_logic;
SIGNAL \Mult0~24\ : std_logic;
SIGNAL \Mult0~25\ : std_logic;
SIGNAL \Mult0~26\ : std_logic;
SIGNAL \Mult0~27\ : std_logic;
SIGNAL \Mult0~28\ : std_logic;
SIGNAL \Mult0~29\ : std_logic;
SIGNAL \Mult0~30\ : std_logic;
SIGNAL \Mult0~31\ : std_logic;
SIGNAL \Mult0~32\ : std_logic;
SIGNAL \Mult0~33\ : std_logic;
SIGNAL \Mult0~34\ : std_logic;
SIGNAL \Mult0~35\ : std_logic;
SIGNAL \Mult0~36\ : std_logic;
SIGNAL \Mult0~37\ : std_logic;
SIGNAL \Mult0~38\ : std_logic;
SIGNAL \Mult0~39\ : std_logic;
SIGNAL \Mult0~40\ : std_logic;
SIGNAL \Mult0~41\ : std_logic;
SIGNAL \Mult0~42\ : std_logic;
SIGNAL \Mult0~43\ : std_logic;
SIGNAL \Mult0~44\ : std_logic;
SIGNAL \Mult0~45\ : std_logic;
SIGNAL \Mult0~46\ : std_logic;
SIGNAL \Mult0~47\ : std_logic;
SIGNAL \Mult0~48\ : std_logic;
SIGNAL \Mult0~49\ : std_logic;
SIGNAL \Mult0~50\ : std_logic;
SIGNAL \Mult0~51\ : std_logic;
SIGNAL \Mult0~52\ : std_logic;
SIGNAL \Mult0~53\ : std_logic;
SIGNAL \Mult0~54\ : std_logic;
SIGNAL \Mult0~55\ : std_logic;
SIGNAL \Mult0~56\ : std_logic;
SIGNAL \Mult0~57\ : std_logic;
SIGNAL \Mult0~58\ : std_logic;
SIGNAL \Mult0~59\ : std_logic;
SIGNAL \Mult0~60\ : std_logic;
SIGNAL \Mult0~61\ : std_logic;
SIGNAL \Mult0~62\ : std_logic;
SIGNAL \Mult0~63\ : std_logic;
SIGNAL \Mult0~64\ : std_logic;
SIGNAL \Mult0~65\ : std_logic;
SIGNAL \Mult0~66\ : std_logic;
SIGNAL \Mult0~67\ : std_logic;
SIGNAL \Mult0~68\ : std_logic;
SIGNAL \Mult0~69\ : std_logic;
SIGNAL \Mult0~70\ : std_logic;
SIGNAL \Mult0~71\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \B[0]~input_o\ : std_logic;
SIGNAL \A[0]~input_o\ : std_logic;
SIGNAL \B[5]~input_o\ : std_logic;
SIGNAL \A[5]~input_o\ : std_logic;
SIGNAL \B[3]~input_o\ : std_logic;
SIGNAL \A[3]~input_o\ : std_logic;
SIGNAL \A[4]~input_o\ : std_logic;
SIGNAL \B[4]~input_o\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \B[7]~input_o\ : std_logic;
SIGNAL \A[6]~input_o\ : std_logic;
SIGNAL \A[7]~input_o\ : std_logic;
SIGNAL \B[6]~input_o\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \A[1]~input_o\ : std_logic;
SIGNAL \A[2]~input_o\ : std_logic;
SIGNAL \B[1]~input_o\ : std_logic;
SIGNAL \B[2]~input_o\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~3_combout\ : std_logic;
SIGNAL \OP[1]~input_o\ : std_logic;
SIGNAL \Add1~29_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~6\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~7\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[0]~2_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~6\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~7\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~14\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~15\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~10\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~11\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~6\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~7\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[9]~1_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[9]~3_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[8]~7_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~13_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_4~22_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_4~18\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_4~14\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_4~10\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_4~6\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_4~1_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[18]~4_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_4~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[17]~8_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_4~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_4~13_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[16]~13_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_4~17_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~26_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~22\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~18\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~14\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~10\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~6\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~1_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[27]~0_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[27]~5_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[26]~9_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[25]~14_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~13_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~17_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[24]~17_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_5~21_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~30_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~26\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~22\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~18\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~14\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~10\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~6\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~1_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[36]~6_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[35]~10_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[34]~15_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~13_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[33]~18_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~17_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~21_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[32]~20_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_6~25_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~34_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~30\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~26\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~22\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~18\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~14\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~10\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~6\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~1_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[45]~11_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[45]~12_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[44]~16_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[43]~19_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~13_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[42]~21_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~17_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[41]~22_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~21_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~25_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[40]~23_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_7~29_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_8~38_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_8~34_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_8~30_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_8~26_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_8~22_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_8~18_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_8~14_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_8~10_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_8~6_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|op_8~1_sumout\ : std_logic;
SIGNAL \Mult0~8_resulta\ : std_logic;
SIGNAL \OP[0]~input_o\ : std_logic;
SIGNAL \OP[2]~input_o\ : std_logic;
SIGNAL \Add0~33_sumout\ : std_logic;
SIGNAL \Mux7~0_combout\ : std_logic;
SIGNAL \LessThan0~0_combout\ : std_logic;
SIGNAL \LessThan0~1_combout\ : std_logic;
SIGNAL \LessThan0~2_combout\ : std_logic;
SIGNAL \LessThan0~3_combout\ : std_logic;
SIGNAL \Mux7~4_combout\ : std_logic;
SIGNAL \Mult0~9\ : std_logic;
SIGNAL \Add1~30\ : std_logic;
SIGNAL \Add1~31\ : std_logic;
SIGNAL \Add1~1_sumout\ : std_logic;
SIGNAL \Add0~34\ : std_logic;
SIGNAL \Add0~1_sumout\ : std_logic;
SIGNAL \Mux6~0_combout\ : std_logic;
SIGNAL \Mux6~1_combout\ : std_logic;
SIGNAL \Mult0~10\ : std_logic;
SIGNAL \Add1~2\ : std_logic;
SIGNAL \Add1~3\ : std_logic;
SIGNAL \Add1~5_sumout\ : std_logic;
SIGNAL \Add0~2\ : std_logic;
SIGNAL \Add0~5_sumout\ : std_logic;
SIGNAL \Mux5~1_combout\ : std_logic;
SIGNAL \Mux5~0_combout\ : std_logic;
SIGNAL \Add0~6\ : std_logic;
SIGNAL \Add0~9_sumout\ : std_logic;
SIGNAL \Add1~6\ : std_logic;
SIGNAL \Add1~7\ : std_logic;
SIGNAL \Add1~9_sumout\ : std_logic;
SIGNAL \Mult0~11\ : std_logic;
SIGNAL \Mux4~0_combout\ : std_logic;
SIGNAL \Mux4~1_combout\ : std_logic;
SIGNAL \Mult0~12\ : std_logic;
SIGNAL \Add1~10\ : std_logic;
SIGNAL \Add1~11\ : std_logic;
SIGNAL \Add1~13_sumout\ : std_logic;
SIGNAL \Add0~10\ : std_logic;
SIGNAL \Add0~13_sumout\ : std_logic;
SIGNAL \Mux3~1_combout\ : std_logic;
SIGNAL \Mux3~0_combout\ : std_logic;
SIGNAL \Add1~14\ : std_logic;
SIGNAL \Add1~15\ : std_logic;
SIGNAL \Add1~17_sumout\ : std_logic;
SIGNAL \Add0~14\ : std_logic;
SIGNAL \Add0~17_sumout\ : std_logic;
SIGNAL \Mult0~13\ : std_logic;
SIGNAL \Mux2~0_combout\ : std_logic;
SIGNAL \Mux2~1_combout\ : std_logic;
SIGNAL \Add1~18\ : std_logic;
SIGNAL \Add1~19\ : std_logic;
SIGNAL \Add1~21_sumout\ : std_logic;
SIGNAL \Mult0~14\ : std_logic;
SIGNAL \Add0~18\ : std_logic;
SIGNAL \Add0~21_sumout\ : std_logic;
SIGNAL \Mux1~0_combout\ : std_logic;
SIGNAL \Mux1~1_combout\ : std_logic;
SIGNAL \Add0~22\ : std_logic;
SIGNAL \Add0~25_sumout\ : std_logic;
SIGNAL \Add1~22\ : std_logic;
SIGNAL \Add1~23\ : std_logic;
SIGNAL \Add1~25_sumout\ : std_logic;
SIGNAL \Mult0~15\ : std_logic;
SIGNAL \Mux0~0_combout\ : std_logic;
SIGNAL \Mux0~1_combout\ : std_logic;
SIGNAL \Add0~26\ : std_logic;
SIGNAL \Add0~29_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|selnose\ : std_logic_vector(71 DOWNTO 0);
SIGNAL \Div0|auto_generated|divider|divider|sel\ : std_logic_vector(71 DOWNTO 0);
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[2]~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_4~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_5~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_6~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_7~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~33_sumout\ : std_logic;
SIGNAL \ALT_INV_Mux7~0_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[1]~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~25_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~21_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[3]~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~17_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~13_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Mult0~15\ : std_logic;
SIGNAL \ALT_INV_Mult0~14\ : std_logic;
SIGNAL \ALT_INV_Mult0~13\ : std_logic;
SIGNAL \ALT_INV_Mult0~12\ : std_logic;
SIGNAL \ALT_INV_Mult0~11\ : std_logic;
SIGNAL \ALT_INV_Mult0~10\ : std_logic;
SIGNAL \ALT_INV_Mult0~9\ : std_logic;
SIGNAL \ALT_INV_Mult0~8_resulta\ : std_logic;
SIGNAL \ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~1_sumout\ : std_logic;
SIGNAL \ALT_INV_B[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_A[1]~input_o\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[40]~23_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[41]~22_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[42]~21_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[32]~20_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[43]~19_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[33]~18_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[24]~17_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[44]~16_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[34]~15_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[25]~14_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[16]~13_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~12_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~11_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[35]~10_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[26]~9_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[17]~8_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[8]~7_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[36]~6_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~5_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[18]~4_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~3_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[0]~2_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~1_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~0_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~3_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~2_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~1_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal0~3_combout\ : std_logic;
SIGNAL \ALT_INV_Equal0~2_combout\ : std_logic;
SIGNAL \ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \ALT_INV_Mux0~0_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_selnose\ : std_logic_vector(54 DOWNTO 0);
SIGNAL \ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \ALT_INV_Mux2~0_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_sel\ : std_logic_vector(45 DOWNTO 18);
SIGNAL \ALT_INV_Mux4~0_combout\ : std_logic;
SIGNAL \ALT_INV_Mux6~0_combout\ : std_logic;
SIGNAL \ALT_INV_Mux5~1_combout\ : std_logic;
SIGNAL \ALT_INV_Mux3~1_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_7~29_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_6~25_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_7~25_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_5~21_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_6~21_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_7~21_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_4~17_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_5~17_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_6~17_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_7~17_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[0]~13_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_4~13_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_5~13_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_6~13_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_7~13_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[0]~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[1]~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_4~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_5~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_6~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_7~9_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_op_8~1_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[0]~5_sumout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[1]~5_sumout\ : std_logic;
SIGNAL \ALT_INV_B[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_A[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_A[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_A[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_A[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_B[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_A[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_B[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_B[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_A[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_B[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_B[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_A[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_B[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_OP[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_OP[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_OP[2]~input_o\ : std_logic;

BEGIN

ww_A <= A;
ww_B <= B;
ww_OP <= OP;
Result <= ww_Result;
Carryout <= ww_Carryout;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\Mult0~8_AX_bus\ <= (\A[7]~input_o\ & \A[6]~input_o\ & \A[5]~input_o\ & \A[4]~input_o\ & \A[3]~input_o\ & \A[2]~input_o\ & \A[1]~input_o\ & \A[0]~input_o\);

\Mult0~8_AY_bus\ <= (\B[7]~input_o\ & \B[6]~input_o\ & \B[5]~input_o\ & \B[4]~input_o\ & \B[3]~input_o\ & \B[2]~input_o\ & \B[1]~input_o\ & \B[0]~input_o\);

\Mult0~8_resulta\ <= \Mult0~8_RESULTA_bus\(0);
\Mult0~9\ <= \Mult0~8_RESULTA_bus\(1);
\Mult0~10\ <= \Mult0~8_RESULTA_bus\(2);
\Mult0~11\ <= \Mult0~8_RESULTA_bus\(3);
\Mult0~12\ <= \Mult0~8_RESULTA_bus\(4);
\Mult0~13\ <= \Mult0~8_RESULTA_bus\(5);
\Mult0~14\ <= \Mult0~8_RESULTA_bus\(6);
\Mult0~15\ <= \Mult0~8_RESULTA_bus\(7);
\Mult0~16\ <= \Mult0~8_RESULTA_bus\(8);
\Mult0~17\ <= \Mult0~8_RESULTA_bus\(9);
\Mult0~18\ <= \Mult0~8_RESULTA_bus\(10);
\Mult0~19\ <= \Mult0~8_RESULTA_bus\(11);
\Mult0~20\ <= \Mult0~8_RESULTA_bus\(12);
\Mult0~21\ <= \Mult0~8_RESULTA_bus\(13);
\Mult0~22\ <= \Mult0~8_RESULTA_bus\(14);
\Mult0~23\ <= \Mult0~8_RESULTA_bus\(15);
\Mult0~24\ <= \Mult0~8_RESULTA_bus\(16);
\Mult0~25\ <= \Mult0~8_RESULTA_bus\(17);
\Mult0~26\ <= \Mult0~8_RESULTA_bus\(18);
\Mult0~27\ <= \Mult0~8_RESULTA_bus\(19);
\Mult0~28\ <= \Mult0~8_RESULTA_bus\(20);
\Mult0~29\ <= \Mult0~8_RESULTA_bus\(21);
\Mult0~30\ <= \Mult0~8_RESULTA_bus\(22);
\Mult0~31\ <= \Mult0~8_RESULTA_bus\(23);
\Mult0~32\ <= \Mult0~8_RESULTA_bus\(24);
\Mult0~33\ <= \Mult0~8_RESULTA_bus\(25);
\Mult0~34\ <= \Mult0~8_RESULTA_bus\(26);
\Mult0~35\ <= \Mult0~8_RESULTA_bus\(27);
\Mult0~36\ <= \Mult0~8_RESULTA_bus\(28);
\Mult0~37\ <= \Mult0~8_RESULTA_bus\(29);
\Mult0~38\ <= \Mult0~8_RESULTA_bus\(30);
\Mult0~39\ <= \Mult0~8_RESULTA_bus\(31);
\Mult0~40\ <= \Mult0~8_RESULTA_bus\(32);
\Mult0~41\ <= \Mult0~8_RESULTA_bus\(33);
\Mult0~42\ <= \Mult0~8_RESULTA_bus\(34);
\Mult0~43\ <= \Mult0~8_RESULTA_bus\(35);
\Mult0~44\ <= \Mult0~8_RESULTA_bus\(36);
\Mult0~45\ <= \Mult0~8_RESULTA_bus\(37);
\Mult0~46\ <= \Mult0~8_RESULTA_bus\(38);
\Mult0~47\ <= \Mult0~8_RESULTA_bus\(39);
\Mult0~48\ <= \Mult0~8_RESULTA_bus\(40);
\Mult0~49\ <= \Mult0~8_RESULTA_bus\(41);
\Mult0~50\ <= \Mult0~8_RESULTA_bus\(42);
\Mult0~51\ <= \Mult0~8_RESULTA_bus\(43);
\Mult0~52\ <= \Mult0~8_RESULTA_bus\(44);
\Mult0~53\ <= \Mult0~8_RESULTA_bus\(45);
\Mult0~54\ <= \Mult0~8_RESULTA_bus\(46);
\Mult0~55\ <= \Mult0~8_RESULTA_bus\(47);
\Mult0~56\ <= \Mult0~8_RESULTA_bus\(48);
\Mult0~57\ <= \Mult0~8_RESULTA_bus\(49);
\Mult0~58\ <= \Mult0~8_RESULTA_bus\(50);
\Mult0~59\ <= \Mult0~8_RESULTA_bus\(51);
\Mult0~60\ <= \Mult0~8_RESULTA_bus\(52);
\Mult0~61\ <= \Mult0~8_RESULTA_bus\(53);
\Mult0~62\ <= \Mult0~8_RESULTA_bus\(54);
\Mult0~63\ <= \Mult0~8_RESULTA_bus\(55);
\Mult0~64\ <= \Mult0~8_RESULTA_bus\(56);
\Mult0~65\ <= \Mult0~8_RESULTA_bus\(57);
\Mult0~66\ <= \Mult0~8_RESULTA_bus\(58);
\Mult0~67\ <= \Mult0~8_RESULTA_bus\(59);
\Mult0~68\ <= \Mult0~8_RESULTA_bus\(60);
\Mult0~69\ <= \Mult0~8_RESULTA_bus\(61);
\Mult0~70\ <= \Mult0~8_RESULTA_bus\(62);
\Mult0~71\ <= \Mult0~8_RESULTA_bus\(63);
\Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[2]~5_sumout\ <= NOT \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~5_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_4~5_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_4~5_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_5~5_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_5~5_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_6~5_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_6~5_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_7~5_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_7~5_sumout\;
\ALT_INV_Add1~29_sumout\ <= NOT \Add1~29_sumout\;
\ALT_INV_Add0~33_sumout\ <= NOT \Add0~33_sumout\;
\ALT_INV_Mux7~0_combout\ <= NOT \Mux7~0_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[1]~1_sumout\ <= NOT \Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\;
\ALT_INV_Add1~25_sumout\ <= NOT \Add1~25_sumout\;
\ALT_INV_Add0~25_sumout\ <= NOT \Add0~25_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\ <= NOT \Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\;
\ALT_INV_Add1~21_sumout\ <= NOT \Add1~21_sumout\;
\ALT_INV_Add0~21_sumout\ <= NOT \Add0~21_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[3]~1_sumout\ <= NOT \Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\;
\ALT_INV_Add1~17_sumout\ <= NOT \Add1~17_sumout\;
\ALT_INV_Add0~17_sumout\ <= NOT \Add0~17_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_4~1_sumout\;
\ALT_INV_Add1~13_sumout\ <= NOT \Add1~13_sumout\;
\ALT_INV_Add0~13_sumout\ <= NOT \Add0~13_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_5~1_sumout\;
\ALT_INV_Add1~9_sumout\ <= NOT \Add1~9_sumout\;
\ALT_INV_Add0~9_sumout\ <= NOT \Add0~9_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_6~1_sumout\;
\ALT_INV_Add1~5_sumout\ <= NOT \Add1~5_sumout\;
\ALT_INV_Add0~5_sumout\ <= NOT \Add0~5_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_7~1_sumout\;
\ALT_INV_Mult0~15\ <= NOT \Mult0~15\;
\ALT_INV_Mult0~14\ <= NOT \Mult0~14\;
\ALT_INV_Mult0~13\ <= NOT \Mult0~13\;
\ALT_INV_Mult0~12\ <= NOT \Mult0~12\;
\ALT_INV_Mult0~11\ <= NOT \Mult0~11\;
\ALT_INV_Mult0~10\ <= NOT \Mult0~10\;
\ALT_INV_Mult0~9\ <= NOT \Mult0~9\;
\ALT_INV_Mult0~8_resulta\ <= NOT \Mult0~8_resulta\;
\ALT_INV_Add1~1_sumout\ <= NOT \Add1~1_sumout\;
\ALT_INV_Add0~1_sumout\ <= NOT \Add0~1_sumout\;
\ALT_INV_B[1]~input_o\ <= NOT \B[1]~input_o\;
\ALT_INV_A[1]~input_o\ <= NOT \A[1]~input_o\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[40]~23_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[40]~23_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[41]~22_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[41]~22_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[42]~21_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[42]~21_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[32]~20_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[32]~20_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[43]~19_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[43]~19_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[33]~18_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[33]~18_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[24]~17_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[24]~17_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[44]~16_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[44]~16_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[34]~15_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[34]~15_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[25]~14_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[25]~14_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[16]~13_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[16]~13_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~12_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[45]~12_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~11_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[45]~11_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[35]~10_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[35]~10_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[26]~9_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[26]~9_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[17]~8_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[17]~8_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[8]~7_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[8]~7_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[36]~6_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[36]~6_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~5_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[27]~5_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[18]~4_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[18]~4_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~3_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[9]~3_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[0]~2_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[0]~2_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~1_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[9]~1_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~0_combout\ <= NOT \Div0|auto_generated|divider|divider|StageOut[27]~0_combout\;
\ALT_INV_LessThan0~3_combout\ <= NOT \LessThan0~3_combout\;
\ALT_INV_LessThan0~2_combout\ <= NOT \LessThan0~2_combout\;
\ALT_INV_LessThan0~1_combout\ <= NOT \LessThan0~1_combout\;
\ALT_INV_LessThan0~0_combout\ <= NOT \LessThan0~0_combout\;
\ALT_INV_Equal0~3_combout\ <= NOT \Equal0~3_combout\;
\ALT_INV_Equal0~2_combout\ <= NOT \Equal0~2_combout\;
\ALT_INV_Equal0~1_combout\ <= NOT \Equal0~1_combout\;
\ALT_INV_Equal0~0_combout\ <= NOT \Equal0~0_combout\;
\ALT_INV_Mux0~0_combout\ <= NOT \Mux0~0_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_selnose\(0) <= NOT \Div0|auto_generated|divider|divider|selnose\(0);
\ALT_INV_Mux1~0_combout\ <= NOT \Mux1~0_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_selnose\(9) <= NOT \Div0|auto_generated|divider|divider|selnose\(9);
\ALT_INV_Mux2~0_combout\ <= NOT \Mux2~0_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_selnose\(18) <= NOT \Div0|auto_generated|divider|divider|selnose\(18);
\Div0|auto_generated|divider|divider|ALT_INV_sel\(18) <= NOT \Div0|auto_generated|divider|divider|sel\(18);
\Div0|auto_generated|divider|divider|ALT_INV_sel\(27) <= NOT \Div0|auto_generated|divider|divider|sel\(27);
\ALT_INV_Mux4~0_combout\ <= NOT \Mux4~0_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_selnose\(36) <= NOT \Div0|auto_generated|divider|divider|selnose\(36);
\Div0|auto_generated|divider|divider|ALT_INV_sel\(36) <= NOT \Div0|auto_generated|divider|divider|sel\(36);
\Div0|auto_generated|divider|divider|ALT_INV_sel\(45) <= NOT \Div0|auto_generated|divider|divider|sel\(45);
\ALT_INV_Mux6~0_combout\ <= NOT \Mux6~0_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_selnose\(54) <= NOT \Div0|auto_generated|divider|divider|selnose\(54);
\ALT_INV_Mux5~1_combout\ <= NOT \Mux5~1_combout\;
\ALT_INV_Mux3~1_combout\ <= NOT \Mux3~1_combout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_7~29_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_7~29_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_6~25_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_6~25_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_7~25_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_7~25_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_5~21_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_5~21_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_6~21_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_6~21_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_7~21_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_7~21_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_4~17_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_4~17_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_5~17_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_5~17_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_6~17_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_6~17_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_7~17_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_7~17_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[0]~13_sumout\ <= NOT \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~13_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_4~13_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_4~13_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_5~13_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_5~13_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_6~13_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_6~13_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_7~13_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_7~13_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[0]~9_sumout\ <= NOT \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[1]~9_sumout\ <= NOT \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~9_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_4~9_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_4~9_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_5~9_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_5~9_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_6~9_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_6~9_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_7~9_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_7~9_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_op_8~1_sumout\ <= NOT \Div0|auto_generated|divider|divider|op_8~1_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[0]~5_sumout\ <= NOT \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\;
\Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[1]~5_sumout\ <= NOT \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\;
\ALT_INV_B[0]~input_o\ <= NOT \B[0]~input_o\;
\ALT_INV_A[0]~input_o\ <= NOT \A[0]~input_o\;
\ALT_INV_A[7]~input_o\ <= NOT \A[7]~input_o\;
\ALT_INV_A[6]~input_o\ <= NOT \A[6]~input_o\;
\ALT_INV_A[5]~input_o\ <= NOT \A[5]~input_o\;
\ALT_INV_B[4]~input_o\ <= NOT \B[4]~input_o\;
\ALT_INV_A[4]~input_o\ <= NOT \A[4]~input_o\;
\ALT_INV_B[5]~input_o\ <= NOT \B[5]~input_o\;
\ALT_INV_B[3]~input_o\ <= NOT \B[3]~input_o\;
\ALT_INV_A[3]~input_o\ <= NOT \A[3]~input_o\;
\ALT_INV_B[6]~input_o\ <= NOT \B[6]~input_o\;
\ALT_INV_B[2]~input_o\ <= NOT \B[2]~input_o\;
\ALT_INV_A[2]~input_o\ <= NOT \A[2]~input_o\;
\ALT_INV_B[7]~input_o\ <= NOT \B[7]~input_o\;
\ALT_INV_OP[0]~input_o\ <= NOT \OP[0]~input_o\;
\ALT_INV_OP[1]~input_o\ <= NOT \OP[1]~input_o\;
\ALT_INV_OP[2]~input_o\ <= NOT \OP[2]~input_o\;

-- Location: IOOBUF_X88_Y0_N19
\Result[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux7~4_combout\,
	devoe => ww_devoe,
	o => ww_Result(0));

-- Location: IOOBUF_X88_Y0_N36
\Result[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux6~1_combout\,
	devoe => ww_devoe,
	o => ww_Result(1));

-- Location: IOOBUF_X84_Y0_N36
\Result[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux5~0_combout\,
	devoe => ww_devoe,
	o => ww_Result(2));

-- Location: IOOBUF_X86_Y0_N36
\Result[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux4~1_combout\,
	devoe => ww_devoe,
	o => ww_Result(3));

-- Location: IOOBUF_X86_Y0_N53
\Result[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux3~0_combout\,
	devoe => ww_devoe,
	o => ww_Result(4));

-- Location: IOOBUF_X84_Y0_N53
\Result[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux2~1_combout\,
	devoe => ww_devoe,
	o => ww_Result(5));

-- Location: IOOBUF_X84_Y0_N19
\Result[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux1~1_combout\,
	devoe => ww_devoe,
	o => ww_Result(6));

-- Location: IOOBUF_X86_Y0_N2
\Result[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux0~1_combout\,
	devoe => ww_devoe,
	o => ww_Result(7));

-- Location: IOOBUF_X86_Y0_N19
\Carryout~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add0~29_sumout\,
	devoe => ww_devoe,
	o => ww_Carryout);

-- Location: IOIBUF_X121_Y17_N38
\B[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(0),
	o => \B[0]~input_o\);

-- Location: IOIBUF_X88_Y0_N52
\A[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(0),
	o => \A[0]~input_o\);

-- Location: IOIBUF_X121_Y17_N55
\B[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(5),
	o => \B[5]~input_o\);

-- Location: IOIBUF_X121_Y16_N38
\A[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(5),
	o => \A[5]~input_o\);

-- Location: IOIBUF_X121_Y14_N4
\B[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(3),
	o => \B[3]~input_o\);

-- Location: IOIBUF_X121_Y13_N95
\A[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(3),
	o => \A[3]~input_o\);

-- Location: IOIBUF_X121_Y13_N61
\A[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(4),
	o => \A[4]~input_o\);

-- Location: IOIBUF_X121_Y14_N38
\B[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(4),
	o => \B[4]~input_o\);

-- Location: MLABCELL_X114_Y18_N48
\Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = ( \B[4]~input_o\ & ( (\A[4]~input_o\ & (!\B[3]~input_o\ $ (\A[3]~input_o\))) ) ) # ( !\B[4]~input_o\ & ( (!\A[4]~input_o\ & (!\B[3]~input_o\ $ (\A[3]~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001100100000000100110010000000000000000100110010000000010011001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[3]~input_o\,
	datab => \ALT_INV_A[3]~input_o\,
	datad => \ALT_INV_A[4]~input_o\,
	dataf => \ALT_INV_B[4]~input_o\,
	combout => \Equal0~2_combout\);

-- Location: IOIBUF_X88_Y0_N1
\B[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(7),
	o => \B[7]~input_o\);

-- Location: IOIBUF_X121_Y14_N55
\A[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(6),
	o => \A[6]~input_o\);

-- Location: IOIBUF_X121_Y16_N55
\A[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(7),
	o => \A[7]~input_o\);

-- Location: IOIBUF_X121_Y16_N21
\B[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(6),
	o => \B[6]~input_o\);

-- Location: MLABCELL_X114_Y18_N39
\Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = ( \B[6]~input_o\ & ( (\A[6]~input_o\ & (!\B[7]~input_o\ $ (\A[7]~input_o\))) ) ) # ( !\B[6]~input_o\ & ( (!\A[6]~input_o\ & (!\B[7]~input_o\ $ (\A[7]~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000110000110000000011000000001100000000110000110000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B[7]~input_o\,
	datac => \ALT_INV_A[6]~input_o\,
	datad => \ALT_INV_A[7]~input_o\,
	dataf => \ALT_INV_B[6]~input_o\,
	combout => \Equal0~0_combout\);

-- Location: IOIBUF_X121_Y14_N21
\A[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(1),
	o => \A[1]~input_o\);

-- Location: IOIBUF_X121_Y16_N4
\A[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(2),
	o => \A[2]~input_o\);

-- Location: IOIBUF_X121_Y17_N21
\B[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(1),
	o => \B[1]~input_o\);

-- Location: IOIBUF_X121_Y17_N4
\B[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(2),
	o => \B[2]~input_o\);

-- Location: MLABCELL_X114_Y18_N42
\Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = ( \B[2]~input_o\ & ( \B[0]~input_o\ & ( (\A[2]~input_o\ & (\A[0]~input_o\ & (!\A[1]~input_o\ $ (\B[1]~input_o\)))) ) ) ) # ( !\B[2]~input_o\ & ( \B[0]~input_o\ & ( (!\A[2]~input_o\ & (\A[0]~input_o\ & (!\A[1]~input_o\ $ 
-- (\B[1]~input_o\)))) ) ) ) # ( \B[2]~input_o\ & ( !\B[0]~input_o\ & ( (\A[2]~input_o\ & (!\A[0]~input_o\ & (!\A[1]~input_o\ $ (\B[1]~input_o\)))) ) ) ) # ( !\B[2]~input_o\ & ( !\B[0]~input_o\ & ( (!\A[2]~input_o\ & (!\A[0]~input_o\ & (!\A[1]~input_o\ $ 
-- (\B[1]~input_o\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000001000000001000000001000000001000000001000000001000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[1]~input_o\,
	datab => \ALT_INV_A[2]~input_o\,
	datac => \ALT_INV_A[0]~input_o\,
	datad => \ALT_INV_B[1]~input_o\,
	datae => \ALT_INV_B[2]~input_o\,
	dataf => \ALT_INV_B[0]~input_o\,
	combout => \Equal0~1_combout\);

-- Location: MLABCELL_X114_Y18_N54
\Equal0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~3_combout\ = ( \Equal0~1_combout\ & ( (\Equal0~2_combout\ & (\Equal0~0_combout\ & (!\B[5]~input_o\ $ (\A[5]~input_o\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000010010000000000001001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[5]~input_o\,
	datab => \ALT_INV_A[5]~input_o\,
	datac => \ALT_INV_Equal0~2_combout\,
	datad => \ALT_INV_Equal0~0_combout\,
	dataf => \ALT_INV_Equal0~1_combout\,
	combout => \Equal0~3_combout\);

-- Location: IOIBUF_X121_Y13_N44
\OP[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_OP(1),
	o => \OP[1]~input_o\);

-- Location: MLABCELL_X114_Y17_N30
\Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~29_sumout\ = SUM(( !\B[0]~input_o\ $ (!\A[0]~input_o\) ) + ( !VCC ) + ( !VCC ))
-- \Add1~30\ = CARRY(( !\B[0]~input_o\ $ (!\A[0]~input_o\) ) + ( !VCC ) + ( !VCC ))
-- \Add1~31\ = SHARE((!\B[0]~input_o\) # (\A[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001111111100000000000000000011001111001100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B[0]~input_o\,
	datad => \ALT_INV_A[0]~input_o\,
	cin => GND,
	sharein => GND,
	sumout => \Add1~29_sumout\,
	cout => \Add1~30\,
	shareout => \Add1~31\);

-- Location: MLABCELL_X119_Y18_N30
\Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\ = SUM(( !\B[0]~input_o\ $ (!\A[7]~input_o\) ) + ( !VCC ) + ( !VCC ))
-- \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~6\ = CARRY(( !\B[0]~input_o\ $ (!\A[7]~input_o\) ) + ( !VCC ) + ( !VCC ))
-- \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~7\ = SHARE((!\B[0]~input_o\) # (\A[7]~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101011111010111100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[0]~input_o\,
	datac => \ALT_INV_A[7]~input_o\,
	cin => GND,
	sharein => GND,
	sumout => \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~6\,
	shareout => \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~7\);

-- Location: MLABCELL_X119_Y18_N33
\Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\ = SUM(( VCC ) + ( \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~7\ ) + ( \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~6\,
	sharein => \Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~7\,
	sumout => \Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\);

-- Location: MLABCELL_X114_Y18_N0
\Div0|auto_generated|divider|divider|sel[18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|sel\(18) = ( \B[5]~input_o\ ) # ( !\B[5]~input_o\ & ( (((\B[3]~input_o\) # (\B[6]~input_o\)) # (\B[4]~input_o\)) # (\B[7]~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111111111111011111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[7]~input_o\,
	datab => \ALT_INV_B[4]~input_o\,
	datac => \ALT_INV_B[6]~input_o\,
	datad => \ALT_INV_B[3]~input_o\,
	dataf => \ALT_INV_B[5]~input_o\,
	combout => \Div0|auto_generated|divider|divider|sel\(18));

-- Location: MLABCELL_X119_Y18_N54
\Div0|auto_generated|divider|divider|selnose[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|selnose\(0) = ( \Div0|auto_generated|divider|divider|sel\(18) & ( \B[1]~input_o\ ) ) # ( !\Div0|auto_generated|divider|divider|sel\(18) & ( \B[1]~input_o\ ) ) # ( \Div0|auto_generated|divider|divider|sel\(18) & ( 
-- !\B[1]~input_o\ ) ) # ( !\Div0|auto_generated|divider|divider|sel\(18) & ( !\B[1]~input_o\ & ( (\Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\) # (\B[2]~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B[2]~input_o\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[1]~1_sumout\,
	datae => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	dataf => \ALT_INV_B[1]~input_o\,
	combout => \Div0|auto_generated|divider|divider|selnose\(0));

-- Location: MLABCELL_X119_Y18_N0
\Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\ = SUM(( !\A[6]~input_o\ $ (!\B[0]~input_o\) ) + ( !VCC ) + ( !VCC ))
-- \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\ = CARRY(( !\A[6]~input_o\ $ (!\B[0]~input_o\) ) + ( !VCC ) + ( !VCC ))
-- \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\ = SHARE((!\B[0]~input_o\) # (\A[6]~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000111100000000000000000000111111110000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_A[6]~input_o\,
	datad => \ALT_INV_B[0]~input_o\,
	cin => GND,
	sharein => GND,
	sumout => \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\,
	shareout => \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\);

-- Location: MLABCELL_X119_Y18_N3
\Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\ = SUM(( !\B[1]~input_o\ $ (((!\Div0|auto_generated|divider|divider|selnose\(0) & (\Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|selnose\(0) & ((\A[7]~input_o\))))) ) + ( \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\ ) + ( \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\ ))
-- \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~6\ = CARRY(( !\B[1]~input_o\ $ (((!\Div0|auto_generated|divider|divider|selnose\(0) & (\Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|selnose\(0) & ((\A[7]~input_o\))))) ) + ( \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\ ) + ( \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\ ))
-- \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~7\ = SHARE((!\B[1]~input_o\ & ((!\Div0|auto_generated|divider|divider|selnose\(0) & (\Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|selnose\(0) & ((\A[7]~input_o\))))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001000000111000000000000000000001101001010000111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_selnose\(0),
	datab => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[0]~5_sumout\,
	datac => \ALT_INV_B[1]~input_o\,
	datad => \ALT_INV_A[7]~input_o\,
	cin => \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\,
	sharein => \Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\,
	sumout => \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~6\,
	shareout => \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~7\);

-- Location: MLABCELL_X119_Y18_N42
\Div0|auto_generated|divider|divider|StageOut[0]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[0]~2_combout\ = ( \Div0|auto_generated|divider|divider|sel\(18) & ( \B[1]~input_o\ & ( \A[7]~input_o\ ) ) ) # ( !\Div0|auto_generated|divider|divider|sel\(18) & ( \B[1]~input_o\ & ( \A[7]~input_o\ ) ) ) # ( 
-- \Div0|auto_generated|divider|divider|sel\(18) & ( !\B[1]~input_o\ & ( \A[7]~input_o\ ) ) ) # ( !\Div0|auto_generated|divider|divider|sel\(18) & ( !\B[1]~input_o\ & ( (!\Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\ & 
-- ((!\B[2]~input_o\ & ((\Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\))) # (\B[2]~input_o\ & (\A[7]~input_o\)))) # (\Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\ & (\A[7]~input_o\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[7]~input_o\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[0]~5_sumout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[1]~1_sumout\,
	datad => \ALT_INV_B[2]~input_o\,
	datae => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	dataf => \ALT_INV_B[1]~input_o\,
	combout => \Div0|auto_generated|divider|divider|StageOut[0]~2_combout\);

-- Location: MLABCELL_X119_Y18_N6
\Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ = SUM(( VCC ) + ( \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~7\ ) + ( \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~6\,
	sharein => \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~7\,
	sumout => \Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\);

-- Location: MLABCELL_X119_Y18_N21
\Div0|auto_generated|divider|divider|selnose[9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|selnose\(9) = ( \Div0|auto_generated|divider|divider|sel\(18) & ( \Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ ) ) # ( !\Div0|auto_generated|divider|divider|sel\(18) & ( 
-- \Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ ) ) # ( \Div0|auto_generated|divider|divider|sel\(18) & ( !\Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ ) ) # ( !\Div0|auto_generated|divider|divider|sel\(18) 
-- & ( !\Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ( \B[2]~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_B[2]~input_o\,
	datae => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\,
	combout => \Div0|auto_generated|divider|divider|selnose\(9));

-- Location: LABCELL_X117_Y18_N0
\Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~13_sumout\ = SUM(( !\A[5]~input_o\ $ (!\B[0]~input_o\) ) + ( !VCC ) + ( !VCC ))
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~14\ = CARRY(( !\A[5]~input_o\ $ (!\B[0]~input_o\) ) + ( !VCC ) + ( !VCC ))
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~15\ = SHARE((!\B[0]~input_o\) # (\A[5]~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000111100000000000000000000111111110000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_A[5]~input_o\,
	datad => \ALT_INV_B[0]~input_o\,
	cin => GND,
	sharein => GND,
	sumout => \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~13_sumout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~14\,
	shareout => \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~15\);

-- Location: LABCELL_X117_Y18_N3
\Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~9_sumout\ = SUM(( !\B[1]~input_o\ $ (((!\Div0|auto_generated|divider|divider|selnose\(9) & (\Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|selnose\(9) & ((\A[6]~input_o\))))) ) + ( \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~15\ ) + ( \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~14\ ))
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~10\ = CARRY(( !\B[1]~input_o\ $ (((!\Div0|auto_generated|divider|divider|selnose\(9) & (\Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|selnose\(9) & ((\A[6]~input_o\))))) ) + ( \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~15\ ) + ( \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~14\ ))
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~11\ = SHARE((!\B[1]~input_o\ & ((!\Div0|auto_generated|divider|divider|selnose\(9) & (\Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|selnose\(9) & ((\A[6]~input_o\))))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001001110000000000000000000000001101100000100111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_selnose\(9),
	datab => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[0]~9_sumout\,
	datac => \ALT_INV_A[6]~input_o\,
	datad => \ALT_INV_B[1]~input_o\,
	cin => \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~14\,
	sharein => \Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~15\,
	sumout => \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~9_sumout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~10\,
	shareout => \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~11\);

-- Location: LABCELL_X117_Y18_N6
\Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~5_sumout\ = SUM(( !\B[2]~input_o\ $ (((!\Div0|auto_generated|divider|divider|selnose\(9) & (\Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|selnose\(9) & ((\Div0|auto_generated|divider|divider|StageOut[0]~2_combout\))))) ) + ( \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~11\ ) + ( 
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~10\ ))
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~6\ = CARRY(( !\B[2]~input_o\ $ (((!\Div0|auto_generated|divider|divider|selnose\(9) & (\Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|selnose\(9) & ((\Div0|auto_generated|divider|divider|StageOut[0]~2_combout\))))) ) + ( \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~11\ ) + ( 
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~10\ ))
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~7\ = SHARE((!\B[2]~input_o\ & ((!\Div0|auto_generated|divider|divider|selnose\(9) & (\Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|selnose\(9) & ((\Div0|auto_generated|divider|divider|StageOut[0]~2_combout\))))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010100110000000000000000000000001010110001010011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[1]~5_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[0]~2_combout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_selnose\(9),
	datad => \ALT_INV_B[2]~input_o\,
	cin => \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~10\,
	sharein => \Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~11\,
	sumout => \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~5_sumout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~6\,
	shareout => \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~7\);

-- Location: LABCELL_X117_Y18_N9
\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ = SUM(( VCC ) + ( \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~7\ ) + ( \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~6\,
	sharein => \Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~7\,
	sumout => \Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\);

-- Location: MLABCELL_X119_Y18_N12
\Div0|auto_generated|divider|divider|selnose[18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|selnose\(18) = ( \Div0|auto_generated|divider|divider|sel\(18) & ( \Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ ) ) # ( !\Div0|auto_generated|divider|divider|sel\(18) & ( 
-- \Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ ) ) # ( \Div0|auto_generated|divider|divider|sel\(18) & ( !\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[3]~1_sumout\,
	combout => \Div0|auto_generated|divider|divider|selnose\(18));

-- Location: MLABCELL_X119_Y18_N36
\Div0|auto_generated|divider|divider|StageOut[9]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[9]~1_combout\ = ( !\Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ( (!\Div0|auto_generated|divider|divider|sel\(18) & (!\B[2]~input_o\ & 
-- \Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010100000000000001010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	datac => \ALT_INV_B[2]~input_o\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[1]~5_sumout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[9]~1_combout\);

-- Location: MLABCELL_X119_Y18_N51
\Div0|auto_generated|divider|divider|StageOut[9]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[9]~3_combout\ = ( \Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ( \Div0|auto_generated|divider|divider|StageOut[0]~2_combout\ ) ) # ( 
-- !\Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ( (\Div0|auto_generated|divider|divider|StageOut[0]~2_combout\ & ((\Div0|auto_generated|divider|divider|sel\(18)) # (\B[2]~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100001111000000110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B[2]~input_o\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[0]~2_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[9]~3_combout\);

-- Location: MLABCELL_X119_Y18_N48
\Div0|auto_generated|divider|divider|StageOut[8]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[8]~7_combout\ = ( \Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ( \A[6]~input_o\ ) ) # ( !\Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ( 
-- (!\Div0|auto_generated|divider|divider|sel\(18) & ((!\B[2]~input_o\ & (\Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\)) # (\B[2]~input_o\ & ((\A[6]~input_o\))))) # (\Div0|auto_generated|divider|divider|sel\(18) & 
-- (((\A[6]~input_o\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100001111111000010000111111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	datab => \ALT_INV_B[2]~input_o\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[0]~9_sumout\,
	datad => \ALT_INV_A[6]~input_o\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[8]~7_combout\);

-- Location: LABCELL_X117_Y18_N36
\Div0|auto_generated|divider|divider|op_4~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_4~22_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \Div0|auto_generated|divider|divider|op_4~22_cout\);

-- Location: LABCELL_X117_Y18_N39
\Div0|auto_generated|divider|divider|op_4~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_4~17_sumout\ = SUM(( !\B[0]~input_o\ ) + ( \A[4]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_4~22_cout\ ))
-- \Div0|auto_generated|divider|divider|op_4~18\ = CARRY(( !\B[0]~input_o\ ) + ( \A[4]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_4~22_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[4]~input_o\,
	datac => \ALT_INV_B[0]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_4~22_cout\,
	sumout => \Div0|auto_generated|divider|divider|op_4~17_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_4~18\);

-- Location: LABCELL_X117_Y18_N42
\Div0|auto_generated|divider|divider|op_4~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_4~13_sumout\ = SUM(( !\B[1]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(18) & 
-- (\Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~13_sumout\)) # (\Div0|auto_generated|divider|divider|sel\(18) & ((\A[5]~input_o\))))) # (\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & (((\A[5]~input_o\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_4~18\ ))
-- \Div0|auto_generated|divider|divider|op_4~14\ = CARRY(( !\B[1]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(18) & 
-- (\Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~13_sumout\)) # (\Div0|auto_generated|divider|divider|sel\(18) & ((\A[5]~input_o\))))) # (\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & (((\A[5]~input_o\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_4~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111101111000000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[3]~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[0]~13_sumout\,
	datad => \ALT_INV_B[1]~input_o\,
	dataf => \ALT_INV_A[5]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_4~18\,
	sumout => \Div0|auto_generated|divider|divider|op_4~13_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_4~14\);

-- Location: LABCELL_X117_Y18_N45
\Div0|auto_generated|divider|divider|op_4~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_4~9_sumout\ = SUM(( (!\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(18) & 
-- (\Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~9_sumout\)) # (\Div0|auto_generated|divider|divider|sel\(18) & ((\Div0|auto_generated|divider|divider|StageOut[8]~7_combout\))))) # 
-- (\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[8]~7_combout\)))) ) + ( !\B[2]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_4~14\ ))
-- \Div0|auto_generated|divider|divider|op_4~10\ = CARRY(( (!\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(18) & (\Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~9_sumout\)) 
-- # (\Div0|auto_generated|divider|divider|sel\(18) & ((\Div0|auto_generated|divider|divider|StageOut[8]~7_combout\))))) # (\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & 
-- (((\Div0|auto_generated|divider|divider|StageOut[8]~7_combout\)))) ) + ( !\B[2]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_4~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[3]~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[1]~9_sumout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[8]~7_combout\,
	dataf => \ALT_INV_B[2]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_4~14\,
	sumout => \Div0|auto_generated|divider|divider|op_4~9_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_4~10\);

-- Location: LABCELL_X117_Y18_N48
\Div0|auto_generated|divider|divider|op_4~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_4~5_sumout\ = SUM(( (!\Div0|auto_generated|divider|divider|selnose\(18) & (\Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~5_sumout\)) # (\Div0|auto_generated|divider|divider|selnose\(18) & 
-- (((\Div0|auto_generated|divider|divider|StageOut[9]~3_combout\) # (\Div0|auto_generated|divider|divider|StageOut[9]~1_combout\)))) ) + ( !\B[3]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_4~10\ ))
-- \Div0|auto_generated|divider|divider|op_4~6\ = CARRY(( (!\Div0|auto_generated|divider|divider|selnose\(18) & (\Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~5_sumout\)) # (\Div0|auto_generated|divider|divider|selnose\(18) & 
-- (((\Div0|auto_generated|divider|divider|StageOut[9]~3_combout\) # (\Div0|auto_generated|divider|divider|StageOut[9]~1_combout\)))) ) + ( !\B[3]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_4~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000100011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[2]~5_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_selnose\(18),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~1_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~3_combout\,
	dataf => \ALT_INV_B[3]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_4~10\,
	sumout => \Div0|auto_generated|divider|divider|op_4~5_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_4~6\);

-- Location: LABCELL_X117_Y18_N51
\Div0|auto_generated|divider|divider|op_4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_4~1_sumout\ = SUM(( VCC ) + ( GND ) + ( \Div0|auto_generated|divider|divider|op_4~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|op_4~6\,
	sumout => \Div0|auto_generated|divider|divider|op_4~1_sumout\);

-- Location: LABCELL_X115_Y17_N45
\Div0|auto_generated|divider|divider|sel[45]\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|sel\(45) = (\B[7]~input_o\) # (\B[6]~input_o\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111011101110111011101110111011101110111011101110111011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[6]~input_o\,
	datab => \ALT_INV_B[7]~input_o\,
	combout => \Div0|auto_generated|divider|divider|sel\(45));

-- Location: LABCELL_X115_Y18_N3
\Div0|auto_generated|divider|divider|sel[36]\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|sel\(36) = (\B[5]~input_o\) # (\Div0|auto_generated|divider|divider|sel\(45))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101011111010111110101111101011111010111110101111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \ALT_INV_B[5]~input_o\,
	combout => \Div0|auto_generated|divider|divider|sel\(36));

-- Location: LABCELL_X116_Y18_N6
\Div0|auto_generated|divider|divider|sel[27]\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|sel\(27) = ( \Div0|auto_generated|divider|divider|sel\(36) ) # ( !\Div0|auto_generated|divider|divider|sel\(36) & ( \B[4]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B[4]~input_o\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_sel\(36),
	combout => \Div0|auto_generated|divider|divider|sel\(27));

-- Location: MLABCELL_X119_Y18_N39
\Div0|auto_generated|divider|divider|StageOut[18]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[18]~4_combout\ = ( \Div0|auto_generated|divider|divider|StageOut[9]~3_combout\ & ( ((\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\) # 
-- (\Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~5_sumout\)) # (\Div0|auto_generated|divider|divider|sel\(18)) ) ) # ( !\Div0|auto_generated|divider|divider|StageOut[9]~3_combout\ & ( (!\Div0|auto_generated|divider|divider|sel\(18) & 
-- ((!\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & (\Div0|auto_generated|divider|divider|add_sub_2_result_int[2]~5_sumout\)) # (\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & 
-- ((\Div0|auto_generated|divider|divider|StageOut[9]~1_combout\))))) # (\Div0|auto_generated|divider|divider|sel\(18) & (((\Div0|auto_generated|divider|divider|StageOut[9]~1_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000001111111001000000111111101111111011111110111111101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	datab => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[2]~5_sumout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[3]~1_sumout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~1_combout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~3_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[18]~4_combout\);

-- Location: LABCELL_X117_Y18_N54
\Div0|auto_generated|divider|divider|StageOut[17]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[17]~8_combout\ = ( \Div0|auto_generated|divider|divider|sel\(18) & ( \Div0|auto_generated|divider|divider|StageOut[8]~7_combout\ ) ) # ( !\Div0|auto_generated|divider|divider|sel\(18) & ( 
-- (!\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & (\Div0|auto_generated|divider|divider|add_sub_2_result_int[1]~9_sumout\)) # (\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & 
-- ((\Div0|auto_generated|divider|divider|StageOut[8]~7_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000111111000011000011111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[3]~1_sumout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[1]~9_sumout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[8]~7_combout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	combout => \Div0|auto_generated|divider|divider|StageOut[17]~8_combout\);

-- Location: LABCELL_X117_Y18_N57
\Div0|auto_generated|divider|divider|StageOut[16]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[16]~13_combout\ = ( \A[5]~input_o\ & ( ((\Div0|auto_generated|divider|divider|sel\(18)) # (\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~13_sumout\) ) ) # ( !\A[5]~input_o\ & ( (\Div0|auto_generated|divider|divider|add_sub_2_result_int[0]~13_sumout\ & (!\Div0|auto_generated|divider|divider|add_sub_2_result_int[3]~1_sumout\ & 
-- !\Div0|auto_generated|divider|divider|sel\(18))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000001000000010000000100000001111111011111110111111101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[0]~13_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_add_sub_2_result_int[3]~1_sumout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_sel\(18),
	dataf => \ALT_INV_A[5]~input_o\,
	combout => \Div0|auto_generated|divider|divider|StageOut[16]~13_combout\);

-- Location: LABCELL_X117_Y18_N12
\Div0|auto_generated|divider|divider|op_5~26\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_5~26_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \Div0|auto_generated|divider|divider|op_5~26_cout\);

-- Location: LABCELL_X117_Y18_N15
\Div0|auto_generated|divider|divider|op_5~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_5~21_sumout\ = SUM(( !\B[0]~input_o\ ) + ( \A[3]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_5~26_cout\ ))
-- \Div0|auto_generated|divider|divider|op_5~22\ = CARRY(( !\B[0]~input_o\ ) + ( \A[3]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_5~26_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B[0]~input_o\,
	datac => \ALT_INV_A[3]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_5~26_cout\,
	sumout => \Div0|auto_generated|divider|divider|op_5~21_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_5~22\);

-- Location: LABCELL_X117_Y18_N18
\Div0|auto_generated|divider|divider|op_5~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_5~17_sumout\ = SUM(( !\B[1]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(27) & (\Div0|auto_generated|divider|divider|op_4~17_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(27) & ((\A[4]~input_o\))))) # (\Div0|auto_generated|divider|divider|op_4~1_sumout\ & (((\A[4]~input_o\)))) ) + ( \Div0|auto_generated|divider|divider|op_5~22\ ))
-- \Div0|auto_generated|divider|divider|op_5~18\ = CARRY(( !\B[1]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(27) & (\Div0|auto_generated|divider|divider|op_4~17_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(27) & ((\A[4]~input_o\))))) # (\Div0|auto_generated|divider|divider|op_4~1_sumout\ & (((\A[4]~input_o\)))) ) + ( \Div0|auto_generated|divider|divider|op_5~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111101111000000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(27),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_4~17_sumout\,
	datad => \ALT_INV_B[1]~input_o\,
	dataf => \ALT_INV_A[4]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_5~22\,
	sumout => \Div0|auto_generated|divider|divider|op_5~17_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_5~18\);

-- Location: LABCELL_X117_Y18_N21
\Div0|auto_generated|divider|divider|op_5~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_5~13_sumout\ = SUM(( !\B[2]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(27) & (\Div0|auto_generated|divider|divider|op_4~13_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(27) & ((\Div0|auto_generated|divider|divider|StageOut[16]~13_combout\))))) # (\Div0|auto_generated|divider|divider|op_4~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[16]~13_combout\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_5~18\ ))
-- \Div0|auto_generated|divider|divider|op_5~14\ = CARRY(( !\B[2]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(27) & (\Div0|auto_generated|divider|divider|op_4~13_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(27) & ((\Div0|auto_generated|divider|divider|StageOut[16]~13_combout\))))) # (\Div0|auto_generated|divider|divider|op_4~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[16]~13_combout\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_5~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111101111000000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(27),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_4~13_sumout\,
	datad => \ALT_INV_B[2]~input_o\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[16]~13_combout\,
	cin => \Div0|auto_generated|divider|divider|op_5~18\,
	sumout => \Div0|auto_generated|divider|divider|op_5~13_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_5~14\);

-- Location: LABCELL_X117_Y18_N24
\Div0|auto_generated|divider|divider|op_5~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_5~9_sumout\ = SUM(( (!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(27) & ((\Div0|auto_generated|divider|divider|op_4~9_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(27) & (\Div0|auto_generated|divider|divider|StageOut[17]~8_combout\)))) # (\Div0|auto_generated|divider|divider|op_4~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[17]~8_combout\)))) ) + ( 
-- !\B[3]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_5~14\ ))
-- \Div0|auto_generated|divider|divider|op_5~10\ = CARRY(( (!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(27) & ((\Div0|auto_generated|divider|divider|op_4~9_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(27) & (\Div0|auto_generated|divider|divider|StageOut[17]~8_combout\)))) # (\Div0|auto_generated|divider|divider|op_4~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[17]~8_combout\)))) ) + ( 
-- !\B[3]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_5~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(27),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[17]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_4~9_sumout\,
	dataf => \ALT_INV_B[3]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_5~14\,
	sumout => \Div0|auto_generated|divider|divider|op_5~9_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_5~10\);

-- Location: LABCELL_X117_Y18_N27
\Div0|auto_generated|divider|divider|op_5~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_5~5_sumout\ = SUM(( !\B[4]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(27) & ((\Div0|auto_generated|divider|divider|op_4~5_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(27) & (\Div0|auto_generated|divider|divider|StageOut[18]~4_combout\)))) # (\Div0|auto_generated|divider|divider|op_4~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[18]~4_combout\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_5~10\ ))
-- \Div0|auto_generated|divider|divider|op_5~6\ = CARRY(( !\B[4]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(27) & ((\Div0|auto_generated|divider|divider|op_4~5_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(27) & (\Div0|auto_generated|divider|divider|StageOut[18]~4_combout\)))) # (\Div0|auto_generated|divider|divider|op_4~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[18]~4_combout\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_5~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111110000111000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(27),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[18]~4_combout\,
	datad => \ALT_INV_B[4]~input_o\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_4~5_sumout\,
	cin => \Div0|auto_generated|divider|divider|op_5~10\,
	sumout => \Div0|auto_generated|divider|divider|op_5~5_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_5~6\);

-- Location: LABCELL_X117_Y18_N30
\Div0|auto_generated|divider|divider|op_5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_5~1_sumout\ = SUM(( VCC ) + ( GND ) + ( \Div0|auto_generated|divider|divider|op_5~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|op_5~6\,
	sumout => \Div0|auto_generated|divider|divider|op_5~1_sumout\);

-- Location: LABCELL_X116_Y18_N3
\Div0|auto_generated|divider|divider|selnose[36]\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|selnose\(36) = (\Div0|auto_generated|divider|divider|sel\(36)) # (\Div0|auto_generated|divider|divider|op_5~1_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111011101110111011101110111011101110111011101110111011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(36),
	combout => \Div0|auto_generated|divider|divider|selnose\(36));

-- Location: LABCELL_X116_Y18_N12
\Div0|auto_generated|divider|divider|StageOut[27]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[27]~0_combout\ = (!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & (!\Div0|auto_generated|divider|divider|sel\(27) & \Div0|auto_generated|divider|divider|op_4~5_sumout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010001000000000001000100000000000100010000000000010001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(27),
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_4~5_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[27]~0_combout\);

-- Location: LABCELL_X116_Y18_N9
\Div0|auto_generated|divider|divider|StageOut[27]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[27]~5_combout\ = ( \Div0|auto_generated|divider|divider|StageOut[18]~4_combout\ & ( (\Div0|auto_generated|divider|divider|sel\(27)) # (\Div0|auto_generated|divider|divider|op_4~1_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001011111010111110101111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_sel\(27),
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[18]~4_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[27]~5_combout\);

-- Location: LABCELL_X116_Y18_N15
\Div0|auto_generated|divider|divider|StageOut[26]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[26]~9_combout\ = ( \Div0|auto_generated|divider|divider|op_4~9_sumout\ & ( ((!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & !\Div0|auto_generated|divider|divider|sel\(27))) # 
-- (\Div0|auto_generated|divider|divider|StageOut[17]~8_combout\) ) ) # ( !\Div0|auto_generated|divider|divider|op_4~9_sumout\ & ( (\Div0|auto_generated|divider|divider|StageOut[17]~8_combout\ & ((\Div0|auto_generated|divider|divider|sel\(27)) # 
-- (\Div0|auto_generated|divider|divider|op_4~1_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011100000111000001110000011110001111100011111000111110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(27),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[17]~8_combout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_4~9_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[26]~9_combout\);

-- Location: LABCELL_X116_Y18_N54
\Div0|auto_generated|divider|divider|StageOut[25]~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[25]~14_combout\ = (!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(27) & ((\Div0|auto_generated|divider|divider|op_4~13_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(27) & (\Div0|auto_generated|divider|divider|StageOut[16]~13_combout\)))) # (\Div0|auto_generated|divider|divider|op_4~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[16]~13_combout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011110001111000001111000111100000111100011110000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(27),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[16]~13_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_4~13_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[25]~14_combout\);

-- Location: LABCELL_X116_Y18_N57
\Div0|auto_generated|divider|divider|StageOut[24]~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[24]~17_combout\ = ( \A[4]~input_o\ & ( ((\Div0|auto_generated|divider|divider|op_4~17_sumout\) # (\Div0|auto_generated|divider|divider|sel\(27))) # (\Div0|auto_generated|divider|divider|op_4~1_sumout\) ) ) # ( 
-- !\A[4]~input_o\ & ( (!\Div0|auto_generated|divider|divider|op_4~1_sumout\ & (!\Div0|auto_generated|divider|divider|sel\(27) & \Div0|auto_generated|divider|divider|op_4~17_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000000010000000100001111111011111110111111101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(27),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_4~17_sumout\,
	dataf => \ALT_INV_A[4]~input_o\,
	combout => \Div0|auto_generated|divider|divider|StageOut[24]~17_combout\);

-- Location: LABCELL_X116_Y18_N30
\Div0|auto_generated|divider|divider|op_6~30\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_6~30_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \Div0|auto_generated|divider|divider|op_6~30_cout\);

-- Location: LABCELL_X116_Y18_N33
\Div0|auto_generated|divider|divider|op_6~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_6~25_sumout\ = SUM(( !\B[0]~input_o\ ) + ( \A[2]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_6~30_cout\ ))
-- \Div0|auto_generated|divider|divider|op_6~26\ = CARRY(( !\B[0]~input_o\ ) + ( \A[2]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_6~30_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_A[2]~input_o\,
	datad => \ALT_INV_B[0]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_6~30_cout\,
	sumout => \Div0|auto_generated|divider|divider|op_6~25_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_6~26\);

-- Location: LABCELL_X116_Y18_N36
\Div0|auto_generated|divider|divider|op_6~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_6~21_sumout\ = SUM(( !\B[1]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(36) & (\Div0|auto_generated|divider|divider|op_5~21_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(36) & ((\A[3]~input_o\))))) # (\Div0|auto_generated|divider|divider|op_5~1_sumout\ & (((\A[3]~input_o\)))) ) + ( \Div0|auto_generated|divider|divider|op_6~26\ ))
-- \Div0|auto_generated|divider|divider|op_6~22\ = CARRY(( !\B[1]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(36) & (\Div0|auto_generated|divider|divider|op_5~21_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(36) & ((\A[3]~input_o\))))) # (\Div0|auto_generated|divider|divider|op_5~1_sumout\ & (((\A[3]~input_o\)))) ) + ( \Div0|auto_generated|divider|divider|op_6~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111101111000000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(36),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_5~21_sumout\,
	datad => \ALT_INV_B[1]~input_o\,
	dataf => \ALT_INV_A[3]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_6~26\,
	sumout => \Div0|auto_generated|divider|divider|op_6~21_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_6~22\);

-- Location: LABCELL_X116_Y18_N39
\Div0|auto_generated|divider|divider|op_6~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_6~17_sumout\ = SUM(( (!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(36) & (\Div0|auto_generated|divider|divider|op_5~17_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(36) & ((\Div0|auto_generated|divider|divider|StageOut[24]~17_combout\))))) # (\Div0|auto_generated|divider|divider|op_5~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[24]~17_combout\)))) ) + ( 
-- !\B[2]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_6~22\ ))
-- \Div0|auto_generated|divider|divider|op_6~18\ = CARRY(( (!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(36) & (\Div0|auto_generated|divider|divider|op_5~17_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(36) & ((\Div0|auto_generated|divider|divider|StageOut[24]~17_combout\))))) # (\Div0|auto_generated|divider|divider|op_5~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[24]~17_combout\)))) ) + ( 
-- !\B[2]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_6~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(36),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_5~17_sumout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[24]~17_combout\,
	dataf => \ALT_INV_B[2]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_6~22\,
	sumout => \Div0|auto_generated|divider|divider|op_6~17_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_6~18\);

-- Location: LABCELL_X116_Y18_N42
\Div0|auto_generated|divider|divider|op_6~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_6~13_sumout\ = SUM(( (!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(36) & ((\Div0|auto_generated|divider|divider|op_5~13_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(36) & (\Div0|auto_generated|divider|divider|StageOut[25]~14_combout\)))) # (\Div0|auto_generated|divider|divider|op_5~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[25]~14_combout\)))) ) + ( 
-- !\B[3]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_6~18\ ))
-- \Div0|auto_generated|divider|divider|op_6~14\ = CARRY(( (!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(36) & ((\Div0|auto_generated|divider|divider|op_5~13_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(36) & (\Div0|auto_generated|divider|divider|StageOut[25]~14_combout\)))) # (\Div0|auto_generated|divider|divider|op_5~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[25]~14_combout\)))) ) + ( 
-- !\B[3]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_6~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(36),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[25]~14_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_5~13_sumout\,
	dataf => \ALT_INV_B[3]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_6~18\,
	sumout => \Div0|auto_generated|divider|divider|op_6~13_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_6~14\);

-- Location: LABCELL_X116_Y18_N45
\Div0|auto_generated|divider|divider|op_6~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_6~9_sumout\ = SUM(( !\B[4]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(36) & ((\Div0|auto_generated|divider|divider|op_5~9_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(36) & (\Div0|auto_generated|divider|divider|StageOut[26]~9_combout\)))) # (\Div0|auto_generated|divider|divider|op_5~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[26]~9_combout\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_6~14\ ))
-- \Div0|auto_generated|divider|divider|op_6~10\ = CARRY(( !\B[4]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(36) & ((\Div0|auto_generated|divider|divider|op_5~9_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(36) & (\Div0|auto_generated|divider|divider|StageOut[26]~9_combout\)))) # (\Div0|auto_generated|divider|divider|op_5~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[26]~9_combout\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_6~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111110000111000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(36),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[26]~9_combout\,
	datad => \ALT_INV_B[4]~input_o\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_5~9_sumout\,
	cin => \Div0|auto_generated|divider|divider|op_6~14\,
	sumout => \Div0|auto_generated|divider|divider|op_6~9_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_6~10\);

-- Location: LABCELL_X116_Y18_N48
\Div0|auto_generated|divider|divider|op_6~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_6~5_sumout\ = SUM(( (!\Div0|auto_generated|divider|divider|selnose\(36) & (((\Div0|auto_generated|divider|divider|op_5~5_sumout\)))) # (\Div0|auto_generated|divider|divider|selnose\(36) & 
-- (((\Div0|auto_generated|divider|divider|StageOut[27]~5_combout\)) # (\Div0|auto_generated|divider|divider|StageOut[27]~0_combout\))) ) + ( !\B[5]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_6~10\ ))
-- \Div0|auto_generated|divider|divider|op_6~6\ = CARRY(( (!\Div0|auto_generated|divider|divider|selnose\(36) & (((\Div0|auto_generated|divider|divider|op_5~5_sumout\)))) # (\Div0|auto_generated|divider|divider|selnose\(36) & 
-- (((\Div0|auto_generated|divider|divider|StageOut[27]~5_combout\)) # (\Div0|auto_generated|divider|divider|StageOut[27]~0_combout\))) ) + ( !\B[5]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_6~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000001101101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_selnose\(36),
	datab => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~0_combout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_5~5_sumout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~5_combout\,
	dataf => \ALT_INV_B[5]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_6~10\,
	sumout => \Div0|auto_generated|divider|divider|op_6~5_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_6~6\);

-- Location: LABCELL_X116_Y18_N51
\Div0|auto_generated|divider|divider|op_6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_6~1_sumout\ = SUM(( VCC ) + ( GND ) + ( \Div0|auto_generated|divider|divider|op_6~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|op_6~6\,
	sumout => \Div0|auto_generated|divider|divider|op_6~1_sumout\);

-- Location: LABCELL_X116_Y18_N18
\Div0|auto_generated|divider|divider|StageOut[36]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[36]~6_combout\ = ( \Div0|auto_generated|divider|divider|StageOut[27]~0_combout\ & ( ((\Div0|auto_generated|divider|divider|op_5~5_sumout\) # (\Div0|auto_generated|divider|divider|sel\(36))) # 
-- (\Div0|auto_generated|divider|divider|op_5~1_sumout\) ) ) # ( !\Div0|auto_generated|divider|divider|StageOut[27]~0_combout\ & ( (!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(36) & 
-- ((\Div0|auto_generated|divider|divider|op_5~5_sumout\))) # (\Div0|auto_generated|divider|divider|sel\(36) & (\Div0|auto_generated|divider|divider|StageOut[27]~5_combout\)))) # (\Div0|auto_generated|divider|divider|op_5~1_sumout\ & 
-- (((\Div0|auto_generated|divider|divider|StageOut[27]~5_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011110001111000001111000111101110111111111110111011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(36),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~5_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_5~5_sumout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~0_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[36]~6_combout\);

-- Location: LABCELL_X116_Y18_N27
\Div0|auto_generated|divider|divider|StageOut[35]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[35]~10_combout\ = ( \Div0|auto_generated|divider|divider|op_5~9_sumout\ & ( ((!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & !\Div0|auto_generated|divider|divider|sel\(36))) # 
-- (\Div0|auto_generated|divider|divider|StageOut[26]~9_combout\) ) ) # ( !\Div0|auto_generated|divider|divider|op_5~9_sumout\ & ( (\Div0|auto_generated|divider|divider|StageOut[26]~9_combout\ & ((\Div0|auto_generated|divider|divider|sel\(36)) # 
-- (\Div0|auto_generated|divider|divider|op_5~1_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011100000111000001110000011110001111100011111000111110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(36),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[26]~9_combout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_5~9_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[35]~10_combout\);

-- Location: LABCELL_X116_Y18_N21
\Div0|auto_generated|divider|divider|StageOut[34]~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[34]~15_combout\ = ( \Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( \Div0|auto_generated|divider|divider|StageOut[25]~14_combout\ ) ) # ( !\Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( 
-- (!\Div0|auto_generated|divider|divider|sel\(36) & ((\Div0|auto_generated|divider|divider|op_5~13_sumout\))) # (\Div0|auto_generated|divider|divider|sel\(36) & (\Div0|auto_generated|divider|divider|StageOut[25]~14_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111001111000000111100111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(36),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[25]~14_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_5~13_sumout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[34]~15_combout\);

-- Location: LABCELL_X116_Y18_N24
\Div0|auto_generated|divider|divider|StageOut[33]~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[33]~18_combout\ = ( \Div0|auto_generated|divider|divider|op_5~17_sumout\ & ( ((!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & !\Div0|auto_generated|divider|divider|sel\(36))) # 
-- (\Div0|auto_generated|divider|divider|StageOut[24]~17_combout\) ) ) # ( !\Div0|auto_generated|divider|divider|op_5~17_sumout\ & ( (\Div0|auto_generated|divider|divider|StageOut[24]~17_combout\ & ((\Div0|auto_generated|divider|divider|sel\(36)) # 
-- (\Div0|auto_generated|divider|divider|op_5~1_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001110111000000000111011110001000111111111000100011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(36),
	datad => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[24]~17_combout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_5~17_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[33]~18_combout\);

-- Location: LABCELL_X116_Y18_N0
\Div0|auto_generated|divider|divider|StageOut[32]~20\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[32]~20_combout\ = ( \A[3]~input_o\ & ( ((\Div0|auto_generated|divider|divider|op_5~21_sumout\) # (\Div0|auto_generated|divider|divider|sel\(36))) # (\Div0|auto_generated|divider|divider|op_5~1_sumout\) ) ) # ( 
-- !\A[3]~input_o\ & ( (!\Div0|auto_generated|divider|divider|op_5~1_sumout\ & (!\Div0|auto_generated|divider|divider|sel\(36) & \Div0|auto_generated|divider|divider|op_5~21_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000000010000000100001111111011111110111111101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(36),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_5~21_sumout\,
	dataf => \ALT_INV_A[3]~input_o\,
	combout => \Div0|auto_generated|divider|divider|StageOut[32]~20_combout\);

-- Location: LABCELL_X115_Y18_N18
\Div0|auto_generated|divider|divider|op_7~34\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_7~34_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \Div0|auto_generated|divider|divider|op_7~34_cout\);

-- Location: LABCELL_X115_Y18_N21
\Div0|auto_generated|divider|divider|op_7~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_7~29_sumout\ = SUM(( !\B[0]~input_o\ ) + ( \A[1]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_7~34_cout\ ))
-- \Div0|auto_generated|divider|divider|op_7~30\ = CARRY(( !\B[0]~input_o\ ) + ( \A[1]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_7~34_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[1]~input_o\,
	datac => \ALT_INV_B[0]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_7~34_cout\,
	sumout => \Div0|auto_generated|divider|divider|op_7~29_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_7~30\);

-- Location: LABCELL_X115_Y18_N24
\Div0|auto_generated|divider|divider|op_7~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_7~25_sumout\ = SUM(( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|op_6~25_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & ((\A[2]~input_o\))))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\A[2]~input_o\)))) ) + ( !\B[1]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_7~30\ ))
-- \Div0|auto_generated|divider|divider|op_7~26\ = CARRY(( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|op_6~25_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & ((\A[2]~input_o\))))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\A[2]~input_o\)))) ) + ( !\B[1]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_7~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_6~25_sumout\,
	datad => \ALT_INV_A[2]~input_o\,
	dataf => \ALT_INV_B[1]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_7~30\,
	sumout => \Div0|auto_generated|divider|divider|op_7~25_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_7~26\);

-- Location: LABCELL_X115_Y18_N27
\Div0|auto_generated|divider|divider|op_7~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_7~21_sumout\ = SUM(( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|op_6~21_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|StageOut[32]~20_combout\))))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[32]~20_combout\)))) ) + ( 
-- !\B[2]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_7~26\ ))
-- \Div0|auto_generated|divider|divider|op_7~22\ = CARRY(( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|op_6~21_sumout\)) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|StageOut[32]~20_combout\))))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[32]~20_combout\)))) ) + ( 
-- !\B[2]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_7~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_6~21_sumout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[32]~20_combout\,
	dataf => \ALT_INV_B[2]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_7~26\,
	sumout => \Div0|auto_generated|divider|divider|op_7~21_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_7~22\);

-- Location: LABCELL_X115_Y18_N30
\Div0|auto_generated|divider|divider|op_7~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_7~17_sumout\ = SUM(( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|op_6~17_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|StageOut[33]~18_combout\)))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[33]~18_combout\)))) ) + ( 
-- !\B[3]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_7~22\ ))
-- \Div0|auto_generated|divider|divider|op_7~18\ = CARRY(( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|op_6~17_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|StageOut[33]~18_combout\)))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[33]~18_combout\)))) ) + ( 
-- !\B[3]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_7~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[33]~18_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_6~17_sumout\,
	dataf => \ALT_INV_B[3]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_7~22\,
	sumout => \Div0|auto_generated|divider|divider|op_7~17_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_7~18\);

-- Location: LABCELL_X115_Y18_N33
\Div0|auto_generated|divider|divider|op_7~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_7~13_sumout\ = SUM(( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|op_6~13_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|StageOut[34]~15_combout\)))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[34]~15_combout\)))) ) + ( 
-- !\B[4]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_7~18\ ))
-- \Div0|auto_generated|divider|divider|op_7~14\ = CARRY(( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|op_6~13_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|StageOut[34]~15_combout\)))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[34]~15_combout\)))) ) + ( 
-- !\B[4]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_7~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[34]~15_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_6~13_sumout\,
	dataf => \ALT_INV_B[4]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_7~18\,
	sumout => \Div0|auto_generated|divider|divider|op_7~13_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_7~14\);

-- Location: LABCELL_X115_Y18_N36
\Div0|auto_generated|divider|divider|op_7~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_7~9_sumout\ = SUM(( !\B[5]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|op_6~9_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|StageOut[35]~10_combout\)))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[35]~10_combout\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_7~14\ ))
-- \Div0|auto_generated|divider|divider|op_7~10\ = CARRY(( !\B[5]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|op_6~9_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|StageOut[35]~10_combout\)))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[35]~10_combout\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_7~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111110000111000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[35]~10_combout\,
	datad => \ALT_INV_B[5]~input_o\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_6~9_sumout\,
	cin => \Div0|auto_generated|divider|divider|op_7~14\,
	sumout => \Div0|auto_generated|divider|divider|op_7~9_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_7~10\);

-- Location: LABCELL_X115_Y18_N39
\Div0|auto_generated|divider|divider|op_7~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_7~5_sumout\ = SUM(( !\B[6]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|op_6~5_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|StageOut[36]~6_combout\)))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[36]~6_combout\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_7~10\ ))
-- \Div0|auto_generated|divider|divider|op_7~6\ = CARRY(( !\B[6]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((!\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|op_6~5_sumout\))) # 
-- (\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|StageOut[36]~6_combout\)))) # (\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[36]~6_combout\)))) ) + ( 
-- \Div0|auto_generated|divider|divider|op_7~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111110000111000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[36]~6_combout\,
	datad => \ALT_INV_B[6]~input_o\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_6~5_sumout\,
	cin => \Div0|auto_generated|divider|divider|op_7~10\,
	sumout => \Div0|auto_generated|divider|divider|op_7~5_sumout\,
	cout => \Div0|auto_generated|divider|divider|op_7~6\);

-- Location: LABCELL_X115_Y18_N42
\Div0|auto_generated|divider|divider|op_7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_7~1_sumout\ = SUM(( VCC ) + ( GND ) + ( \Div0|auto_generated|divider|divider|op_7~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|op_7~6\,
	sumout => \Div0|auto_generated|divider|divider|op_7~1_sumout\);

-- Location: MLABCELL_X114_Y18_N3
\Div0|auto_generated|divider|divider|selnose[54]\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|selnose\(54) = ( \Div0|auto_generated|divider|divider|op_7~1_sumout\ ) # ( !\Div0|auto_generated|divider|divider|op_7~1_sumout\ & ( \B[7]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[7]~input_o\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	combout => \Div0|auto_generated|divider|divider|selnose\(54));

-- Location: LABCELL_X115_Y18_N9
\Div0|auto_generated|divider|divider|StageOut[45]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[45]~11_combout\ = ( !\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( (!\Div0|auto_generated|divider|divider|sel\(45) & \Div0|auto_generated|divider|divider|op_6~5_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000010100000101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_6~5_sumout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[45]~11_combout\);

-- Location: LABCELL_X115_Y18_N0
\Div0|auto_generated|divider|divider|StageOut[45]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[45]~12_combout\ = (\Div0|auto_generated|divider|divider|StageOut[36]~6_combout\ & ((\Div0|auto_generated|divider|divider|op_6~1_sumout\) # (\Div0|auto_generated|divider|divider|sel\(45))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001011111000000000101111100000000010111110000000001011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[36]~6_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[45]~12_combout\);

-- Location: LABCELL_X115_Y18_N15
\Div0|auto_generated|divider|divider|StageOut[44]~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[44]~16_combout\ = ( \Div0|auto_generated|divider|divider|op_6~9_sumout\ & ( ((!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & !\Div0|auto_generated|divider|divider|sel\(45))) # 
-- (\Div0|auto_generated|divider|divider|StageOut[35]~10_combout\) ) ) # ( !\Div0|auto_generated|divider|divider|op_6~9_sumout\ & ( (\Div0|auto_generated|divider|divider|StageOut[35]~10_combout\ & ((\Div0|auto_generated|divider|divider|sel\(45)) # 
-- (\Div0|auto_generated|divider|divider|op_6~1_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001110111000000000111011110001000111111111000100011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datad => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[35]~10_combout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_6~9_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[44]~16_combout\);

-- Location: LABCELL_X115_Y18_N12
\Div0|auto_generated|divider|divider|StageOut[43]~19\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[43]~19_combout\ = ( \Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( \Div0|auto_generated|divider|divider|StageOut[34]~15_combout\ ) ) # ( !\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( 
-- (!\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|op_6~13_sumout\))) # (\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|StageOut[34]~15_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111001111000000111100111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[34]~15_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_6~13_sumout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[43]~19_combout\);

-- Location: LABCELL_X115_Y18_N51
\Div0|auto_generated|divider|divider|StageOut[42]~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[42]~21_combout\ = ( \Div0|auto_generated|divider|divider|StageOut[33]~18_combout\ & ( ((\Div0|auto_generated|divider|divider|op_6~17_sumout\) # (\Div0|auto_generated|divider|divider|sel\(45))) # 
-- (\Div0|auto_generated|divider|divider|op_6~1_sumout\) ) ) # ( !\Div0|auto_generated|divider|divider|StageOut[33]~18_combout\ & ( (!\Div0|auto_generated|divider|divider|op_6~1_sumout\ & (!\Div0|auto_generated|divider|divider|sel\(45) & 
-- \Div0|auto_generated|divider|divider|op_6~17_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000000010000000100001111111011111110111111101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_6~17_sumout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[33]~18_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[42]~21_combout\);

-- Location: LABCELL_X115_Y18_N48
\Div0|auto_generated|divider|divider|StageOut[41]~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[41]~22_combout\ = ( \Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( \Div0|auto_generated|divider|divider|StageOut[32]~20_combout\ ) ) # ( !\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( 
-- (!\Div0|auto_generated|divider|divider|sel\(45) & ((\Div0|auto_generated|divider|divider|op_6~21_sumout\))) # (\Div0|auto_generated|divider|divider|sel\(45) & (\Div0|auto_generated|divider|divider|StageOut[32]~20_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111001111000000111100111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[32]~20_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_6~21_sumout\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[41]~22_combout\);

-- Location: LABCELL_X115_Y18_N6
\Div0|auto_generated|divider|divider|StageOut[40]~23\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[40]~23_combout\ = ( \Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( \A[2]~input_o\ ) ) # ( !\Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( (!\Div0|auto_generated|divider|divider|sel\(45) & 
-- (\Div0|auto_generated|divider|divider|op_6~25_sumout\)) # (\Div0|auto_generated|divider|divider|sel\(45) & ((\A[2]~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101001011111000010100101111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_6~25_sumout\,
	datad => \ALT_INV_A[2]~input_o\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[40]~23_combout\);

-- Location: MLABCELL_X114_Y18_N6
\Div0|auto_generated|divider|divider|op_8~38\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_8~38_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \Div0|auto_generated|divider|divider|op_8~38_cout\);

-- Location: MLABCELL_X114_Y18_N9
\Div0|auto_generated|divider|divider|op_8~34\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_8~34_cout\ = CARRY(( !\B[0]~input_o\ ) + ( \A[0]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_8~38_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[0]~input_o\,
	datac => \ALT_INV_B[0]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_8~38_cout\,
	cout => \Div0|auto_generated|divider|divider|op_8~34_cout\);

-- Location: MLABCELL_X114_Y18_N12
\Div0|auto_generated|divider|divider|op_8~30\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_8~30_cout\ = CARRY(( (!\Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((!\B[7]~input_o\ & (\Div0|auto_generated|divider|divider|op_7~29_sumout\)) # (\B[7]~input_o\ & ((\A[1]~input_o\))))) # 
-- (\Div0|auto_generated|divider|divider|op_7~1_sumout\ & (((\A[1]~input_o\)))) ) + ( !\B[1]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_8~34_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datab => \ALT_INV_B[7]~input_o\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_7~29_sumout\,
	datad => \ALT_INV_A[1]~input_o\,
	dataf => \ALT_INV_B[1]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_8~34_cout\,
	cout => \Div0|auto_generated|divider|divider|op_8~30_cout\);

-- Location: MLABCELL_X114_Y18_N15
\Div0|auto_generated|divider|divider|op_8~26\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_8~26_cout\ = CARRY(( !\B[2]~input_o\ ) + ( (!\Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((!\B[7]~input_o\ & (\Div0|auto_generated|divider|divider|op_7~25_sumout\)) # (\B[7]~input_o\ & 
-- ((\Div0|auto_generated|divider|divider|StageOut[40]~23_combout\))))) # (\Div0|auto_generated|divider|divider|op_7~1_sumout\ & (((\Div0|auto_generated|divider|divider|StageOut[40]~23_combout\)))) ) + ( \Div0|auto_generated|divider|divider|op_8~30_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111101111000000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datab => \ALT_INV_B[7]~input_o\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_op_7~25_sumout\,
	datad => \ALT_INV_B[2]~input_o\,
	dataf => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[40]~23_combout\,
	cin => \Div0|auto_generated|divider|divider|op_8~30_cout\,
	cout => \Div0|auto_generated|divider|divider|op_8~26_cout\);

-- Location: MLABCELL_X114_Y18_N18
\Div0|auto_generated|divider|divider|op_8~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_8~22_cout\ = CARRY(( (!\B[7]~input_o\ & ((!\Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((\Div0|auto_generated|divider|divider|op_7~21_sumout\))) # (\Div0|auto_generated|divider|divider|op_7~1_sumout\ & 
-- (\Div0|auto_generated|divider|divider|StageOut[41]~22_combout\)))) # (\B[7]~input_o\ & (((\Div0|auto_generated|divider|divider|StageOut[41]~22_combout\)))) ) + ( !\B[3]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_8~26_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[7]~input_o\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[41]~22_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_7~21_sumout\,
	dataf => \ALT_INV_B[3]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_8~26_cout\,
	cout => \Div0|auto_generated|divider|divider|op_8~22_cout\);

-- Location: MLABCELL_X114_Y18_N21
\Div0|auto_generated|divider|divider|op_8~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_8~18_cout\ = CARRY(( (!\B[7]~input_o\ & ((!\Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((\Div0|auto_generated|divider|divider|op_7~17_sumout\))) # (\Div0|auto_generated|divider|divider|op_7~1_sumout\ & 
-- (\Div0|auto_generated|divider|divider|StageOut[42]~21_combout\)))) # (\B[7]~input_o\ & (((\Div0|auto_generated|divider|divider|StageOut[42]~21_combout\)))) ) + ( !\B[4]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_8~22_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[7]~input_o\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[42]~21_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_7~17_sumout\,
	dataf => \ALT_INV_B[4]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_8~22_cout\,
	cout => \Div0|auto_generated|divider|divider|op_8~18_cout\);

-- Location: MLABCELL_X114_Y18_N24
\Div0|auto_generated|divider|divider|op_8~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_8~14_cout\ = CARRY(( (!\B[7]~input_o\ & ((!\Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((\Div0|auto_generated|divider|divider|op_7~13_sumout\))) # (\Div0|auto_generated|divider|divider|op_7~1_sumout\ & 
-- (\Div0|auto_generated|divider|divider|StageOut[43]~19_combout\)))) # (\B[7]~input_o\ & (((\Div0|auto_generated|divider|divider|StageOut[43]~19_combout\)))) ) + ( !\B[5]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_8~18_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[7]~input_o\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[43]~19_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_7~13_sumout\,
	dataf => \ALT_INV_B[5]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_8~18_cout\,
	cout => \Div0|auto_generated|divider|divider|op_8~14_cout\);

-- Location: MLABCELL_X114_Y18_N27
\Div0|auto_generated|divider|divider|op_8~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_8~10_cout\ = CARRY(( (!\B[7]~input_o\ & ((!\Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((\Div0|auto_generated|divider|divider|op_7~9_sumout\))) # (\Div0|auto_generated|divider|divider|op_7~1_sumout\ & 
-- (\Div0|auto_generated|divider|divider|StageOut[44]~16_combout\)))) # (\B[7]~input_o\ & (((\Div0|auto_generated|divider|divider|StageOut[44]~16_combout\)))) ) + ( !\B[6]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_8~14_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[7]~input_o\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[44]~16_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_7~9_sumout\,
	dataf => \ALT_INV_B[6]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_8~14_cout\,
	cout => \Div0|auto_generated|divider|divider|op_8~10_cout\);

-- Location: MLABCELL_X114_Y18_N30
\Div0|auto_generated|divider|divider|op_8~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_8~6_cout\ = CARRY(( (!\Div0|auto_generated|divider|divider|selnose\(54) & (\Div0|auto_generated|divider|divider|op_7~5_sumout\)) # (\Div0|auto_generated|divider|divider|selnose\(54) & 
-- (((\Div0|auto_generated|divider|divider|StageOut[45]~12_combout\) # (\Div0|auto_generated|divider|divider|StageOut[45]~11_combout\)))) ) + ( !\B[7]~input_o\ ) + ( \Div0|auto_generated|divider|divider|op_8~10_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000010011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_selnose\(54),
	datab => \Div0|auto_generated|divider|divider|ALT_INV_op_7~5_sumout\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~11_combout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~12_combout\,
	dataf => \ALT_INV_B[7]~input_o\,
	cin => \Div0|auto_generated|divider|divider|op_8~10_cout\,
	cout => \Div0|auto_generated|divider|divider|op_8~6_cout\);

-- Location: MLABCELL_X114_Y18_N33
\Div0|auto_generated|divider|divider|op_8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|op_8~1_sumout\ = SUM(( VCC ) + ( GND ) + ( \Div0|auto_generated|divider|divider|op_8~6_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|op_8~6_cout\,
	sumout => \Div0|auto_generated|divider|divider|op_8~1_sumout\);

-- Location: DSP_X113_Y17_N0
\Mult0~8\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 8,
	ay_scan_in_clock => "none",
	ay_scan_in_width => 8,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	by_clock => "none",
	by_use_scan_in => "false",
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m9x9",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "false",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	ax => \Mult0~8_AX_bus\,
	ay => \Mult0~8_AY_bus\,
	resulta => \Mult0~8_RESULTA_bus\);

-- Location: IOIBUF_X121_Y13_N78
\OP[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_OP(0),
	o => \OP[0]~input_o\);

-- Location: IOIBUF_X84_Y0_N1
\OP[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_OP(2),
	o => \OP[2]~input_o\);

-- Location: LABCELL_X115_Y17_N0
\Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~33_sumout\ = SUM(( \A[0]~input_o\ ) + ( \B[0]~input_o\ ) + ( !VCC ))
-- \Add0~34\ = CARRY(( \A[0]~input_o\ ) + ( \B[0]~input_o\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_A[0]~input_o\,
	dataf => \ALT_INV_B[0]~input_o\,
	cin => GND,
	sumout => \Add0~33_sumout\,
	cout => \Add0~34\);

-- Location: LABCELL_X115_Y17_N30
\Mux7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux7~0_combout\ = ( !\OP[1]~input_o\ & ( ((!\OP[0]~input_o\ & (((\Add0~33_sumout\ & !\OP[2]~input_o\)))) # (\OP[0]~input_o\ & (((\OP[2]~input_o\)) # (\Add1~29_sumout\)))) ) ) # ( \OP[1]~input_o\ & ( ((!\OP[0]~input_o\ & (((\Mult0~8_resulta\ & 
-- !\OP[2]~input_o\)))) # (\OP[0]~input_o\ & ((!\Div0|auto_generated|divider|divider|op_8~1_sumout\) # ((\OP[2]~input_o\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000111101010101000011111100110000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add1~29_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_op_8~1_sumout\,
	datac => \ALT_INV_Mult0~8_resulta\,
	datad => \ALT_INV_OP[0]~input_o\,
	datae => \ALT_INV_OP[1]~input_o\,
	dataf => \ALT_INV_OP[2]~input_o\,
	datag => \ALT_INV_Add0~33_sumout\,
	combout => \Mux7~0_combout\);

-- Location: MLABCELL_X114_Y18_N36
\LessThan0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~0_combout\ = ( \B[0]~input_o\ & ( (!\B[1]~input_o\ & \A[1]~input_o\) ) ) # ( !\B[0]~input_o\ & ( (!\A[0]~input_o\ & (!\B[1]~input_o\ & \A[1]~input_o\)) # (\A[0]~input_o\ & ((!\B[1]~input_o\) # (\A[1]~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000011110101010100001111010100000000111100000000000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[0]~input_o\,
	datac => \ALT_INV_B[1]~input_o\,
	datad => \ALT_INV_A[1]~input_o\,
	dataf => \ALT_INV_B[0]~input_o\,
	combout => \LessThan0~0_combout\);

-- Location: MLABCELL_X114_Y18_N51
\LessThan0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~1_combout\ = ( \B[2]~input_o\ & ( (!\B[3]~input_o\ & (((\A[2]~input_o\ & \LessThan0~0_combout\)) # (\A[3]~input_o\))) # (\B[3]~input_o\ & (\A[3]~input_o\ & (\A[2]~input_o\ & \LessThan0~0_combout\))) ) ) # ( !\B[2]~input_o\ & ( (!\B[3]~input_o\ 
-- & (((\LessThan0~0_combout\) # (\A[2]~input_o\)) # (\A[3]~input_o\))) # (\B[3]~input_o\ & (\A[3]~input_o\ & ((\LessThan0~0_combout\) # (\A[2]~input_o\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010101110111011001010111011101100100010001010110010001000101011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[3]~input_o\,
	datab => \ALT_INV_A[3]~input_o\,
	datac => \ALT_INV_A[2]~input_o\,
	datad => \ALT_INV_LessThan0~0_combout\,
	dataf => \ALT_INV_B[2]~input_o\,
	combout => \LessThan0~1_combout\);

-- Location: MLABCELL_X114_Y18_N57
\LessThan0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~2_combout\ = ( \LessThan0~1_combout\ & ( (!\B[5]~input_o\ & (((!\B[4]~input_o\) # (\A[4]~input_o\)) # (\A[5]~input_o\))) # (\B[5]~input_o\ & (\A[5]~input_o\ & ((!\B[4]~input_o\) # (\A[4]~input_o\)))) ) ) # ( !\LessThan0~1_combout\ & ( 
-- (!\B[5]~input_o\ & (((!\B[4]~input_o\ & \A[4]~input_o\)) # (\A[5]~input_o\))) # (\B[5]~input_o\ & (\A[5]~input_o\ & (!\B[4]~input_o\ & \A[4]~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001010110010001000101011001010110010101110111011001010111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[5]~input_o\,
	datab => \ALT_INV_A[5]~input_o\,
	datac => \ALT_INV_B[4]~input_o\,
	datad => \ALT_INV_A[4]~input_o\,
	dataf => \ALT_INV_LessThan0~1_combout\,
	combout => \LessThan0~2_combout\);

-- Location: LABCELL_X115_Y17_N42
\LessThan0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~3_combout\ = ( \A[6]~input_o\ & ( (!\B[7]~input_o\ & ((!\B[6]~input_o\) # ((\A[7]~input_o\) # (\LessThan0~2_combout\)))) # (\B[7]~input_o\ & (\A[7]~input_o\ & ((!\B[6]~input_o\) # (\LessThan0~2_combout\)))) ) ) # ( !\A[6]~input_o\ & ( 
-- (!\B[7]~input_o\ & (((!\B[6]~input_o\ & \LessThan0~2_combout\)) # (\A[7]~input_o\))) # (\B[7]~input_o\ & (!\B[6]~input_o\ & (\LessThan0~2_combout\ & \A[7]~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100011001110000010001100111010001100111011111000110011101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[6]~input_o\,
	datab => \ALT_INV_B[7]~input_o\,
	datac => \ALT_INV_LessThan0~2_combout\,
	datad => \ALT_INV_A[7]~input_o\,
	dataf => \ALT_INV_A[6]~input_o\,
	combout => \LessThan0~3_combout\);

-- Location: LABCELL_X115_Y17_N36
\Mux7~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux7~4_combout\ = ( !\Mux7~0_combout\ & ( (\OP[2]~input_o\ & ((!\OP[1]~input_o\ & (\B[0]~input_o\ & (\A[0]~input_o\))) # (\OP[1]~input_o\ & (((\LessThan0~3_combout\)))))) ) ) # ( \Mux7~0_combout\ & ( (!\OP[2]~input_o\) # ((!\OP[1]~input_o\ & 
-- (((\A[0]~input_o\)) # (\B[0]~input_o\))) # (\OP[1]~input_o\ & (((\Equal0~3_combout\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000000000000000111111111111111100010001000011110111011100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[0]~input_o\,
	datab => \ALT_INV_A[0]~input_o\,
	datac => \ALT_INV_Equal0~3_combout\,
	datad => \ALT_INV_OP[1]~input_o\,
	datae => \ALT_INV_Mux7~0_combout\,
	dataf => \ALT_INV_OP[2]~input_o\,
	datag => \ALT_INV_LessThan0~3_combout\,
	combout => \Mux7~4_combout\);

-- Location: MLABCELL_X114_Y17_N33
\Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~1_sumout\ = SUM(( !\B[1]~input_o\ $ (\A[1]~input_o\) ) + ( \Add1~31\ ) + ( \Add1~30\ ))
-- \Add1~2\ = CARRY(( !\B[1]~input_o\ $ (\A[1]~input_o\) ) + ( \Add1~31\ ) + ( \Add1~30\ ))
-- \Add1~3\ = SHARE((!\B[1]~input_o\ & \A[1]~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000010100000101000000000000000001010010110100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[1]~input_o\,
	datac => \ALT_INV_A[1]~input_o\,
	cin => \Add1~30\,
	sharein => \Add1~31\,
	sumout => \Add1~1_sumout\,
	cout => \Add1~2\,
	shareout => \Add1~3\);

-- Location: LABCELL_X115_Y17_N3
\Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~1_sumout\ = SUM(( \B[1]~input_o\ ) + ( \A[1]~input_o\ ) + ( \Add0~34\ ))
-- \Add0~2\ = CARRY(( \B[1]~input_o\ ) + ( \A[1]~input_o\ ) + ( \Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[1]~input_o\,
	datac => \ALT_INV_A[1]~input_o\,
	cin => \Add0~34\,
	sumout => \Add0~1_sumout\,
	cout => \Add0~2\);

-- Location: MLABCELL_X114_Y17_N6
\Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux6~0_combout\ = ( \OP[0]~input_o\ & ( \OP[1]~input_o\ & ( !\Div0|auto_generated|divider|divider|selnose\(54) ) ) ) # ( !\OP[0]~input_o\ & ( \OP[1]~input_o\ & ( \Mult0~9\ ) ) ) # ( \OP[0]~input_o\ & ( !\OP[1]~input_o\ & ( \Add1~1_sumout\ ) ) ) # ( 
-- !\OP[0]~input_o\ & ( !\OP[1]~input_o\ & ( \Add0~1_sumout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000011110000111100110011001100111010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_selnose\(54),
	datab => \ALT_INV_Mult0~9\,
	datac => \ALT_INV_Add1~1_sumout\,
	datad => \ALT_INV_Add0~1_sumout\,
	datae => \ALT_INV_OP[0]~input_o\,
	dataf => \ALT_INV_OP[1]~input_o\,
	combout => \Mux6~0_combout\);

-- Location: MLABCELL_X114_Y17_N12
\Mux6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux6~1_combout\ = ( \OP[0]~input_o\ & ( \B[1]~input_o\ & ( (!\OP[2]~input_o\ & ((\Mux6~0_combout\))) # (\OP[2]~input_o\ & (!\OP[1]~input_o\)) ) ) ) # ( !\OP[0]~input_o\ & ( \B[1]~input_o\ & ( (!\OP[2]~input_o\ & (((\Mux6~0_combout\)))) # (\OP[2]~input_o\ 
-- & (\A[1]~input_o\ & (!\OP[1]~input_o\))) ) ) ) # ( \OP[0]~input_o\ & ( !\B[1]~input_o\ & ( (!\OP[2]~input_o\ & (((\Mux6~0_combout\)))) # (\OP[2]~input_o\ & (\A[1]~input_o\ & (!\OP[1]~input_o\))) ) ) ) # ( !\OP[0]~input_o\ & ( !\B[1]~input_o\ & ( 
-- (!\OP[2]~input_o\ & \Mux6~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011001100000100001101110000010000110111000011000011111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[1]~input_o\,
	datab => \ALT_INV_OP[2]~input_o\,
	datac => \ALT_INV_OP[1]~input_o\,
	datad => \ALT_INV_Mux6~0_combout\,
	datae => \ALT_INV_OP[0]~input_o\,
	dataf => \ALT_INV_B[1]~input_o\,
	combout => \Mux6~1_combout\);

-- Location: MLABCELL_X114_Y17_N36
\Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~5_sumout\ = SUM(( !\A[2]~input_o\ $ (\B[2]~input_o\) ) + ( \Add1~3\ ) + ( \Add1~2\ ))
-- \Add1~6\ = CARRY(( !\A[2]~input_o\ $ (\B[2]~input_o\) ) + ( \Add1~3\ ) + ( \Add1~2\ ))
-- \Add1~7\ = SHARE((\A[2]~input_o\ & !\B[2]~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000000000000000000000001111000000001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_A[2]~input_o\,
	datad => \ALT_INV_B[2]~input_o\,
	cin => \Add1~2\,
	sharein => \Add1~3\,
	sumout => \Add1~5_sumout\,
	cout => \Add1~6\,
	shareout => \Add1~7\);

-- Location: LABCELL_X115_Y17_N6
\Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~5_sumout\ = SUM(( \B[2]~input_o\ ) + ( \A[2]~input_o\ ) + ( \Add0~2\ ))
-- \Add0~6\ = CARRY(( \B[2]~input_o\ ) + ( \A[2]~input_o\ ) + ( \Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[2]~input_o\,
	datac => \ALT_INV_A[2]~input_o\,
	cin => \Add0~2\,
	sumout => \Add0~5_sumout\,
	cout => \Add0~6\);

-- Location: LABCELL_X112_Y17_N33
\Mux5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux5~1_combout\ = ( !\OP[1]~input_o\ & ( ((!\OP[0]~input_o\ & (\Add0~5_sumout\)) # (\OP[0]~input_o\ & (((\Add1~5_sumout\))))) ) ) # ( \OP[1]~input_o\ & ( (!\OP[0]~input_o\ & (\Mult0~10\)) # (\OP[0]~input_o\ & 
-- (((!\Div0|auto_generated|divider|divider|sel\(45) & (!\Div0|auto_generated|divider|divider|op_6~1_sumout\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000110000001100011101000100010000111111001111110111010001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Mult0~10\,
	datab => \ALT_INV_OP[0]~input_o\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_sel\(45),
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datae => \ALT_INV_OP[1]~input_o\,
	dataf => \ALT_INV_Add1~5_sumout\,
	datag => \ALT_INV_Add0~5_sumout\,
	combout => \Mux5~1_combout\);

-- Location: LABCELL_X116_Y17_N0
\Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux5~0_combout\ = ( \OP[1]~input_o\ & ( \B[2]~input_o\ & ( (!\OP[2]~input_o\ & \Mux5~1_combout\) ) ) ) # ( !\OP[1]~input_o\ & ( \B[2]~input_o\ & ( (!\OP[2]~input_o\ & (((\Mux5~1_combout\)))) # (\OP[2]~input_o\ & (((\A[2]~input_o\)) # (\OP[0]~input_o\))) 
-- ) ) ) # ( \OP[1]~input_o\ & ( !\B[2]~input_o\ & ( (!\OP[2]~input_o\ & \Mux5~1_combout\) ) ) ) # ( !\OP[1]~input_o\ & ( !\B[2]~input_o\ & ( (!\OP[2]~input_o\ & (((\Mux5~1_combout\)))) # (\OP[2]~input_o\ & (\OP[0]~input_o\ & ((\A[2]~input_o\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000011101000011000000110000011101001111110000110000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \ALT_INV_OP[2]~input_o\,
	datac => \ALT_INV_Mux5~1_combout\,
	datad => \ALT_INV_A[2]~input_o\,
	datae => \ALT_INV_OP[1]~input_o\,
	dataf => \ALT_INV_B[2]~input_o\,
	combout => \Mux5~0_combout\);

-- Location: LABCELL_X115_Y17_N9
\Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~9_sumout\ = SUM(( \B[3]~input_o\ ) + ( \A[3]~input_o\ ) + ( \Add0~6\ ))
-- \Add0~10\ = CARRY(( \B[3]~input_o\ ) + ( \A[3]~input_o\ ) + ( \Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B[3]~input_o\,
	datac => \ALT_INV_A[3]~input_o\,
	cin => \Add0~6\,
	sumout => \Add0~9_sumout\,
	cout => \Add0~10\);

-- Location: MLABCELL_X114_Y17_N39
\Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~9_sumout\ = SUM(( !\B[3]~input_o\ $ (\A[3]~input_o\) ) + ( \Add1~7\ ) + ( \Add1~6\ ))
-- \Add1~10\ = CARRY(( !\B[3]~input_o\ $ (\A[3]~input_o\) ) + ( \Add1~7\ ) + ( \Add1~6\ ))
-- \Add1~11\ = SHARE((!\B[3]~input_o\ & \A[3]~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000010100000101000000000000000001010010110100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B[3]~input_o\,
	datac => \ALT_INV_A[3]~input_o\,
	cin => \Add1~6\,
	sharein => \Add1~7\,
	sumout => \Add1~9_sumout\,
	cout => \Add1~10\,
	shareout => \Add1~11\);

-- Location: MLABCELL_X114_Y17_N18
\Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux4~0_combout\ = ( \OP[0]~input_o\ & ( \OP[1]~input_o\ & ( !\Div0|auto_generated|divider|divider|selnose\(36) ) ) ) # ( !\OP[0]~input_o\ & ( \OP[1]~input_o\ & ( \Mult0~11\ ) ) ) # ( \OP[0]~input_o\ & ( !\OP[1]~input_o\ & ( \Add1~9_sumout\ ) ) ) # ( 
-- !\OP[0]~input_o\ & ( !\OP[1]~input_o\ & ( \Add0~9_sumout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101001100110011001100001111000011111111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add0~9_sumout\,
	datab => \ALT_INV_Add1~9_sumout\,
	datac => \ALT_INV_Mult0~11\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_selnose\(36),
	datae => \ALT_INV_OP[0]~input_o\,
	dataf => \ALT_INV_OP[1]~input_o\,
	combout => \Mux4~0_combout\);

-- Location: MLABCELL_X114_Y17_N54
\Mux4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux4~1_combout\ = ( \OP[0]~input_o\ & ( \B[3]~input_o\ & ( (!\OP[2]~input_o\ & ((\Mux4~0_combout\))) # (\OP[2]~input_o\ & (!\OP[1]~input_o\)) ) ) ) # ( !\OP[0]~input_o\ & ( \B[3]~input_o\ & ( (!\OP[2]~input_o\ & (((\Mux4~0_combout\)))) # (\OP[2]~input_o\ 
-- & (!\OP[1]~input_o\ & ((\A[3]~input_o\)))) ) ) ) # ( \OP[0]~input_o\ & ( !\B[3]~input_o\ & ( (!\OP[2]~input_o\ & (((\Mux4~0_combout\)))) # (\OP[2]~input_o\ & (!\OP[1]~input_o\ & ((\A[3]~input_o\)))) ) ) ) # ( !\OP[0]~input_o\ & ( !\B[3]~input_o\ & ( 
-- (!\OP[2]~input_o\ & \Mux4~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000011000010111000001100001011100010111000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[1]~input_o\,
	datab => \ALT_INV_OP[2]~input_o\,
	datac => \ALT_INV_Mux4~0_combout\,
	datad => \ALT_INV_A[3]~input_o\,
	datae => \ALT_INV_OP[0]~input_o\,
	dataf => \ALT_INV_B[3]~input_o\,
	combout => \Mux4~1_combout\);

-- Location: MLABCELL_X114_Y17_N42
\Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~13_sumout\ = SUM(( !\A[4]~input_o\ $ (\B[4]~input_o\) ) + ( \Add1~11\ ) + ( \Add1~10\ ))
-- \Add1~14\ = CARRY(( !\A[4]~input_o\ $ (\B[4]~input_o\) ) + ( \Add1~11\ ) + ( \Add1~10\ ))
-- \Add1~15\ = SHARE((\A[4]~input_o\ & !\B[4]~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100000011000000000000000000001100001111000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_A[4]~input_o\,
	datac => \ALT_INV_B[4]~input_o\,
	cin => \Add1~10\,
	sharein => \Add1~11\,
	sumout => \Add1~13_sumout\,
	cout => \Add1~14\,
	shareout => \Add1~15\);

-- Location: LABCELL_X115_Y17_N12
\Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~13_sumout\ = SUM(( \B[4]~input_o\ ) + ( \A[4]~input_o\ ) + ( \Add0~10\ ))
-- \Add0~14\ = CARRY(( \B[4]~input_o\ ) + ( \A[4]~input_o\ ) + ( \Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B[4]~input_o\,
	datac => \ALT_INV_A[4]~input_o\,
	cin => \Add0~10\,
	sumout => \Add0~13_sumout\,
	cout => \Add0~14\);

-- Location: MLABCELL_X114_Y17_N0
\Mux3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux3~1_combout\ = ( !\OP[1]~input_o\ & ( (!\OP[0]~input_o\ & (((\Add0~13_sumout\)))) # (\OP[0]~input_o\ & ((((\Add1~13_sumout\))))) ) ) # ( \OP[1]~input_o\ & ( (!\OP[0]~input_o\ & (\Mult0~12\)) # (\OP[0]~input_o\ & 
-- (((!\Div0|auto_generated|divider|divider|sel\(27) & (!\Div0|auto_generated|divider|divider|op_4~1_sumout\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000101000001010011100100010001001011111010111110111001000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \ALT_INV_Mult0~12\,
	datac => \Div0|auto_generated|divider|divider|ALT_INV_sel\(27),
	datad => \Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datae => \ALT_INV_OP[1]~input_o\,
	dataf => \ALT_INV_Add1~13_sumout\,
	datag => \ALT_INV_Add0~13_sumout\,
	combout => \Mux3~1_combout\);

-- Location: MLABCELL_X114_Y17_N24
\Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux3~0_combout\ = ( \OP[0]~input_o\ & ( \B[4]~input_o\ & ( (!\OP[2]~input_o\ & ((\Mux3~1_combout\))) # (\OP[2]~input_o\ & (!\OP[1]~input_o\)) ) ) ) # ( !\OP[0]~input_o\ & ( \B[4]~input_o\ & ( (!\OP[2]~input_o\ & (((\Mux3~1_combout\)))) # (\OP[2]~input_o\ 
-- & (!\OP[1]~input_o\ & ((\A[4]~input_o\)))) ) ) ) # ( \OP[0]~input_o\ & ( !\B[4]~input_o\ & ( (!\OP[2]~input_o\ & (((\Mux3~1_combout\)))) # (\OP[2]~input_o\ & (!\OP[1]~input_o\ & ((\A[4]~input_o\)))) ) ) ) # ( !\OP[0]~input_o\ & ( !\B[4]~input_o\ & ( 
-- (!\OP[2]~input_o\ & \Mux3~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000011000010111000001100001011100010111000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[1]~input_o\,
	datab => \ALT_INV_OP[2]~input_o\,
	datac => \ALT_INV_Mux3~1_combout\,
	datad => \ALT_INV_A[4]~input_o\,
	datae => \ALT_INV_OP[0]~input_o\,
	dataf => \ALT_INV_B[4]~input_o\,
	combout => \Mux3~0_combout\);

-- Location: MLABCELL_X114_Y17_N45
\Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~17_sumout\ = SUM(( !\A[5]~input_o\ $ (\B[5]~input_o\) ) + ( \Add1~15\ ) + ( \Add1~14\ ))
-- \Add1~18\ = CARRY(( !\A[5]~input_o\ $ (\B[5]~input_o\) ) + ( \Add1~15\ ) + ( \Add1~14\ ))
-- \Add1~19\ = SHARE((\A[5]~input_o\ & !\B[5]~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010000000000000000000000001010101001010101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[5]~input_o\,
	datad => \ALT_INV_B[5]~input_o\,
	cin => \Add1~14\,
	sharein => \Add1~15\,
	sumout => \Add1~17_sumout\,
	cout => \Add1~18\,
	shareout => \Add1~19\);

-- Location: LABCELL_X115_Y17_N15
\Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~17_sumout\ = SUM(( \B[5]~input_o\ ) + ( \A[5]~input_o\ ) + ( \Add0~14\ ))
-- \Add0~18\ = CARRY(( \B[5]~input_o\ ) + ( \A[5]~input_o\ ) + ( \Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[5]~input_o\,
	datad => \ALT_INV_B[5]~input_o\,
	cin => \Add0~14\,
	sumout => \Add0~17_sumout\,
	cout => \Add0~18\);

-- Location: LABCELL_X116_Y17_N6
\Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux2~0_combout\ = ( \OP[0]~input_o\ & ( \Mult0~13\ & ( (!\OP[1]~input_o\ & (\Add1~17_sumout\)) # (\OP[1]~input_o\ & ((!\Div0|auto_generated|divider|divider|selnose\(18)))) ) ) ) # ( !\OP[0]~input_o\ & ( \Mult0~13\ & ( (\OP[1]~input_o\) # 
-- (\Add0~17_sumout\) ) ) ) # ( \OP[0]~input_o\ & ( !\Mult0~13\ & ( (!\OP[1]~input_o\ & (\Add1~17_sumout\)) # (\OP[1]~input_o\ & ((!\Div0|auto_generated|divider|divider|selnose\(18)))) ) ) ) # ( !\OP[0]~input_o\ & ( !\Mult0~13\ & ( (\Add0~17_sumout\ & 
-- !\OP[1]~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000010101011100110000001111111111110101010111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add1~17_sumout\,
	datab => \Div0|auto_generated|divider|divider|ALT_INV_selnose\(18),
	datac => \ALT_INV_Add0~17_sumout\,
	datad => \ALT_INV_OP[1]~input_o\,
	datae => \ALT_INV_OP[0]~input_o\,
	dataf => \ALT_INV_Mult0~13\,
	combout => \Mux2~0_combout\);

-- Location: LABCELL_X116_Y17_N42
\Mux2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux2~1_combout\ = ( \B[5]~input_o\ & ( \OP[1]~input_o\ & ( (!\OP[2]~input_o\ & \Mux2~0_combout\) ) ) ) # ( !\B[5]~input_o\ & ( \OP[1]~input_o\ & ( (!\OP[2]~input_o\ & \Mux2~0_combout\) ) ) ) # ( \B[5]~input_o\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ & 
-- (((\Mux2~0_combout\)))) # (\OP[2]~input_o\ & (((\OP[0]~input_o\)) # (\A[5]~input_o\))) ) ) ) # ( !\B[5]~input_o\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ & (((\Mux2~0_combout\)))) # (\OP[2]~input_o\ & (\A[5]~input_o\ & (\OP[0]~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000111001101000100111101111100000000110011000000000011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[5]~input_o\,
	datab => \ALT_INV_OP[2]~input_o\,
	datac => \ALT_INV_OP[0]~input_o\,
	datad => \ALT_INV_Mux2~0_combout\,
	datae => \ALT_INV_B[5]~input_o\,
	dataf => \ALT_INV_OP[1]~input_o\,
	combout => \Mux2~1_combout\);

-- Location: MLABCELL_X114_Y17_N48
\Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~21_sumout\ = SUM(( !\B[6]~input_o\ $ (\A[6]~input_o\) ) + ( \Add1~19\ ) + ( \Add1~18\ ))
-- \Add1~22\ = CARRY(( !\B[6]~input_o\ $ (\A[6]~input_o\) ) + ( \Add1~19\ ) + ( \Add1~18\ ))
-- \Add1~23\ = SHARE((!\B[6]~input_o\ & \A[6]~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111000000000000000000001111000000001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_B[6]~input_o\,
	datad => \ALT_INV_A[6]~input_o\,
	cin => \Add1~18\,
	sharein => \Add1~19\,
	sumout => \Add1~21_sumout\,
	cout => \Add1~22\,
	shareout => \Add1~23\);

-- Location: LABCELL_X115_Y17_N18
\Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~21_sumout\ = SUM(( \B[6]~input_o\ ) + ( \A[6]~input_o\ ) + ( \Add0~18\ ))
-- \Add0~22\ = CARRY(( \B[6]~input_o\ ) + ( \A[6]~input_o\ ) + ( \Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_B[6]~input_o\,
	dataf => \ALT_INV_A[6]~input_o\,
	cin => \Add0~18\,
	sumout => \Add0~21_sumout\,
	cout => \Add0~22\);

-- Location: LABCELL_X116_Y17_N18
\Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux1~0_combout\ = ( \OP[0]~input_o\ & ( \Add0~21_sumout\ & ( (!\OP[1]~input_o\ & ((\Add1~21_sumout\))) # (\OP[1]~input_o\ & (!\Div0|auto_generated|divider|divider|selnose\(9))) ) ) ) # ( !\OP[0]~input_o\ & ( \Add0~21_sumout\ & ( (!\OP[1]~input_o\) # 
-- (\Mult0~14\) ) ) ) # ( \OP[0]~input_o\ & ( !\Add0~21_sumout\ & ( (!\OP[1]~input_o\ & ((\Add1~21_sumout\))) # (\OP[1]~input_o\ & (!\Div0|auto_generated|divider|divider|selnose\(9))) ) ) ) # ( !\OP[0]~input_o\ & ( !\Add0~21_sumout\ & ( (\Mult0~14\ & 
-- \OP[1]~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111001100111010101011111111000011110011001110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|ALT_INV_selnose\(9),
	datab => \ALT_INV_Add1~21_sumout\,
	datac => \ALT_INV_Mult0~14\,
	datad => \ALT_INV_OP[1]~input_o\,
	datae => \ALT_INV_OP[0]~input_o\,
	dataf => \ALT_INV_Add0~21_sumout\,
	combout => \Mux1~0_combout\);

-- Location: LABCELL_X116_Y17_N54
\Mux1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux1~1_combout\ = ( \OP[1]~input_o\ & ( \B[6]~input_o\ & ( (!\OP[2]~input_o\ & \Mux1~0_combout\) ) ) ) # ( !\OP[1]~input_o\ & ( \B[6]~input_o\ & ( (!\OP[2]~input_o\ & (((\Mux1~0_combout\)))) # (\OP[2]~input_o\ & (((\OP[0]~input_o\)) # (\A[6]~input_o\))) 
-- ) ) ) # ( \OP[1]~input_o\ & ( !\B[6]~input_o\ & ( (!\OP[2]~input_o\ & \Mux1~0_combout\) ) ) ) # ( !\OP[1]~input_o\ & ( !\B[6]~input_o\ & ( (!\OP[2]~input_o\ & (((\Mux1~0_combout\)))) # (\OP[2]~input_o\ & (\A[6]~input_o\ & ((\OP[0]~input_o\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000011101000011000000110000011101001111110000110000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[6]~input_o\,
	datab => \ALT_INV_OP[2]~input_o\,
	datac => \ALT_INV_Mux1~0_combout\,
	datad => \ALT_INV_OP[0]~input_o\,
	datae => \ALT_INV_OP[1]~input_o\,
	dataf => \ALT_INV_B[6]~input_o\,
	combout => \Mux1~1_combout\);

-- Location: LABCELL_X115_Y17_N21
\Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~25_sumout\ = SUM(( \B[7]~input_o\ ) + ( \A[7]~input_o\ ) + ( \Add0~22\ ))
-- \Add0~26\ = CARRY(( \B[7]~input_o\ ) + ( \A[7]~input_o\ ) + ( \Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B[7]~input_o\,
	datac => \ALT_INV_A[7]~input_o\,
	cin => \Add0~22\,
	sumout => \Add0~25_sumout\,
	cout => \Add0~26\);

-- Location: MLABCELL_X114_Y17_N51
\Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~25_sumout\ = SUM(( !\A[7]~input_o\ $ (\B[7]~input_o\) ) + ( \Add1~23\ ) + ( \Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001010010110100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A[7]~input_o\,
	datac => \ALT_INV_B[7]~input_o\,
	cin => \Add1~22\,
	sharein => \Add1~23\,
	sumout => \Add1~25_sumout\);

-- Location: LABCELL_X115_Y17_N48
\Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux0~0_combout\ = ( \OP[0]~input_o\ & ( \Mult0~15\ & ( (!\OP[1]~input_o\ & (\Add1~25_sumout\)) # (\OP[1]~input_o\ & ((!\Div0|auto_generated|divider|divider|selnose\(0)))) ) ) ) # ( !\OP[0]~input_o\ & ( \Mult0~15\ & ( (\OP[1]~input_o\) # 
-- (\Add0~25_sumout\) ) ) ) # ( \OP[0]~input_o\ & ( !\Mult0~15\ & ( (!\OP[1]~input_o\ & (\Add1~25_sumout\)) # (\OP[1]~input_o\ & ((!\Div0|auto_generated|divider|divider|selnose\(0)))) ) ) ) # ( !\OP[0]~input_o\ & ( !\Mult0~15\ & ( (\Add0~25_sumout\ & 
-- !\OP[1]~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010001000100001111110000110001110111011101110011111100001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add0~25_sumout\,
	datab => \ALT_INV_OP[1]~input_o\,
	datac => \ALT_INV_Add1~25_sumout\,
	datad => \Div0|auto_generated|divider|divider|ALT_INV_selnose\(0),
	datae => \ALT_INV_OP[0]~input_o\,
	dataf => \ALT_INV_Mult0~15\,
	combout => \Mux0~0_combout\);

-- Location: LABCELL_X115_Y17_N54
\Mux0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux0~1_combout\ = ( \A[7]~input_o\ & ( \B[7]~input_o\ & ( (!\OP[2]~input_o\ & (\Mux0~0_combout\)) # (\OP[2]~input_o\ & ((!\OP[1]~input_o\))) ) ) ) # ( !\A[7]~input_o\ & ( \B[7]~input_o\ & ( (!\OP[2]~input_o\ & (\Mux0~0_combout\)) # (\OP[2]~input_o\ & 
-- (((!\OP[1]~input_o\ & \OP[0]~input_o\)))) ) ) ) # ( \A[7]~input_o\ & ( !\B[7]~input_o\ & ( (!\OP[2]~input_o\ & (\Mux0~0_combout\)) # (\OP[2]~input_o\ & (((!\OP[1]~input_o\ & \OP[0]~input_o\)))) ) ) ) # ( !\A[7]~input_o\ & ( !\B[7]~input_o\ & ( 
-- (\Mux0~0_combout\ & !\OP[2]~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000001010000010100000101110001010000010111000101110001011100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Mux0~0_combout\,
	datab => \ALT_INV_OP[1]~input_o\,
	datac => \ALT_INV_OP[2]~input_o\,
	datad => \ALT_INV_OP[0]~input_o\,
	datae => \ALT_INV_A[7]~input_o\,
	dataf => \ALT_INV_B[7]~input_o\,
	combout => \Mux0~1_combout\);

-- Location: LABCELL_X115_Y17_N24
\Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~29_sumout\ = SUM(( GND ) + ( GND ) + ( \Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \Add0~26\,
	sumout => \Add0~29_sumout\);

-- Location: LABCELL_X91_Y102_N3
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


