-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "06/01/2021 10:07:56"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          alu_controller
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY alu_controller_vhd_vec_tst IS
END alu_controller_vhd_vec_tst;
ARCHITECTURE alu_controller_arch OF alu_controller_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL ALUResult : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL Carryout : STD_LOGIC;
SIGNAL DA : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL MUXResult8 : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL MUXsel : STD_LOGIC;
SIGNAL OP : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL RAResult7 : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL RB : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL RBResult : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL Result : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL RSA : STD_LOGIC;
SIGNAL RSB : STD_LOGIC;
SIGNAL RSC : STD_LOGIC;
SIGNAL WRA : STD_LOGIC;
SIGNAL WRB : STD_LOGIC;
SIGNAL WRC : STD_LOGIC;
COMPONENT alu_controller
	PORT (
	ALUResult : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	Carryout : OUT STD_LOGIC;
	DA : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	MUXResult8 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	MUXsel : IN STD_LOGIC;
	OP : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
	RAResult7 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	RB : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	RBResult : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	Result : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	RSA : IN STD_LOGIC;
	RSB : IN STD_LOGIC;
	RSC : IN STD_LOGIC;
	WRA : IN STD_LOGIC;
	WRB : IN STD_LOGIC;
	WRC : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : alu_controller
	PORT MAP (
-- list connections between master ports and signals
	ALUResult => ALUResult,
	Carryout => Carryout,
	DA => DA,
	MUXResult8 => MUXResult8,
	MUXsel => MUXsel,
	OP => OP,
	RAResult7 => RAResult7,
	RB => RB,
	RBResult => RBResult,
	Result => Result,
	RSA => RSA,
	RSB => RSB,
	RSC => RSC,
	WRA => WRA,
	WRB => WRB,
	WRC => WRC
	);
-- DA[7]
t_prcs_DA_7: PROCESS
BEGIN
	DA(7) <= '0';
WAIT;
END PROCESS t_prcs_DA_7;
-- DA[6]
t_prcs_DA_6: PROCESS
BEGIN
	DA(6) <= '0';
WAIT;
END PROCESS t_prcs_DA_6;
-- DA[5]
t_prcs_DA_5: PROCESS
BEGIN
	DA(5) <= '0';
WAIT;
END PROCESS t_prcs_DA_5;
-- DA[4]
t_prcs_DA_4: PROCESS
BEGIN
	DA(4) <= '1';
WAIT;
END PROCESS t_prcs_DA_4;
-- DA[3]
t_prcs_DA_3: PROCESS
BEGIN
	DA(3) <= '0';
WAIT;
END PROCESS t_prcs_DA_3;
-- DA[2]
t_prcs_DA_2: PROCESS
BEGIN
	DA(2) <= '1';
WAIT;
END PROCESS t_prcs_DA_2;
-- DA[1]
t_prcs_DA_1: PROCESS
BEGIN
	DA(1) <= '1';
WAIT;
END PROCESS t_prcs_DA_1;
-- DA[0]
t_prcs_DA_0: PROCESS
BEGIN
	DA(0) <= '1';
WAIT;
END PROCESS t_prcs_DA_0;
-- RB[7]
t_prcs_RB_7: PROCESS
BEGIN
	RB(7) <= '0';
WAIT;
END PROCESS t_prcs_RB_7;
-- RB[6]
t_prcs_RB_6: PROCESS
BEGIN
	RB(6) <= '1';
WAIT;
END PROCESS t_prcs_RB_6;
-- RB[5]
t_prcs_RB_5: PROCESS
BEGIN
	RB(5) <= '0';
WAIT;
END PROCESS t_prcs_RB_5;
-- RB[4]
t_prcs_RB_4: PROCESS
BEGIN
	RB(4) <= '1';
WAIT;
END PROCESS t_prcs_RB_4;
-- RB[3]
t_prcs_RB_3: PROCESS
BEGIN
	RB(3) <= '0';
WAIT;
END PROCESS t_prcs_RB_3;
-- RB[2]
t_prcs_RB_2: PROCESS
BEGIN
	RB(2) <= '0';
WAIT;
END PROCESS t_prcs_RB_2;
-- RB[1]
t_prcs_RB_1: PROCESS
BEGIN
	RB(1) <= '0';
WAIT;
END PROCESS t_prcs_RB_1;
-- RB[0]
t_prcs_RB_0: PROCESS
BEGIN
	RB(0) <= '0';
WAIT;
END PROCESS t_prcs_RB_0;

-- MUXsel
t_prcs_MUXsel: PROCESS
BEGIN
	MUXsel <= '0';
WAIT;
END PROCESS t_prcs_MUXsel;
-- OP[2]
t_prcs_OP_2: PROCESS
BEGIN
	OP(2) <= '0';
WAIT;
END PROCESS t_prcs_OP_2;
-- OP[1]
t_prcs_OP_1: PROCESS
BEGIN
	OP(1) <= '0';
WAIT;
END PROCESS t_prcs_OP_1;
-- OP[0]
t_prcs_OP_0: PROCESS
BEGIN
	OP(0) <= '0';
WAIT;
END PROCESS t_prcs_OP_0;

-- RSA
t_prcs_RSA: PROCESS
BEGIN
	RSA <= '0';
WAIT;
END PROCESS t_prcs_RSA;

-- RSB
t_prcs_RSB: PROCESS
BEGIN
	RSB <= '0';
WAIT;
END PROCESS t_prcs_RSB;

-- RSC
t_prcs_RSC: PROCESS
BEGIN
	RSC <= '0';
WAIT;
END PROCESS t_prcs_RSC;

-- WRA
t_prcs_WRA: PROCESS
BEGIN
	WRA <= '0';
	WAIT FOR 200000 ps;
	WRA <= '1';
	WAIT FOR 280000 ps;
	WRA <= '0';
WAIT;
END PROCESS t_prcs_WRA;

-- WRB
t_prcs_WRB: PROCESS
BEGIN
	WRB <= '0';
	WAIT FOR 200000 ps;
	WRB <= '1';
	WAIT FOR 280000 ps;
	WRB <= '0';
WAIT;
END PROCESS t_prcs_WRB;

-- WRC
t_prcs_WRC: PROCESS
BEGIN
	WRC <= '0';
	WAIT FOR 560000 ps;
	WRC <= '1';
	WAIT FOR 240000 ps;
	WRC <= '0';
WAIT;
END PROCESS t_prcs_WRC;
END alu_controller_arch;
