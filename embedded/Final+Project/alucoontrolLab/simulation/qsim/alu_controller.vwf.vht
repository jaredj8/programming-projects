-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "06/10/2021 11:25:08"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          alu_controller
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY alu_controller_vhd_vec_tst IS
END alu_controller_vhd_vec_tst;
ARCHITECTURE alu_controller_arch OF alu_controller_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL acc : STD_LOGIC;
SIGNAL ALUResult : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL Carryout : STD_LOGIC;
SIGNAL clock : STD_LOGIC;
SIGNAL DA : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL OP : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL RB : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL RBResult : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL reset : STD_LOGIC;
SIGNAL Result : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL RSA : STD_LOGIC;
SIGNAL RSB : STD_LOGIC;
SIGNAL RSC : STD_LOGIC;
SIGNAL start : STD_LOGIC;
SIGNAL WRCmnitor : STD_LOGIC;
COMPONENT alu_controller
	PORT (
	acc : IN STD_LOGIC;
	ALUResult : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	Carryout : OUT STD_LOGIC;
	clock : IN STD_LOGIC;
	DA : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	OP : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
	RB : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	RBResult : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	reset : IN STD_LOGIC;
	Result : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	RSA : IN STD_LOGIC;
	RSB : IN STD_LOGIC;
	RSC : IN STD_LOGIC;
	start : IN STD_LOGIC;
	WRCmnitor : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : alu_controller
	PORT MAP (
-- list connections between master ports and signals
	acc => acc,
	ALUResult => ALUResult,
	Carryout => Carryout,
	clock => clock,
	DA => DA,
	OP => OP,
	RB => RB,
	RBResult => RBResult,
	reset => reset,
	Result => Result,
	RSA => RSA,
	RSB => RSB,
	RSC => RSC,
	start => start,
	WRCmnitor => WRCmnitor
	);

-- clock
t_prcs_clock: PROCESS
BEGIN
LOOP
	clock <= '0';
	WAIT FOR 1000000 ps;
	clock <= '1';
	WAIT FOR 1000000 ps;
	IF (NOW >= 100000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_clock;

-- reset
t_prcs_reset: PROCESS
BEGIN
	reset <= '0';
	WAIT FOR 640000 ps;
	reset <= '1';
	WAIT FOR 2560000 ps;
	reset <= '0';
WAIT;
END PROCESS t_prcs_reset;

-- start
t_prcs_start: PROCESS
BEGIN
	start <= '0';
	WAIT FOR 5120000 ps;
	start <= '1';
	WAIT FOR 1920000 ps;
	start <= '0';
	WAIT FOR 3840000 ps;
	start <= '1';
	WAIT FOR 3840000 ps;
	start <= '0';
WAIT;
END PROCESS t_prcs_start;

-- acc
t_prcs_acc: PROCESS
BEGIN
	acc <= '0';
	WAIT FOR 10880000 ps;
	acc <= '1';
	WAIT FOR 3840000 ps;
	acc <= '0';
WAIT;
END PROCESS t_prcs_acc;
-- OP[2]
t_prcs_OP_2: PROCESS
BEGIN
	OP(2) <= '0';
WAIT;
END PROCESS t_prcs_OP_2;
-- OP[1]
t_prcs_OP_1: PROCESS
BEGIN
	OP(1) <= '0';
WAIT;
END PROCESS t_prcs_OP_1;
-- OP[0]
t_prcs_OP_0: PROCESS
BEGIN
	OP(0) <= '0';
WAIT;
END PROCESS t_prcs_OP_0;
-- DA[7]
t_prcs_DA_7: PROCESS
BEGIN
	DA(7) <= '0';
WAIT;
END PROCESS t_prcs_DA_7;
-- DA[6]
t_prcs_DA_6: PROCESS
BEGIN
	DA(6) <= '0';
WAIT;
END PROCESS t_prcs_DA_6;
-- DA[5]
t_prcs_DA_5: PROCESS
BEGIN
	DA(5) <= '1';
WAIT;
END PROCESS t_prcs_DA_5;
-- DA[4]
t_prcs_DA_4: PROCESS
BEGIN
	DA(4) <= '0';
WAIT;
END PROCESS t_prcs_DA_4;
-- DA[3]
t_prcs_DA_3: PROCESS
BEGIN
	DA(3) <= '1';
WAIT;
END PROCESS t_prcs_DA_3;
-- DA[2]
t_prcs_DA_2: PROCESS
BEGIN
	DA(2) <= '0';
WAIT;
END PROCESS t_prcs_DA_2;
-- DA[1]
t_prcs_DA_1: PROCESS
BEGIN
	DA(1) <= '1';
WAIT;
END PROCESS t_prcs_DA_1;
-- DA[0]
t_prcs_DA_0: PROCESS
BEGIN
	DA(0) <= '1';
WAIT;
END PROCESS t_prcs_DA_0;
-- RB[7]
t_prcs_RB_7: PROCESS
BEGIN
	RB(7) <= '0';
WAIT;
END PROCESS t_prcs_RB_7;
-- RB[6]
t_prcs_RB_6: PROCESS
BEGIN
	RB(6) <= '0';
WAIT;
END PROCESS t_prcs_RB_6;
-- RB[5]
t_prcs_RB_5: PROCESS
BEGIN
	RB(5) <= '1';
WAIT;
END PROCESS t_prcs_RB_5;
-- RB[4]
t_prcs_RB_4: PROCESS
BEGIN
	RB(4) <= '1';
WAIT;
END PROCESS t_prcs_RB_4;
-- RB[3]
t_prcs_RB_3: PROCESS
BEGIN
	RB(3) <= '0';
WAIT;
END PROCESS t_prcs_RB_3;
-- RB[2]
t_prcs_RB_2: PROCESS
BEGIN
	RB(2) <= '1';
WAIT;
END PROCESS t_prcs_RB_2;
-- RB[1]
t_prcs_RB_1: PROCESS
BEGIN
	RB(1) <= '1';
WAIT;
END PROCESS t_prcs_RB_1;
-- RB[0]
t_prcs_RB_0: PROCESS
BEGIN
	RB(0) <= '0';
WAIT;
END PROCESS t_prcs_RB_0;

-- RSA
t_prcs_RSA: PROCESS
BEGIN
	RSA <= '0';
WAIT;
END PROCESS t_prcs_RSA;

-- RSC
t_prcs_RSC: PROCESS
BEGIN
	RSC <= '0';
WAIT;
END PROCESS t_prcs_RSC;

-- RSB
t_prcs_RSB: PROCESS
BEGIN
	RSB <= '0';
WAIT;
END PROCESS t_prcs_RSB;
END alu_controller_arch;
