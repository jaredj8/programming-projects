-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 20.1.1 Build 720 11/11/2020 SJ Lite Edition"

-- DATE "06/17/2021 17:40:19"

-- 
-- Device: Altera 5CEBA5U19C7 Package UFBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	alu_dataflow IS
    PORT (
	carryout : OUT std_logic;
	clock : IN std_logic;
	reset : IN std_logic;
	acc : IN std_logic;
	start : IN std_logic;
	RST_A : IN std_logic;
	A : IN std_logic_vector(7 DOWNTO 0);
	RST_C : IN std_logic;
	RST_B : IN std_logic;
	B : IN std_logic_vector(7 DOWNTO 0);
	OP : IN std_logic_vector(2 DOWNTO 0);
	ALU_OUT : OUT std_logic_vector(7 DOWNTO 0);
	RA_OUTPUT : OUT std_logic_vector(7 DOWNTO 0);
	RB_OUTPUT : OUT std_logic_vector(7 DOWNTO 0);
	RC_OUTPUT : OUT std_logic_vector(7 DOWNTO 0);
	STATE_OUT : OUT std_logic_vector(2 DOWNTO 0)
	);
END alu_dataflow;

-- Design Ports Information
-- carryout	=>  Location: PIN_Y17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ALU_OUT[7]	=>  Location: PIN_V18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ALU_OUT[6]	=>  Location: PIN_Y22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ALU_OUT[5]	=>  Location: PIN_C20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ALU_OUT[4]	=>  Location: PIN_U16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ALU_OUT[3]	=>  Location: PIN_K16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ALU_OUT[2]	=>  Location: PIN_Y21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ALU_OUT[1]	=>  Location: PIN_T15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ALU_OUT[0]	=>  Location: PIN_W18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RA_OUTPUT[7]	=>  Location: PIN_J18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RA_OUTPUT[6]	=>  Location: PIN_H19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RA_OUTPUT[5]	=>  Location: PIN_T22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RA_OUTPUT[4]	=>  Location: PIN_V20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RA_OUTPUT[3]	=>  Location: PIN_G22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RA_OUTPUT[2]	=>  Location: PIN_G20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RA_OUTPUT[1]	=>  Location: PIN_K15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RA_OUTPUT[0]	=>  Location: PIN_U21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RB_OUTPUT[7]	=>  Location: PIN_L19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RB_OUTPUT[6]	=>  Location: PIN_R21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RB_OUTPUT[5]	=>  Location: PIN_L15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RB_OUTPUT[4]	=>  Location: PIN_M16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RB_OUTPUT[3]	=>  Location: PIN_L20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RB_OUTPUT[2]	=>  Location: PIN_D22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RB_OUTPUT[1]	=>  Location: PIN_M17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RB_OUTPUT[0]	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RC_OUTPUT[7]	=>  Location: PIN_AA17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RC_OUTPUT[6]	=>  Location: PIN_AB22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RC_OUTPUT[5]	=>  Location: PIN_R17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RC_OUTPUT[4]	=>  Location: PIN_R20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RC_OUTPUT[3]	=>  Location: PIN_E20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RC_OUTPUT[2]	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RC_OUTPUT[1]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RC_OUTPUT[0]	=>  Location: PIN_R16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- STATE_OUT[2]	=>  Location: PIN_W17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- STATE_OUT[1]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- STATE_OUT[0]	=>  Location: PIN_W16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OP[2]	=>  Location: PIN_L17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OP[1]	=>  Location: PIN_J21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OP[0]	=>  Location: PIN_K21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[7]	=>  Location: PIN_R22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RST_B	=>  Location: PIN_R19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RST_C	=>  Location: PIN_K19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[7]	=>  Location: PIN_W22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RST_A	=>  Location: PIN_P19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[1]	=>  Location: PIN_U22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[6]	=>  Location: PIN_R15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[5]	=>  Location: PIN_V19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[4]	=>  Location: PIN_AB17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[3]	=>  Location: PIN_T18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[2]	=>  Location: PIN_T19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[6]	=>  Location: PIN_H20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[5]	=>  Location: PIN_G17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[4]	=>  Location: PIN_U17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[3]	=>  Location: PIN_J19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[2]	=>  Location: PIN_F20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[1]	=>  Location: PIN_H21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[0]	=>  Location: PIN_T17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[0]	=>  Location: PIN_T20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clock	=>  Location: PIN_K17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reset	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- acc	=>  Location: PIN_F18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- start	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF alu_dataflow IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_carryout : std_logic;
SIGNAL ww_clock : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_acc : std_logic;
SIGNAL ww_start : std_logic;
SIGNAL ww_RST_A : std_logic;
SIGNAL ww_A : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_RST_C : std_logic;
SIGNAL ww_RST_B : std_logic;
SIGNAL ww_B : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_OP : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_ALU_OUT : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_RA_OUTPUT : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_RB_OUTPUT : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_RC_OUTPUT : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_STATE_OUT : std_logic_vector(2 DOWNTO 0);
SIGNAL \inst|Mult0~8_ACLR_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \inst|Mult0~8_CLK_bus\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \inst|Mult0~8_ENA_bus\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \inst|Mult0~8_AX_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \inst|Mult0~8_AY_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \inst|Mult0~8_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \inst|Mult0~16\ : std_logic;
SIGNAL \inst|Mult0~17\ : std_logic;
SIGNAL \inst|Mult0~18\ : std_logic;
SIGNAL \inst|Mult0~19\ : std_logic;
SIGNAL \inst|Mult0~20\ : std_logic;
SIGNAL \inst|Mult0~21\ : std_logic;
SIGNAL \inst|Mult0~22\ : std_logic;
SIGNAL \inst|Mult0~23\ : std_logic;
SIGNAL \inst|Mult0~24\ : std_logic;
SIGNAL \inst|Mult0~25\ : std_logic;
SIGNAL \inst|Mult0~26\ : std_logic;
SIGNAL \inst|Mult0~27\ : std_logic;
SIGNAL \inst|Mult0~28\ : std_logic;
SIGNAL \inst|Mult0~29\ : std_logic;
SIGNAL \inst|Mult0~30\ : std_logic;
SIGNAL \inst|Mult0~31\ : std_logic;
SIGNAL \inst|Mult0~32\ : std_logic;
SIGNAL \inst|Mult0~33\ : std_logic;
SIGNAL \inst|Mult0~34\ : std_logic;
SIGNAL \inst|Mult0~35\ : std_logic;
SIGNAL \inst|Mult0~36\ : std_logic;
SIGNAL \inst|Mult0~37\ : std_logic;
SIGNAL \inst|Mult0~38\ : std_logic;
SIGNAL \inst|Mult0~39\ : std_logic;
SIGNAL \inst|Mult0~40\ : std_logic;
SIGNAL \inst|Mult0~41\ : std_logic;
SIGNAL \inst|Mult0~42\ : std_logic;
SIGNAL \inst|Mult0~43\ : std_logic;
SIGNAL \inst|Mult0~44\ : std_logic;
SIGNAL \inst|Mult0~45\ : std_logic;
SIGNAL \inst|Mult0~46\ : std_logic;
SIGNAL \inst|Mult0~47\ : std_logic;
SIGNAL \inst|Mult0~48\ : std_logic;
SIGNAL \inst|Mult0~49\ : std_logic;
SIGNAL \inst|Mult0~50\ : std_logic;
SIGNAL \inst|Mult0~51\ : std_logic;
SIGNAL \inst|Mult0~52\ : std_logic;
SIGNAL \inst|Mult0~53\ : std_logic;
SIGNAL \inst|Mult0~54\ : std_logic;
SIGNAL \inst|Mult0~55\ : std_logic;
SIGNAL \inst|Mult0~56\ : std_logic;
SIGNAL \inst|Mult0~57\ : std_logic;
SIGNAL \inst|Mult0~58\ : std_logic;
SIGNAL \inst|Mult0~59\ : std_logic;
SIGNAL \inst|Mult0~60\ : std_logic;
SIGNAL \inst|Mult0~61\ : std_logic;
SIGNAL \inst|Mult0~62\ : std_logic;
SIGNAL \inst|Mult0~63\ : std_logic;
SIGNAL \inst|Mult0~64\ : std_logic;
SIGNAL \inst|Mult0~65\ : std_logic;
SIGNAL \inst|Mult0~66\ : std_logic;
SIGNAL \inst|Mult0~67\ : std_logic;
SIGNAL \inst|Mult0~68\ : std_logic;
SIGNAL \inst|Mult0~69\ : std_logic;
SIGNAL \inst|Mult0~70\ : std_logic;
SIGNAL \inst|Mult0~71\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \OP[0]~input_o\ : std_logic;
SIGNAL \clock~input_o\ : std_logic;
SIGNAL \clock~inputCLKENA0_outclk\ : std_logic;
SIGNAL \start~input_o\ : std_logic;
SIGNAL \reset~input_o\ : std_logic;
SIGNAL \inst10|current_state.write_result~q\ : std_logic;
SIGNAL \inst10|Selector4~0_combout\ : std_logic;
SIGNAL \inst10|current_state.init~q\ : std_logic;
SIGNAL \acc~input_o\ : std_logic;
SIGNAL \inst10|next_state.accumulate~0_combout\ : std_logic;
SIGNAL \inst10|current_state.accumulate~q\ : std_logic;
SIGNAL \inst10|next_state.fetch~0_combout\ : std_logic;
SIGNAL \inst10|current_state.fetch~q\ : std_logic;
SIGNAL \inst10|next_state.procc~combout\ : std_logic;
SIGNAL \inst10|current_state.procc~q\ : std_logic;
SIGNAL \inst10|SEL~combout\ : std_logic;
SIGNAL \inst10|WRC~1_combout\ : std_logic;
SIGNAL \inst10|WRC~combout\ : std_logic;
SIGNAL \A[7]~input_o\ : std_logic;
SIGNAL \RST_A~input_o\ : std_logic;
SIGNAL \B[7]~input_o\ : std_logic;
SIGNAL \RST_B~input_o\ : std_logic;
SIGNAL \inst4|Q[7]~_Duplicate_1_q\ : std_logic;
SIGNAL \A[6]~input_o\ : std_logic;
SIGNAL \inst2|DATA_OUT[6]~1_combout\ : std_logic;
SIGNAL \B[6]~input_o\ : std_logic;
SIGNAL \inst4|Q[6]~_Duplicate_1_q\ : std_logic;
SIGNAL \inst|ALU_RESULT~1_combout\ : std_logic;
SIGNAL \B[2]~input_o\ : std_logic;
SIGNAL \inst4|Q[2]~_Duplicate_1_q\ : std_logic;
SIGNAL \B[4]~input_o\ : std_logic;
SIGNAL \inst4|Q[4]~_Duplicate_1_q\ : std_logic;
SIGNAL \B[3]~input_o\ : std_logic;
SIGNAL \inst4|Q[3]~_Duplicate_1_q\ : std_logic;
SIGNAL \B[5]~input_o\ : std_logic;
SIGNAL \inst4|Q[5]~_Duplicate_1_q\ : std_logic;
SIGNAL \A[0]~input_o\ : std_logic;
SIGNAL \B[0]~input_o\ : std_logic;
SIGNAL \inst4|Q[0]~_Duplicate_1_q\ : std_logic;
SIGNAL \inst|Add0~38_cout\ : std_logic;
SIGNAL \inst|Add0~29_sumout\ : std_logic;
SIGNAL \OP[2]~input_o\ : std_logic;
SIGNAL \B[1]~input_o\ : std_logic;
SIGNAL \inst4|Q[1]~_Duplicate_1_q\ : std_logic;
SIGNAL \inst2|DATA_OUT[7]~0_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~6\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~7\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~6\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~7\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[9]~1_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[0]~0_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_3~6\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ : std_logic;
SIGNAL \A[2]~input_o\ : std_logic;
SIGNAL \A[1]~input_o\ : std_logic;
SIGNAL \inst|Add0~30\ : std_logic;
SIGNAL \inst|Add0~26\ : std_logic;
SIGNAL \inst|Add0~21_sumout\ : std_logic;
SIGNAL \inst|ALU_RESULT~5_combout\ : std_logic;
SIGNAL \A[3]~input_o\ : std_logic;
SIGNAL \inst|ALU_RESULT~4_combout\ : std_logic;
SIGNAL \inst|Add0~22\ : std_logic;
SIGNAL \inst|Add0~17_sumout\ : std_logic;
SIGNAL \A[4]~input_o\ : std_logic;
SIGNAL \inst|Add0~18\ : std_logic;
SIGNAL \inst|Add0~13_sumout\ : std_logic;
SIGNAL \inst|ALU_RESULT~3_combout\ : std_logic;
SIGNAL \A[5]~input_o\ : std_logic;
SIGNAL \inst2|DATA_OUT[5]~2_combout\ : std_logic;
SIGNAL \inst|Mult0~12\ : std_logic;
SIGNAL \inst|Mux3~0_combout\ : std_logic;
SIGNAL \OP[1]~input_o\ : std_logic;
SIGNAL \inst|Mux3~1_combout\ : std_logic;
SIGNAL \RST_C~input_o\ : std_logic;
SIGNAL \inst2|DATA_OUT[4]~3_combout\ : std_logic;
SIGNAL \inst|Mult0~11\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[9]~2_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[18]~3_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_3~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_3~18_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_3~13_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_4~22_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_4~18\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_4~14\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_4~10\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_4~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[17]~10_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_4~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_4~13_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_4~17_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~26_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~22\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~18\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~14\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~10\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~6\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ : std_logic;
SIGNAL \inst|Mux4~0_combout\ : std_logic;
SIGNAL \inst|Mux4~1_combout\ : std_logic;
SIGNAL \inst2|DATA_OUT[3]~4_combout\ : std_logic;
SIGNAL \inst|Mult0~10\ : std_logic;
SIGNAL \inst|Mux5~0_combout\ : std_logic;
SIGNAL \inst|Mux5~1_combout\ : std_logic;
SIGNAL \inst2|DATA_OUT[2]~5_combout\ : std_logic;
SIGNAL \inst|Mult0~9\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[27]~5_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[27]~4_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[36]~6_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[26]~11_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[25]~15_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~13_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~17_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[24]~18_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~21_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~30_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~26\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~22\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~18\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~14\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~10\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~13_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[33]~19_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~17_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~21_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[32]~21_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~25_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~34_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~30\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~26\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~22\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~18\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~14\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~10\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~6\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ : std_logic;
SIGNAL \inst|Add0~25_sumout\ : std_logic;
SIGNAL \inst|Mux6~1_combout\ : std_logic;
SIGNAL \inst|Mux6~0_combout\ : std_logic;
SIGNAL \inst2|DATA_OUT[1]~6_combout\ : std_logic;
SIGNAL \inst|Mult0~13\ : std_logic;
SIGNAL \inst|Mux2~0_combout\ : std_logic;
SIGNAL \inst|ALU_RESULT~2_combout\ : std_logic;
SIGNAL \inst|Add0~14\ : std_logic;
SIGNAL \inst|Add0~9_sumout\ : std_logic;
SIGNAL \inst|Mux2~1_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_3~14\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_3~10\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_3~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_4~6\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_5~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~6\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[45]~7_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[45]~8_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[44]~13_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[43]~17_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~13_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[42]~20_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~17_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[41]~22_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~21_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~25_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|StageOut[40]~23_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_7~29_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_8~38_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_8~34_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_8~30_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_8~26_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_8~22_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_8~18_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_8~14_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_8~10_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_8~6_cout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|op_8~1_sumout\ : std_logic;
SIGNAL \inst|Mux7~0_combout\ : std_logic;
SIGNAL \inst|LessThan0~0_combout\ : std_logic;
SIGNAL \inst|LessThan0~1_combout\ : std_logic;
SIGNAL \inst|LessThan0~2_combout\ : std_logic;
SIGNAL \inst|Mux7~1_combout\ : std_logic;
SIGNAL \inst|ALU_RESULT~0_combout\ : std_logic;
SIGNAL \inst|Mux7~3_combout\ : std_logic;
SIGNAL \inst|ALU_RESULT~6_combout\ : std_logic;
SIGNAL \inst|ALU_RESULT~7_combout\ : std_logic;
SIGNAL \inst|Mux7~2_combout\ : std_logic;
SIGNAL \inst|Mux7~4_combout\ : std_logic;
SIGNAL \inst|Mult0~8_resulta\ : std_logic;
SIGNAL \inst|Mux7~6_combout\ : std_logic;
SIGNAL \inst2|DATA_OUT[0]~7_combout\ : std_logic;
SIGNAL \inst|Mult0~14\ : std_logic;
SIGNAL \inst|Mux1~0_combout\ : std_logic;
SIGNAL \inst|Add0~10\ : std_logic;
SIGNAL \inst|Add0~5_sumout\ : std_logic;
SIGNAL \inst|Mux1~1_combout\ : std_logic;
SIGNAL \inst|Add0~6\ : std_logic;
SIGNAL \inst|Add0~1_sumout\ : std_logic;
SIGNAL \inst|Mult0~15\ : std_logic;
SIGNAL \inst|Mux0~0_combout\ : std_logic;
SIGNAL \inst|Mux0~1_combout\ : std_logic;
SIGNAL \inst|Add0~2\ : std_logic;
SIGNAL \inst|Add0~33_sumout\ : std_logic;
SIGNAL \inst|Mux7~5_combout\ : std_logic;
SIGNAL \inst5|Q\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \inst3|Q\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \inst10|STATE_OUT\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \inst|Div0|auto_generated|divider|divider|sel\ : std_logic_vector(71 DOWNTO 0);
SIGNAL \inst|temp\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \inst|Div0|auto_generated|divider|divider|selnose\ : std_logic_vector(71 DOWNTO 0);
SIGNAL \inst|ALT_INV_Add0~21_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\ : std_logic;
SIGNAL \inst|ALT_INV_Add0~17_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\ : std_logic;
SIGNAL \inst|ALT_INV_Add0~13_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~1_sumout\ : std_logic;
SIGNAL \inst|ALT_INV_Add0~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\ : std_logic;
SIGNAL \inst|ALT_INV_Add0~5_sumout\ : std_logic;
SIGNAL \inst|ALT_INV_Mult0~15\ : std_logic;
SIGNAL \inst|ALT_INV_Mult0~14\ : std_logic;
SIGNAL \inst|ALT_INV_Mult0~13\ : std_logic;
SIGNAL \inst|ALT_INV_Mult0~12\ : std_logic;
SIGNAL \inst|ALT_INV_Mult0~11\ : std_logic;
SIGNAL \inst|ALT_INV_Mult0~10\ : std_logic;
SIGNAL \inst|ALT_INV_Mult0~9\ : std_logic;
SIGNAL \inst|ALT_INV_Mult0~8_resulta\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[1]~1_sumout\ : std_logic;
SIGNAL \inst|ALT_INV_Add0~1_sumout\ : std_logic;
SIGNAL \inst|ALT_INV_Mux4~0_combout\ : std_logic;
SIGNAL \inst|ALT_INV_ALU_RESULT~4_combout\ : std_logic;
SIGNAL \inst2|ALT_INV_DATA_OUT[3]~4_combout\ : std_logic;
SIGNAL \inst5|ALT_INV_Q\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \inst3|ALT_INV_Q\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \inst|ALT_INV_Mux3~0_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\ : std_logic_vector(5 DOWNTO 1);
SIGNAL \inst|ALT_INV_ALU_RESULT~3_combout\ : std_logic;
SIGNAL \inst2|ALT_INV_DATA_OUT[4]~3_combout\ : std_logic;
SIGNAL \inst|ALT_INV_Mux2~0_combout\ : std_logic;
SIGNAL \inst|ALT_INV_ALU_RESULT~2_combout\ : std_logic;
SIGNAL \inst2|ALT_INV_DATA_OUT[5]~2_combout\ : std_logic;
SIGNAL \inst|ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \inst|ALT_INV_ALU_RESULT~1_combout\ : std_logic;
SIGNAL \inst2|ALT_INV_DATA_OUT[6]~1_combout\ : std_logic;
SIGNAL \inst|ALT_INV_Mux0~0_combout\ : std_logic;
SIGNAL \inst4|ALT_INV_Q[2]~_Duplicate_1_q\ : std_logic;
SIGNAL \inst4|ALT_INV_Q[3]~_Duplicate_1_q\ : std_logic;
SIGNAL \inst4|ALT_INV_Q[4]~_Duplicate_1_q\ : std_logic;
SIGNAL \inst4|ALT_INV_Q[5]~_Duplicate_1_q\ : std_logic;
SIGNAL \inst4|ALT_INV_Q[6]~_Duplicate_1_q\ : std_logic;
SIGNAL \inst4|ALT_INV_Q[1]~_Duplicate_1_q\ : std_logic;
SIGNAL \inst|ALT_INV_ALU_RESULT~0_combout\ : std_logic;
SIGNAL \inst2|ALT_INV_DATA_OUT[7]~0_combout\ : std_logic;
SIGNAL \inst4|ALT_INV_Q[7]~_Duplicate_1_q\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~29_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~25_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~25_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~21_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~21_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~21_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~17_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~17_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~17_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~17_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~13_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~13_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~13_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~13_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~13_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[0]~9_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[1]~5_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[0]~5_sumout\ : std_logic;
SIGNAL \inst|ALT_INV_Add0~33_sumout\ : std_logic;
SIGNAL \inst|ALT_INV_Add0~29_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_8~1_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\ : std_logic;
SIGNAL \inst|ALT_INV_Add0~25_sumout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\ : std_logic;
SIGNAL \ALT_INV_clock~inputCLKENA0_outclk\ : std_logic;
SIGNAL \ALT_INV_start~input_o\ : std_logic;
SIGNAL \ALT_INV_acc~input_o\ : std_logic;
SIGNAL \ALT_INV_reset~input_o\ : std_logic;
SIGNAL \ALT_INV_clock~input_o\ : std_logic;
SIGNAL \ALT_INV_RST_A~input_o\ : std_logic;
SIGNAL \ALT_INV_RST_C~input_o\ : std_logic;
SIGNAL \ALT_INV_RST_B~input_o\ : std_logic;
SIGNAL \ALT_INV_OP[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_OP[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_OP[2]~input_o\ : std_logic;
SIGNAL \inst10|ALT_INV_WRC~combout\ : std_logic;
SIGNAL \inst10|ALT_INV_SEL~combout\ : std_logic;
SIGNAL \inst|ALT_INV_temp\ : std_logic_vector(8 DOWNTO 8);
SIGNAL \inst|ALT_INV_Mux6~1_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[40]~23_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[41]~22_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[32]~21_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[42]~20_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[33]~19_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[24]~18_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[43]~17_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[34]~16_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[25]~15_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[16]~14_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[44]~13_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[35]~12_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[26]~11_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[17]~10_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[8]~9_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~8_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~7_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_selnose\ : std_logic_vector(54 DOWNTO 0);
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[36]~6_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~5_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~4_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[18]~3_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~2_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~1_combout\ : std_logic;
SIGNAL \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[0]~0_combout\ : std_logic;
SIGNAL \inst10|ALT_INV_WRC~1_combout\ : std_logic;
SIGNAL \inst10|ALT_INV_current_state.init~q\ : std_logic;
SIGNAL \inst|ALT_INV_Mux7~5_combout\ : std_logic;
SIGNAL \inst10|ALT_INV_current_state.fetch~q\ : std_logic;
SIGNAL \inst10|ALT_INV_current_state.accumulate~q\ : std_logic;
SIGNAL \inst10|ALT_INV_current_state.procc~q\ : std_logic;
SIGNAL \inst10|ALT_INV_current_state.write_result~q\ : std_logic;
SIGNAL \inst|ALT_INV_Mux7~4_combout\ : std_logic;
SIGNAL \inst|ALT_INV_Mux7~3_combout\ : std_logic;
SIGNAL \inst|ALT_INV_Mux7~2_combout\ : std_logic;
SIGNAL \inst|ALT_INV_ALU_RESULT~7_combout\ : std_logic;
SIGNAL \inst|ALT_INV_ALU_RESULT~6_combout\ : std_logic;
SIGNAL \inst|ALT_INV_Mux7~1_combout\ : std_logic;
SIGNAL \inst|ALT_INV_LessThan0~2_combout\ : std_logic;
SIGNAL \inst|ALT_INV_LessThan0~1_combout\ : std_logic;
SIGNAL \inst|ALT_INV_LessThan0~0_combout\ : std_logic;
SIGNAL \inst2|ALT_INV_DATA_OUT[0]~7_combout\ : std_logic;
SIGNAL \inst|ALT_INV_Mux7~0_combout\ : std_logic;
SIGNAL \inst4|ALT_INV_Q[0]~_Duplicate_1_q\ : std_logic;
SIGNAL \inst2|ALT_INV_DATA_OUT[1]~6_combout\ : std_logic;
SIGNAL \inst|ALT_INV_Mux5~0_combout\ : std_logic;
SIGNAL \inst|ALT_INV_ALU_RESULT~5_combout\ : std_logic;
SIGNAL \inst2|ALT_INV_DATA_OUT[2]~5_combout\ : std_logic;

BEGIN

carryout <= ww_carryout;
ww_clock <= clock;
ww_reset <= reset;
ww_acc <= acc;
ww_start <= start;
ww_RST_A <= RST_A;
ww_A <= A;
ww_RST_C <= RST_C;
ww_RST_B <= RST_B;
ww_B <= B;
ww_OP <= OP;
ALU_OUT <= ww_ALU_OUT;
RA_OUTPUT <= ww_RA_OUTPUT;
RB_OUTPUT <= ww_RB_OUTPUT;
RC_OUTPUT <= ww_RC_OUTPUT;
STATE_OUT <= ww_STATE_OUT;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\inst|Mult0~8_ACLR_bus\ <= (gnd & \RST_B~input_o\);

\inst|Mult0~8_CLK_bus\ <= (gnd & gnd & \inst10|current_state.fetch~q\);

\inst|Mult0~8_ENA_bus\ <= (vcc & vcc & vcc);

\inst|Mult0~8_AX_bus\ <= (\inst2|DATA_OUT[7]~0_combout\ & \inst2|DATA_OUT[6]~1_combout\ & \inst2|DATA_OUT[5]~2_combout\ & \inst2|DATA_OUT[4]~3_combout\ & \inst2|DATA_OUT[3]~4_combout\ & \inst2|DATA_OUT[2]~5_combout\ & \inst2|DATA_OUT[1]~6_combout\ & 
\inst2|DATA_OUT[0]~7_combout\);

\inst|Mult0~8_AY_bus\ <= (\B[7]~input_o\ & \B[6]~input_o\ & \B[5]~input_o\ & \B[4]~input_o\ & \B[3]~input_o\ & \B[2]~input_o\ & \B[1]~input_o\ & \B[0]~input_o\);

\inst|Mult0~8_resulta\ <= \inst|Mult0~8_RESULTA_bus\(0);
\inst|Mult0~9\ <= \inst|Mult0~8_RESULTA_bus\(1);
\inst|Mult0~10\ <= \inst|Mult0~8_RESULTA_bus\(2);
\inst|Mult0~11\ <= \inst|Mult0~8_RESULTA_bus\(3);
\inst|Mult0~12\ <= \inst|Mult0~8_RESULTA_bus\(4);
\inst|Mult0~13\ <= \inst|Mult0~8_RESULTA_bus\(5);
\inst|Mult0~14\ <= \inst|Mult0~8_RESULTA_bus\(6);
\inst|Mult0~15\ <= \inst|Mult0~8_RESULTA_bus\(7);
\inst|Mult0~16\ <= \inst|Mult0~8_RESULTA_bus\(8);
\inst|Mult0~17\ <= \inst|Mult0~8_RESULTA_bus\(9);
\inst|Mult0~18\ <= \inst|Mult0~8_RESULTA_bus\(10);
\inst|Mult0~19\ <= \inst|Mult0~8_RESULTA_bus\(11);
\inst|Mult0~20\ <= \inst|Mult0~8_RESULTA_bus\(12);
\inst|Mult0~21\ <= \inst|Mult0~8_RESULTA_bus\(13);
\inst|Mult0~22\ <= \inst|Mult0~8_RESULTA_bus\(14);
\inst|Mult0~23\ <= \inst|Mult0~8_RESULTA_bus\(15);
\inst|Mult0~24\ <= \inst|Mult0~8_RESULTA_bus\(16);
\inst|Mult0~25\ <= \inst|Mult0~8_RESULTA_bus\(17);
\inst|Mult0~26\ <= \inst|Mult0~8_RESULTA_bus\(18);
\inst|Mult0~27\ <= \inst|Mult0~8_RESULTA_bus\(19);
\inst|Mult0~28\ <= \inst|Mult0~8_RESULTA_bus\(20);
\inst|Mult0~29\ <= \inst|Mult0~8_RESULTA_bus\(21);
\inst|Mult0~30\ <= \inst|Mult0~8_RESULTA_bus\(22);
\inst|Mult0~31\ <= \inst|Mult0~8_RESULTA_bus\(23);
\inst|Mult0~32\ <= \inst|Mult0~8_RESULTA_bus\(24);
\inst|Mult0~33\ <= \inst|Mult0~8_RESULTA_bus\(25);
\inst|Mult0~34\ <= \inst|Mult0~8_RESULTA_bus\(26);
\inst|Mult0~35\ <= \inst|Mult0~8_RESULTA_bus\(27);
\inst|Mult0~36\ <= \inst|Mult0~8_RESULTA_bus\(28);
\inst|Mult0~37\ <= \inst|Mult0~8_RESULTA_bus\(29);
\inst|Mult0~38\ <= \inst|Mult0~8_RESULTA_bus\(30);
\inst|Mult0~39\ <= \inst|Mult0~8_RESULTA_bus\(31);
\inst|Mult0~40\ <= \inst|Mult0~8_RESULTA_bus\(32);
\inst|Mult0~41\ <= \inst|Mult0~8_RESULTA_bus\(33);
\inst|Mult0~42\ <= \inst|Mult0~8_RESULTA_bus\(34);
\inst|Mult0~43\ <= \inst|Mult0~8_RESULTA_bus\(35);
\inst|Mult0~44\ <= \inst|Mult0~8_RESULTA_bus\(36);
\inst|Mult0~45\ <= \inst|Mult0~8_RESULTA_bus\(37);
\inst|Mult0~46\ <= \inst|Mult0~8_RESULTA_bus\(38);
\inst|Mult0~47\ <= \inst|Mult0~8_RESULTA_bus\(39);
\inst|Mult0~48\ <= \inst|Mult0~8_RESULTA_bus\(40);
\inst|Mult0~49\ <= \inst|Mult0~8_RESULTA_bus\(41);
\inst|Mult0~50\ <= \inst|Mult0~8_RESULTA_bus\(42);
\inst|Mult0~51\ <= \inst|Mult0~8_RESULTA_bus\(43);
\inst|Mult0~52\ <= \inst|Mult0~8_RESULTA_bus\(44);
\inst|Mult0~53\ <= \inst|Mult0~8_RESULTA_bus\(45);
\inst|Mult0~54\ <= \inst|Mult0~8_RESULTA_bus\(46);
\inst|Mult0~55\ <= \inst|Mult0~8_RESULTA_bus\(47);
\inst|Mult0~56\ <= \inst|Mult0~8_RESULTA_bus\(48);
\inst|Mult0~57\ <= \inst|Mult0~8_RESULTA_bus\(49);
\inst|Mult0~58\ <= \inst|Mult0~8_RESULTA_bus\(50);
\inst|Mult0~59\ <= \inst|Mult0~8_RESULTA_bus\(51);
\inst|Mult0~60\ <= \inst|Mult0~8_RESULTA_bus\(52);
\inst|Mult0~61\ <= \inst|Mult0~8_RESULTA_bus\(53);
\inst|Mult0~62\ <= \inst|Mult0~8_RESULTA_bus\(54);
\inst|Mult0~63\ <= \inst|Mult0~8_RESULTA_bus\(55);
\inst|Mult0~64\ <= \inst|Mult0~8_RESULTA_bus\(56);
\inst|Mult0~65\ <= \inst|Mult0~8_RESULTA_bus\(57);
\inst|Mult0~66\ <= \inst|Mult0~8_RESULTA_bus\(58);
\inst|Mult0~67\ <= \inst|Mult0~8_RESULTA_bus\(59);
\inst|Mult0~68\ <= \inst|Mult0~8_RESULTA_bus\(60);
\inst|Mult0~69\ <= \inst|Mult0~8_RESULTA_bus\(61);
\inst|Mult0~70\ <= \inst|Mult0~8_RESULTA_bus\(62);
\inst|Mult0~71\ <= \inst|Mult0~8_RESULTA_bus\(63);
\inst|ALT_INV_Add0~21_sumout\ <= NOT \inst|Add0~21_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_5~1_sumout\;
\inst|ALT_INV_Add0~17_sumout\ <= NOT \inst|Add0~17_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_4~1_sumout\;
\inst|ALT_INV_Add0~13_sumout\ <= NOT \inst|Add0~13_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~1_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_3~1_sumout\;
\inst|ALT_INV_Add0~9_sumout\ <= NOT \inst|Add0~9_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\;
\inst|ALT_INV_Add0~5_sumout\ <= NOT \inst|Add0~5_sumout\;
\inst|ALT_INV_Mult0~15\ <= NOT \inst|Mult0~15\;
\inst|ALT_INV_Mult0~14\ <= NOT \inst|Mult0~14\;
\inst|ALT_INV_Mult0~13\ <= NOT \inst|Mult0~13\;
\inst|ALT_INV_Mult0~12\ <= NOT \inst|Mult0~12\;
\inst|ALT_INV_Mult0~11\ <= NOT \inst|Mult0~11\;
\inst|ALT_INV_Mult0~10\ <= NOT \inst|Mult0~10\;
\inst|ALT_INV_Mult0~9\ <= NOT \inst|Mult0~9\;
\inst|ALT_INV_Mult0~8_resulta\ <= NOT \inst|Mult0~8_resulta\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[1]~1_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\;
\inst|ALT_INV_Add0~1_sumout\ <= NOT \inst|Add0~1_sumout\;
\inst|ALT_INV_Mux4~0_combout\ <= NOT \inst|Mux4~0_combout\;
\inst|ALT_INV_ALU_RESULT~4_combout\ <= NOT \inst|ALU_RESULT~4_combout\;
\inst2|ALT_INV_DATA_OUT[3]~4_combout\ <= NOT \inst2|DATA_OUT[3]~4_combout\;
\inst5|ALT_INV_Q\(3) <= NOT \inst5|Q\(3);
\inst3|ALT_INV_Q\(3) <= NOT \inst3|Q\(3);
\inst|ALT_INV_Mux3~0_combout\ <= NOT \inst|Mux3~0_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(3) <= NOT \inst|Div0|auto_generated|divider|divider|sel\(3);
\inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4) <= NOT \inst|Div0|auto_generated|divider|divider|sel\(4);
\inst|ALT_INV_ALU_RESULT~3_combout\ <= NOT \inst|ALU_RESULT~3_combout\;
\inst2|ALT_INV_DATA_OUT[4]~3_combout\ <= NOT \inst2|DATA_OUT[4]~3_combout\;
\inst5|ALT_INV_Q\(4) <= NOT \inst5|Q\(4);
\inst3|ALT_INV_Q\(4) <= NOT \inst3|Q\(4);
\inst|ALT_INV_Mux2~0_combout\ <= NOT \inst|Mux2~0_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(2) <= NOT \inst|Div0|auto_generated|divider|divider|sel\(2);
\inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5) <= NOT \inst|Div0|auto_generated|divider|divider|sel\(5);
\inst|ALT_INV_ALU_RESULT~2_combout\ <= NOT \inst|ALU_RESULT~2_combout\;
\inst2|ALT_INV_DATA_OUT[5]~2_combout\ <= NOT \inst2|DATA_OUT[5]~2_combout\;
\inst5|ALT_INV_Q\(5) <= NOT \inst5|Q\(5);
\inst3|ALT_INV_Q\(5) <= NOT \inst3|Q\(5);
\inst|ALT_INV_Mux1~0_combout\ <= NOT \inst|Mux1~0_combout\;
\inst|ALT_INV_ALU_RESULT~1_combout\ <= NOT \inst|ALU_RESULT~1_combout\;
\inst2|ALT_INV_DATA_OUT[6]~1_combout\ <= NOT \inst2|DATA_OUT[6]~1_combout\;
\inst5|ALT_INV_Q\(6) <= NOT \inst5|Q\(6);
\inst3|ALT_INV_Q\(6) <= NOT \inst3|Q\(6);
\inst|ALT_INV_Mux0~0_combout\ <= NOT \inst|Mux0~0_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(1) <= NOT \inst|Div0|auto_generated|divider|divider|sel\(1);
\inst4|ALT_INV_Q[2]~_Duplicate_1_q\ <= NOT \inst4|Q[2]~_Duplicate_1_q\;
\inst4|ALT_INV_Q[3]~_Duplicate_1_q\ <= NOT \inst4|Q[3]~_Duplicate_1_q\;
\inst4|ALT_INV_Q[4]~_Duplicate_1_q\ <= NOT \inst4|Q[4]~_Duplicate_1_q\;
\inst4|ALT_INV_Q[5]~_Duplicate_1_q\ <= NOT \inst4|Q[5]~_Duplicate_1_q\;
\inst4|ALT_INV_Q[6]~_Duplicate_1_q\ <= NOT \inst4|Q[6]~_Duplicate_1_q\;
\inst4|ALT_INV_Q[1]~_Duplicate_1_q\ <= NOT \inst4|Q[1]~_Duplicate_1_q\;
\inst|ALT_INV_ALU_RESULT~0_combout\ <= NOT \inst|ALU_RESULT~0_combout\;
\inst2|ALT_INV_DATA_OUT[7]~0_combout\ <= NOT \inst2|DATA_OUT[7]~0_combout\;
\inst5|ALT_INV_Q\(7) <= NOT \inst5|Q\(7);
\inst3|ALT_INV_Q\(7) <= NOT \inst3|Q\(7);
\inst4|ALT_INV_Q[7]~_Duplicate_1_q\ <= NOT \inst4|Q[7]~_Duplicate_1_q\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~29_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_7~29_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~25_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_7~25_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~25_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_6~25_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~21_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_7~21_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~21_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_6~21_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~21_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_5~21_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~17_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_7~17_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~17_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_6~17_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~17_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_5~17_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~17_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_4~17_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~13_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_7~13_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~13_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_6~13_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~13_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_5~13_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~13_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_4~13_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~13_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_3~13_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~9_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_7~9_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~9_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_6~9_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~9_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_5~9_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~9_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_4~9_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~9_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_3~9_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[0]~9_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~5_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_7~5_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~5_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_6~5_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~5_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_5~5_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~5_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_4~5_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~5_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_3~5_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[1]~5_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[0]~5_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\;
\inst|ALT_INV_Add0~33_sumout\ <= NOT \inst|Add0~33_sumout\;
\inst|ALT_INV_Add0~29_sumout\ <= NOT \inst|Add0~29_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_8~1_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_8~1_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_7~1_sumout\;
\inst|ALT_INV_Add0~25_sumout\ <= NOT \inst|Add0~25_sumout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\ <= NOT \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\;
\ALT_INV_clock~inputCLKENA0_outclk\ <= NOT \clock~inputCLKENA0_outclk\;
\ALT_INV_start~input_o\ <= NOT \start~input_o\;
\ALT_INV_acc~input_o\ <= NOT \acc~input_o\;
\ALT_INV_reset~input_o\ <= NOT \reset~input_o\;
\ALT_INV_clock~input_o\ <= NOT \clock~input_o\;
\ALT_INV_RST_A~input_o\ <= NOT \RST_A~input_o\;
\ALT_INV_RST_C~input_o\ <= NOT \RST_C~input_o\;
\ALT_INV_RST_B~input_o\ <= NOT \RST_B~input_o\;
\ALT_INV_OP[0]~input_o\ <= NOT \OP[0]~input_o\;
\ALT_INV_OP[1]~input_o\ <= NOT \OP[1]~input_o\;
\ALT_INV_OP[2]~input_o\ <= NOT \OP[2]~input_o\;
\inst10|ALT_INV_WRC~combout\ <= NOT \inst10|WRC~combout\;
\inst10|ALT_INV_SEL~combout\ <= NOT \inst10|SEL~combout\;
\inst|ALT_INV_temp\(8) <= NOT \inst|temp\(8);
\inst|ALT_INV_Mux6~1_combout\ <= NOT \inst|Mux6~1_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[40]~23_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[40]~23_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[41]~22_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[41]~22_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[32]~21_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[32]~21_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[42]~20_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[42]~20_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[33]~19_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[33]~19_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[24]~18_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[24]~18_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[43]~17_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[43]~17_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[34]~16_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[25]~15_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[25]~15_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[16]~14_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[44]~13_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[44]~13_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[35]~12_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[26]~11_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[26]~11_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[17]~10_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[17]~10_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[8]~9_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~8_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[45]~8_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~7_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[45]~7_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_selnose\(54) <= NOT \inst|Div0|auto_generated|divider|divider|selnose\(54);
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[36]~6_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[36]~6_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~5_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[27]~5_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~4_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[27]~4_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_selnose\(36) <= NOT \inst|Div0|auto_generated|divider|divider|selnose\(36);
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[18]~3_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[18]~3_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~2_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[9]~2_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~1_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[9]~1_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_selnose\(18) <= NOT \inst|Div0|auto_generated|divider|divider|selnose\(18);
\inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[0]~0_combout\ <= NOT \inst|Div0|auto_generated|divider|divider|StageOut[0]~0_combout\;
\inst|Div0|auto_generated|divider|divider|ALT_INV_selnose\(0) <= NOT \inst|Div0|auto_generated|divider|divider|selnose\(0);
\inst10|ALT_INV_WRC~1_combout\ <= NOT \inst10|WRC~1_combout\;
\inst10|ALT_INV_current_state.init~q\ <= NOT \inst10|current_state.init~q\;
\inst|ALT_INV_Mux7~5_combout\ <= NOT \inst|Mux7~5_combout\;
\inst10|ALT_INV_current_state.fetch~q\ <= NOT \inst10|current_state.fetch~q\;
\inst10|ALT_INV_current_state.accumulate~q\ <= NOT \inst10|current_state.accumulate~q\;
\inst10|ALT_INV_current_state.procc~q\ <= NOT \inst10|current_state.procc~q\;
\inst10|ALT_INV_current_state.write_result~q\ <= NOT \inst10|current_state.write_result~q\;
\inst|ALT_INV_Mux7~4_combout\ <= NOT \inst|Mux7~4_combout\;
\inst|ALT_INV_Mux7~3_combout\ <= NOT \inst|Mux7~3_combout\;
\inst|ALT_INV_Mux7~2_combout\ <= NOT \inst|Mux7~2_combout\;
\inst|ALT_INV_ALU_RESULT~7_combout\ <= NOT \inst|ALU_RESULT~7_combout\;
\inst|ALT_INV_ALU_RESULT~6_combout\ <= NOT \inst|ALU_RESULT~6_combout\;
\inst|ALT_INV_Mux7~1_combout\ <= NOT \inst|Mux7~1_combout\;
\inst|ALT_INV_LessThan0~2_combout\ <= NOT \inst|LessThan0~2_combout\;
\inst|ALT_INV_LessThan0~1_combout\ <= NOT \inst|LessThan0~1_combout\;
\inst|ALT_INV_LessThan0~0_combout\ <= NOT \inst|LessThan0~0_combout\;
\inst2|ALT_INV_DATA_OUT[0]~7_combout\ <= NOT \inst2|DATA_OUT[0]~7_combout\;
\inst|ALT_INV_Mux7~0_combout\ <= NOT \inst|Mux7~0_combout\;
\inst5|ALT_INV_Q\(0) <= NOT \inst5|Q\(0);
\inst4|ALT_INV_Q[0]~_Duplicate_1_q\ <= NOT \inst4|Q[0]~_Duplicate_1_q\;
\inst3|ALT_INV_Q\(0) <= NOT \inst3|Q\(0);
\inst2|ALT_INV_DATA_OUT[1]~6_combout\ <= NOT \inst2|DATA_OUT[1]~6_combout\;
\inst5|ALT_INV_Q\(1) <= NOT \inst5|Q\(1);
\inst3|ALT_INV_Q\(1) <= NOT \inst3|Q\(1);
\inst|ALT_INV_Mux5~0_combout\ <= NOT \inst|Mux5~0_combout\;
\inst|ALT_INV_ALU_RESULT~5_combout\ <= NOT \inst|ALU_RESULT~5_combout\;
\inst2|ALT_INV_DATA_OUT[2]~5_combout\ <= NOT \inst2|DATA_OUT[2]~5_combout\;
\inst5|ALT_INV_Q\(2) <= NOT \inst5|Q\(2);
\inst3|ALT_INV_Q\(2) <= NOT \inst3|Q\(2);

-- Location: IOOBUF_X48_Y0_N42
\carryout~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|temp\(8),
	devoe => ww_devoe,
	o => ww_carryout);

-- Location: IOOBUF_X51_Y0_N19
\ALU_OUT[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|Mux0~1_combout\,
	devoe => ww_devoe,
	o => ww_ALU_OUT(7));

-- Location: IOOBUF_X51_Y0_N53
\ALU_OUT[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|Mux1~1_combout\,
	devoe => ww_devoe,
	o => ww_ALU_OUT(6));

-- Location: IOOBUF_X53_Y61_N53
\ALU_OUT[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|Mux2~1_combout\,
	devoe => ww_devoe,
	o => ww_ALU_OUT(5));

-- Location: IOOBUF_X50_Y0_N19
\ALU_OUT[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|Mux3~1_combout\,
	devoe => ww_devoe,
	o => ww_ALU_OUT(4));

-- Location: IOOBUF_X68_Y26_N5
\ALU_OUT[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|Mux4~1_combout\,
	devoe => ww_devoe,
	o => ww_ALU_OUT(3));

-- Location: IOOBUF_X51_Y0_N36
\ALU_OUT[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|Mux5~1_combout\,
	devoe => ww_devoe,
	o => ww_ALU_OUT(2));

-- Location: IOOBUF_X46_Y0_N2
\ALU_OUT[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|Mux6~0_combout\,
	devoe => ww_devoe,
	o => ww_ALU_OUT(1));

-- Location: IOOBUF_X51_Y0_N2
\ALU_OUT[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|Mux7~6_combout\,
	devoe => ww_devoe,
	o => ww_ALU_OUT(0));

-- Location: IOOBUF_X68_Y27_N22
\RA_OUTPUT[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|Q\(7),
	devoe => ww_devoe,
	o => ww_RA_OUTPUT(7));

-- Location: IOOBUF_X68_Y24_N22
\RA_OUTPUT[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|Q\(6),
	devoe => ww_devoe,
	o => ww_RA_OUTPUT(6));

-- Location: IOOBUF_X68_Y12_N39
\RA_OUTPUT[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|Q\(5),
	devoe => ww_devoe,
	o => ww_RA_OUTPUT(5));

-- Location: IOOBUF_X68_Y10_N45
\RA_OUTPUT[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|Q\(4),
	devoe => ww_devoe,
	o => ww_RA_OUTPUT(4));

-- Location: IOOBUF_X68_Y17_N39
\RA_OUTPUT[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|Q\(3),
	devoe => ww_devoe,
	o => ww_RA_OUTPUT(3));

-- Location: IOOBUF_X68_Y19_N39
\RA_OUTPUT[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|Q\(2),
	devoe => ww_devoe,
	o => ww_RA_OUTPUT(2));

-- Location: IOOBUF_X68_Y17_N22
\RA_OUTPUT[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|Q\(1),
	devoe => ww_devoe,
	o => ww_RA_OUTPUT(1));

-- Location: IOOBUF_X68_Y10_N96
\RA_OUTPUT[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|Q\(0),
	devoe => ww_devoe,
	o => ww_RA_OUTPUT(0));

-- Location: IOOBUF_X68_Y16_N5
\RB_OUTPUT[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|Q[7]~_Duplicate_1_q\,
	devoe => ww_devoe,
	o => ww_RB_OUTPUT(7));

-- Location: IOOBUF_X68_Y13_N56
\RB_OUTPUT[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|Q[6]~_Duplicate_1_q\,
	devoe => ww_devoe,
	o => ww_RB_OUTPUT(6));

-- Location: IOOBUF_X68_Y17_N5
\RB_OUTPUT[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|Q[5]~_Duplicate_1_q\,
	devoe => ww_devoe,
	o => ww_RB_OUTPUT(5));

-- Location: IOOBUF_X68_Y14_N45
\RB_OUTPUT[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|Q[4]~_Duplicate_1_q\,
	devoe => ww_devoe,
	o => ww_RB_OUTPUT(4));

-- Location: IOOBUF_X68_Y16_N22
\RB_OUTPUT[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|Q[3]~_Duplicate_1_q\,
	devoe => ww_devoe,
	o => ww_RB_OUTPUT(3));

-- Location: IOOBUF_X68_Y14_N96
\RB_OUTPUT[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|Q[2]~_Duplicate_1_q\,
	devoe => ww_devoe,
	o => ww_RB_OUTPUT(2));

-- Location: IOOBUF_X68_Y14_N62
\RB_OUTPUT[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|Q[1]~_Duplicate_1_q\,
	devoe => ww_devoe,
	o => ww_RB_OUTPUT(1));

-- Location: IOOBUF_X68_Y14_N79
\RB_OUTPUT[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|Q[0]~_Duplicate_1_q\,
	devoe => ww_devoe,
	o => ww_RB_OUTPUT(0));

-- Location: IOOBUF_X50_Y0_N53
\RC_OUTPUT[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|Q\(7),
	devoe => ww_devoe,
	o => ww_RC_OUTPUT(7));

-- Location: IOOBUF_X48_Y0_N93
\RC_OUTPUT[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|Q\(6),
	devoe => ww_devoe,
	o => ww_RC_OUTPUT(6));

-- Location: IOOBUF_X68_Y12_N22
\RC_OUTPUT[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|Q\(5),
	devoe => ww_devoe,
	o => ww_RC_OUTPUT(5));

-- Location: IOOBUF_X68_Y13_N39
\RC_OUTPUT[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|Q\(4),
	devoe => ww_devoe,
	o => ww_RC_OUTPUT(4));

-- Location: IOOBUF_X68_Y22_N79
\RC_OUTPUT[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|Q\(3),
	devoe => ww_devoe,
	o => ww_RC_OUTPUT(3));

-- Location: IOOBUF_X68_Y17_N56
\RC_OUTPUT[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|Q\(2),
	devoe => ww_devoe,
	o => ww_RC_OUTPUT(2));

-- Location: IOOBUF_X68_Y19_N5
\RC_OUTPUT[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|Q\(1),
	devoe => ww_devoe,
	o => ww_RC_OUTPUT(1));

-- Location: IOOBUF_X68_Y12_N5
\RC_OUTPUT[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|Q\(0),
	devoe => ww_devoe,
	o => ww_RC_OUTPUT(0));

-- Location: IOOBUF_X53_Y0_N2
\STATE_OUT[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst10|current_state.write_result~q\,
	devoe => ww_devoe,
	o => ww_STATE_OUT(2));

-- Location: IOOBUF_X68_Y26_N22
\STATE_OUT[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst10|STATE_OUT\(1),
	devoe => ww_devoe,
	o => ww_STATE_OUT(1));

-- Location: IOOBUF_X53_Y0_N19
\STATE_OUT[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst10|STATE_OUT\(0),
	devoe => ww_devoe,
	o => ww_STATE_OUT(0));

-- Location: IOIBUF_X68_Y16_N38
\OP[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_OP(0),
	o => \OP[0]~input_o\);

-- Location: IOIBUF_X68_Y22_N61
\clock~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clock,
	o => \clock~input_o\);

-- Location: CLKCTRL_G10
\clock~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \clock~input_o\,
	outclk => \clock~inputCLKENA0_outclk\);

-- Location: IOIBUF_X53_Y0_N52
\start~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_start,
	o => \start~input_o\);

-- Location: IOIBUF_X68_Y24_N38
\reset~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reset,
	o => \reset~input_o\);

-- Location: FF_X64_Y14_N38
\inst10|current_state.write_result\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clock~inputCLKENA0_outclk\,
	asdata => \inst10|current_state.procc~q\,
	clrn => \ALT_INV_reset~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|current_state.write_result~q\);

-- Location: LABCELL_X64_Y14_N12
\inst10|Selector4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|Selector4~0_combout\ = ( \inst10|current_state.init~q\ & ( !\inst10|current_state.write_result~q\ ) ) # ( !\inst10|current_state.init~q\ & ( (\start~input_o\ & !\inst10|current_state.write_result~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000111100001111000000110000001100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_start~input_o\,
	datac => \inst10|ALT_INV_current_state.write_result~q\,
	datae => \inst10|ALT_INV_current_state.init~q\,
	combout => \inst10|Selector4~0_combout\);

-- Location: FF_X64_Y14_N14
\inst10|current_state.init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clock~inputCLKENA0_outclk\,
	d => \inst10|Selector4~0_combout\,
	clrn => \ALT_INV_reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|current_state.init~q\);

-- Location: IOIBUF_X68_Y26_N55
\acc~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_acc,
	o => \acc~input_o\);

-- Location: LABCELL_X64_Y14_N24
\inst10|next_state.accumulate~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|next_state.accumulate~0_combout\ = ( \start~input_o\ & ( \acc~input_o\ & ( !\inst10|current_state.init~q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst10|ALT_INV_current_state.init~q\,
	datae => \ALT_INV_start~input_o\,
	dataf => \ALT_INV_acc~input_o\,
	combout => \inst10|next_state.accumulate~0_combout\);

-- Location: FF_X64_Y14_N26
\inst10|current_state.accumulate\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clock~inputCLKENA0_outclk\,
	d => \inst10|next_state.accumulate~0_combout\,
	clrn => \ALT_INV_reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|current_state.accumulate~q\);

-- Location: LABCELL_X64_Y14_N9
\inst10|next_state.fetch~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|next_state.fetch~0_combout\ = ( \start~input_o\ & ( !\inst10|current_state.init~q\ & ( !\acc~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_acc~input_o\,
	datae => \ALT_INV_start~input_o\,
	dataf => \inst10|ALT_INV_current_state.init~q\,
	combout => \inst10|next_state.fetch~0_combout\);

-- Location: FF_X64_Y14_N11
\inst10|current_state.fetch\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clock~input_o\,
	d => \inst10|next_state.fetch~0_combout\,
	clrn => \ALT_INV_reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|current_state.fetch~q\);

-- Location: LABCELL_X64_Y14_N21
\inst10|next_state.procc\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|next_state.procc~combout\ = ( \inst10|current_state.fetch~q\ ) # ( !\inst10|current_state.fetch~q\ & ( \inst10|current_state.accumulate~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst10|ALT_INV_current_state.accumulate~q\,
	dataf => \inst10|ALT_INV_current_state.fetch~q\,
	combout => \inst10|next_state.procc~combout\);

-- Location: FF_X64_Y14_N23
\inst10|current_state.procc\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clock~inputCLKENA0_outclk\,
	d => \inst10|next_state.procc~combout\,
	clrn => \ALT_INV_reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|current_state.procc~q\);

-- Location: LABCELL_X64_Y15_N15
\inst10|SEL\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|SEL~combout\ = ( \inst10|current_state.accumulate~q\ & ( (!\inst10|current_state.procc~q\) # (\inst10|SEL~combout\) ) ) # ( !\inst10|current_state.accumulate~q\ & ( (\inst10|SEL~combout\ & \inst10|current_state.procc~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111111111111000011111111111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst10|ALT_INV_SEL~combout\,
	datad => \inst10|ALT_INV_current_state.procc~q\,
	dataf => \inst10|ALT_INV_current_state.accumulate~q\,
	combout => \inst10|SEL~combout\);

-- Location: LABCELL_X64_Y14_N51
\inst10|WRC~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|WRC~1_combout\ = ( \inst10|current_state.write_result~q\ & ( \inst10|current_state.accumulate~q\ ) ) # ( !\inst10|current_state.write_result~q\ & ( \inst10|current_state.accumulate~q\ ) ) # ( \inst10|current_state.write_result~q\ & ( 
-- !\inst10|current_state.accumulate~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst10|ALT_INV_current_state.write_result~q\,
	dataf => \inst10|ALT_INV_current_state.accumulate~q\,
	combout => \inst10|WRC~1_combout\);

-- Location: LABCELL_X64_Y14_N3
\inst10|WRC\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|WRC~combout\ = ( \inst10|WRC~1_combout\ & ( (!\inst10|current_state.procc~q\) # (\inst10|WRC~combout\) ) ) # ( !\inst10|WRC~1_combout\ & ( (\inst10|WRC~combout\ & \inst10|current_state.procc~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010111111111010101011111111101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst10|ALT_INV_WRC~combout\,
	datad => \inst10|ALT_INV_current_state.procc~q\,
	dataf => \inst10|ALT_INV_WRC~1_combout\,
	combout => \inst10|WRC~combout\);

-- Location: IOIBUF_X53_Y0_N35
\A[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(7),
	o => \A[7]~input_o\);

-- Location: IOIBUF_X68_Y13_N21
\RST_A~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_RST_A,
	o => \RST_A~input_o\);

-- Location: FF_X64_Y15_N14
\inst5|Q[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \A[7]~input_o\,
	clrn => \ALT_INV_RST_A~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|Q\(7));

-- Location: IOIBUF_X68_Y12_N55
\B[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(7),
	o => \B[7]~input_o\);

-- Location: IOIBUF_X68_Y13_N4
\RST_B~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_RST_B,
	o => \RST_B~input_o\);

-- Location: FF_X64_Y15_N29
\inst4|Q[7]~_Duplicate_1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \B[7]~input_o\,
	clrn => \ALT_INV_RST_B~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|Q[7]~_Duplicate_1_q\);

-- Location: IOIBUF_X68_Y24_N4
\A[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(6),
	o => \A[6]~input_o\);

-- Location: FF_X63_Y15_N32
\inst5|Q[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \A[6]~input_o\,
	clrn => \ALT_INV_RST_A~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|Q\(6));

-- Location: LABCELL_X63_Y15_N51
\inst2|DATA_OUT[6]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|DATA_OUT[6]~1_combout\ = ( \inst5|Q\(6) & ( (!\inst10|SEL~combout\) # (\inst3|Q\(6)) ) ) # ( !\inst5|Q\(6) & ( (\inst3|Q\(6) & \inst10|SEL~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111111111111000011111111111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst3|ALT_INV_Q\(6),
	datad => \inst10|ALT_INV_SEL~combout\,
	dataf => \inst5|ALT_INV_Q\(6),
	combout => \inst2|DATA_OUT[6]~1_combout\);

-- Location: IOIBUF_X46_Y0_N18
\B[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(6),
	o => \B[6]~input_o\);

-- Location: FF_X64_Y15_N2
\inst4|Q[6]~_Duplicate_1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \B[6]~input_o\,
	clrn => \ALT_INV_RST_B~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|Q[6]~_Duplicate_1_q\);

-- Location: LABCELL_X64_Y14_N0
\inst|ALU_RESULT~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|ALU_RESULT~1_combout\ = ( \inst4|Q[6]~_Duplicate_1_q\ & ( !\inst2|DATA_OUT[6]~1_combout\ ) ) # ( !\inst4|Q[6]~_Duplicate_1_q\ & ( \inst2|DATA_OUT[6]~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_DATA_OUT[6]~1_combout\,
	dataf => \inst4|ALT_INV_Q[6]~_Duplicate_1_q\,
	combout => \inst|ALU_RESULT~1_combout\);

-- Location: IOIBUF_X68_Y11_N38
\B[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(2),
	o => \B[2]~input_o\);

-- Location: FF_X65_Y15_N17
\inst4|Q[2]~_Duplicate_1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \B[2]~input_o\,
	clrn => \ALT_INV_RST_B~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|Q[2]~_Duplicate_1_q\);

-- Location: IOIBUF_X50_Y0_N35
\B[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(4),
	o => \B[4]~input_o\);

-- Location: FF_X65_Y14_N56
\inst4|Q[4]~_Duplicate_1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \B[4]~input_o\,
	clrn => \ALT_INV_RST_B~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|Q[4]~_Duplicate_1_q\);

-- Location: IOIBUF_X68_Y11_N21
\B[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(3),
	o => \B[3]~input_o\);

-- Location: FF_X64_Y15_N11
\inst4|Q[3]~_Duplicate_1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \B[3]~input_o\,
	clrn => \ALT_INV_RST_B~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|Q[3]~_Duplicate_1_q\);

-- Location: IOIBUF_X68_Y10_N61
\B[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(5),
	o => \B[5]~input_o\);

-- Location: FF_X65_Y14_N47
\inst4|Q[5]~_Duplicate_1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \B[5]~input_o\,
	clrn => \ALT_INV_RST_B~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|Q[5]~_Duplicate_1_q\);

-- Location: LABCELL_X64_Y15_N9
\inst|Div0|auto_generated|divider|divider|sel[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|sel\(1) = ( \inst4|Q[3]~_Duplicate_1_q\ & ( \inst4|Q[5]~_Duplicate_1_q\ ) ) # ( !\inst4|Q[3]~_Duplicate_1_q\ & ( \inst4|Q[5]~_Duplicate_1_q\ ) ) # ( \inst4|Q[3]~_Duplicate_1_q\ & ( !\inst4|Q[5]~_Duplicate_1_q\ ) ) 
-- # ( !\inst4|Q[3]~_Duplicate_1_q\ & ( !\inst4|Q[5]~_Duplicate_1_q\ & ( (((\inst4|Q[6]~_Duplicate_1_q\) # (\inst4|Q[4]~_Duplicate_1_q\)) # (\inst4|Q[2]~_Duplicate_1_q\)) # (\inst4|Q[7]~_Duplicate_1_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	datab => \inst4|ALT_INV_Q[2]~_Duplicate_1_q\,
	datac => \inst4|ALT_INV_Q[4]~_Duplicate_1_q\,
	datad => \inst4|ALT_INV_Q[6]~_Duplicate_1_q\,
	datae => \inst4|ALT_INV_Q[3]~_Duplicate_1_q\,
	dataf => \inst4|ALT_INV_Q[5]~_Duplicate_1_q\,
	combout => \inst|Div0|auto_generated|divider|divider|sel\(1));

-- Location: IOIBUF_X68_Y11_N55
\A[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(0),
	o => \A[0]~input_o\);

-- Location: FF_X64_Y15_N56
\inst5|Q[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \A[0]~input_o\,
	clrn => \ALT_INV_RST_A~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|Q\(0));

-- Location: IOIBUF_X68_Y11_N4
\B[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(0),
	o => \B[0]~input_o\);

-- Location: FF_X64_Y15_N8
\inst4|Q[0]~_Duplicate_1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \B[0]~input_o\,
	clrn => \ALT_INV_RST_B~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|Q[0]~_Duplicate_1_q\);

-- Location: LABCELL_X64_Y15_N30
\inst|Add0~38\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Add0~38_cout\ = CARRY(( \OP[0]~input_o\ ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	cin => GND,
	cout => \inst|Add0~38_cout\);

-- Location: LABCELL_X64_Y15_N33
\inst|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Add0~29_sumout\ = SUM(( (!\inst10|SEL~combout\ & ((\inst5|Q\(0)))) # (\inst10|SEL~combout\ & (\inst3|Q\(0))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[0]~_Duplicate_1_q\) ) + ( \inst|Add0~38_cout\ ))
-- \inst|Add0~30\ = CARRY(( (!\inst10|SEL~combout\ & ((\inst5|Q\(0)))) # (\inst10|SEL~combout\ & (\inst3|Q\(0))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[0]~_Duplicate_1_q\) ) + ( \inst|Add0~38_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010100101010100000000000000000000001111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst3|ALT_INV_Q\(0),
	datac => \inst10|ALT_INV_SEL~combout\,
	datad => \inst5|ALT_INV_Q\(0),
	dataf => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	cin => \inst|Add0~38_cout\,
	sumout => \inst|Add0~29_sumout\,
	cout => \inst|Add0~30\);

-- Location: IOIBUF_X68_Y22_N44
\OP[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_OP(2),
	o => \OP[2]~input_o\);

-- Location: LABCELL_X64_Y15_N24
\inst|Div0|auto_generated|divider|divider|sel[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|sel\(5) = (\inst4|Q[6]~_Duplicate_1_q\) # (\inst4|Q[7]~_Duplicate_1_q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100111111001111110011111100111111001111110011111100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	datac => \inst4|ALT_INV_Q[6]~_Duplicate_1_q\,
	combout => \inst|Div0|auto_generated|divider|divider|sel\(5));

-- Location: LABCELL_X65_Y14_N15
\inst|Div0|auto_generated|divider|divider|sel[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|sel\(4) = ( \inst|Div0|auto_generated|divider|divider|sel\(5) ) # ( !\inst|Div0|auto_generated|divider|divider|sel\(5) & ( \inst4|Q[5]~_Duplicate_1_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst4|ALT_INV_Q[5]~_Duplicate_1_q\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	combout => \inst|Div0|auto_generated|divider|divider|sel\(4));

-- Location: LABCELL_X65_Y14_N42
\inst|Div0|auto_generated|divider|divider|sel[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|sel\(3) = (\inst|Div0|auto_generated|divider|divider|sel\(4)) # (\inst4|Q[4]~_Duplicate_1_q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111111111000011111111111100001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst4|ALT_INV_Q[4]~_Duplicate_1_q\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	combout => \inst|Div0|auto_generated|divider|divider|sel\(3));

-- Location: IOIBUF_X68_Y10_N78
\B[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(1),
	o => \B[1]~input_o\);

-- Location: FF_X64_Y15_N26
\inst4|Q[1]~_Duplicate_1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \B[1]~input_o\,
	clrn => \ALT_INV_RST_B~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|Q[1]~_Duplicate_1_q\);

-- Location: LABCELL_X64_Y15_N18
\inst2|DATA_OUT[7]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|DATA_OUT[7]~0_combout\ = ( \inst5|Q\(7) & ( (!\inst10|SEL~combout\) # (\inst3|Q\(7)) ) ) # ( !\inst5|Q\(7) & ( (\inst10|SEL~combout\ & \inst3|Q\(7)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111111110000111111111111000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst10|ALT_INV_SEL~combout\,
	datad => \inst3|ALT_INV_Q\(7),
	dataf => \inst5|ALT_INV_Q\(7),
	combout => \inst2|DATA_OUT[7]~0_combout\);

-- Location: LABCELL_X64_Y15_N0
\inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\ = SUM(( !\inst4|Q[0]~_Duplicate_1_q\ $ (!\inst2|DATA_OUT[7]~0_combout\) ) + ( !VCC ) + ( !VCC ))
-- \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~6\ = CARRY(( !\inst4|Q[0]~_Duplicate_1_q\ $ (!\inst2|DATA_OUT[7]~0_combout\) ) + ( !VCC ) + ( !VCC ))
-- \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~7\ = SHARE((!\inst4|Q[0]~_Duplicate_1_q\) # (\inst2|DATA_OUT[7]~0_combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011111100111100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	datac => \inst2|ALT_INV_DATA_OUT[7]~0_combout\,
	cin => GND,
	sharein => GND,
	sumout => \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~6\,
	shareout => \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~7\);

-- Location: LABCELL_X64_Y15_N3
\inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\ = SUM(( VCC ) + ( \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~7\ ) + ( \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~6\,
	sharein => \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~7\,
	sumout => \inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\);

-- Location: LABCELL_X64_Y15_N27
\inst|Div0|auto_generated|divider|divider|selnose[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|selnose\(0) = ( \inst|Div0|auto_generated|divider|divider|sel\(1) ) # ( !\inst|Div0|auto_generated|divider|divider|sel\(1) & ( (\inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\) # 
-- (\inst4|Q[1]~_Duplicate_1_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010111111111010101011111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[1]~1_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(1),
	combout => \inst|Div0|auto_generated|divider|divider|selnose\(0));

-- Location: LABCELL_X65_Y15_N30
\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\ = SUM(( !\inst4|Q[0]~_Duplicate_1_q\ $ (!\inst2|DATA_OUT[6]~1_combout\) ) + ( !VCC ) + ( !VCC ))
-- \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\ = CARRY(( !\inst4|Q[0]~_Duplicate_1_q\ $ (!\inst2|DATA_OUT[6]~1_combout\) ) + ( !VCC ) + ( !VCC ))
-- \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\ = SHARE((!\inst4|Q[0]~_Duplicate_1_q\) # (\inst2|DATA_OUT[6]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111111100000000000000000000111111110000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	datad => \inst2|ALT_INV_DATA_OUT[6]~1_combout\,
	cin => GND,
	sharein => GND,
	sumout => \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\,
	shareout => \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\);

-- Location: LABCELL_X65_Y15_N33
\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\ = SUM(( !\inst4|Q[1]~_Duplicate_1_q\ $ (((!\inst|Div0|auto_generated|divider|divider|selnose\(0) & ((\inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\))) 
-- # (\inst|Div0|auto_generated|divider|divider|selnose\(0) & (\inst2|DATA_OUT[7]~0_combout\)))) ) + ( \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\ ) + ( \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\ ))
-- \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~6\ = CARRY(( !\inst4|Q[1]~_Duplicate_1_q\ $ (((!\inst|Div0|auto_generated|divider|divider|selnose\(0) & ((\inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|selnose\(0) & (\inst2|DATA_OUT[7]~0_combout\)))) ) + ( \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\ ) + ( \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\ ))
-- \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~7\ = SHARE((!\inst4|Q[1]~_Duplicate_1_q\ & ((!\inst|Div0|auto_generated|divider|divider|selnose\(0) & ((\inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|selnose\(0) & (\inst2|DATA_OUT[7]~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000010100010001000000000000000001010010110011001",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	datab => \inst2|ALT_INV_DATA_OUT[7]~0_combout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[0]~5_sumout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_selnose\(0),
	cin => \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~10\,
	sharein => \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~11\,
	sumout => \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~6\,
	shareout => \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~7\);

-- Location: LABCELL_X65_Y15_N36
\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ = SUM(( VCC ) + ( \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~7\ ) + ( \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~6\,
	sharein => \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~7\,
	sumout => \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\);

-- Location: LABCELL_X65_Y15_N24
\inst|Div0|auto_generated|divider|divider|StageOut[9]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[9]~1_combout\ = ( \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(1) & 
-- !\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011000000110000001100000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(1),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[1]~5_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[9]~1_combout\);

-- Location: LABCELL_X64_Y15_N21
\inst|Div0|auto_generated|divider|divider|StageOut[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[0]~0_combout\ = ( \inst2|DATA_OUT[7]~0_combout\ & ( (((\inst4|Q[1]~_Duplicate_1_q\) # (\inst|Div0|auto_generated|divider|divider|sel\(1))) # 
-- (\inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\)) # (\inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\) ) ) # ( !\inst2|DATA_OUT[7]~0_combout\ & ( 
-- (!\inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[0]~5_sumout\ & (!\inst|Div0|auto_generated|divider|divider|sel\(1) & !\inst4|Q[1]~_Duplicate_1_q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000000000001000000000000001111111111111110111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[1]~1_sumout\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[0]~5_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(1),
	datad => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	dataf => \inst2|ALT_INV_DATA_OUT[7]~0_combout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[0]~0_combout\);

-- Location: LABCELL_X65_Y15_N9
\inst|Div0|auto_generated|divider|divider|op_3~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_3~5_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ((!\inst|Div0|auto_generated|divider|divider|sel\(1) & 
-- (\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\)) # (\inst|Div0|auto_generated|divider|divider|sel\(1) & ((\inst|Div0|auto_generated|divider|divider|StageOut[0]~0_combout\))))) # 
-- (\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & (((\inst|Div0|auto_generated|divider|divider|StageOut[0]~0_combout\)))) ) + ( !\inst4|Q[2]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_3~10\ ))
-- \inst|Div0|auto_generated|divider|divider|op_3~6\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ((!\inst|Div0|auto_generated|divider|divider|sel\(1) & 
-- (\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[1]~5_sumout\)) # (\inst|Div0|auto_generated|divider|divider|sel\(1) & ((\inst|Div0|auto_generated|divider|divider|StageOut[0]~0_combout\))))) # 
-- (\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & (((\inst|Div0|auto_generated|divider|divider|StageOut[0]~0_combout\)))) ) + ( !\inst4|Q[2]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_3~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(1),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[1]~5_sumout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[0]~0_combout\,
	dataf => \inst4|ALT_INV_Q[2]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_3~10\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_3~5_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_3~6\);

-- Location: LABCELL_X65_Y15_N12
\inst|Div0|auto_generated|divider|divider|op_3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ = SUM(( VCC ) + ( GND ) + ( \inst|Div0|auto_generated|divider|divider|op_3~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \inst|Div0|auto_generated|divider|divider|op_3~6\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_3~1_sumout\);

-- Location: IOIBUF_X68_Y22_N95
\A[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(2),
	o => \A[2]~input_o\);

-- Location: FF_X65_Y15_N29
\inst5|Q[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \A[2]~input_o\,
	clrn => \ALT_INV_RST_A~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|Q\(2));

-- Location: IOIBUF_X68_Y19_N55
\A[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(1),
	o => \A[1]~input_o\);

-- Location: FF_X65_Y15_N20
\inst5|Q[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \A[1]~input_o\,
	clrn => \ALT_INV_RST_A~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|Q\(1));

-- Location: LABCELL_X64_Y15_N36
\inst|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Add0~25_sumout\ = SUM(( !\OP[0]~input_o\ $ (!\inst4|Q[1]~_Duplicate_1_q\) ) + ( (!\inst10|SEL~combout\ & ((\inst5|Q\(1)))) # (\inst10|SEL~combout\ & (\inst3|Q\(1))) ) + ( \inst|Add0~30\ ))
-- \inst|Add0~26\ = CARRY(( !\OP[0]~input_o\ $ (!\inst4|Q[1]~_Duplicate_1_q\) ) + ( (!\inst10|SEL~combout\ & ((\inst5|Q\(1)))) # (\inst10|SEL~combout\ & (\inst3|Q\(1))) ) + ( \inst|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111000011000000000000000000000101010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst10|ALT_INV_SEL~combout\,
	datac => \inst3|ALT_INV_Q\(1),
	datad => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	dataf => \inst5|ALT_INV_Q\(1),
	cin => \inst|Add0~30\,
	sumout => \inst|Add0~25_sumout\,
	cout => \inst|Add0~26\);

-- Location: LABCELL_X64_Y15_N39
\inst|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Add0~21_sumout\ = SUM(( !\OP[0]~input_o\ $ (!\inst4|Q[2]~_Duplicate_1_q\) ) + ( (!\inst10|SEL~combout\ & ((\inst5|Q\(2)))) # (\inst10|SEL~combout\ & (\inst3|Q\(2))) ) + ( \inst|Add0~26\ ))
-- \inst|Add0~22\ = CARRY(( !\OP[0]~input_o\ $ (!\inst4|Q[2]~_Duplicate_1_q\) ) + ( (!\inst10|SEL~combout\ & ((\inst5|Q\(2)))) # (\inst10|SEL~combout\ & (\inst3|Q\(2))) ) + ( \inst|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111000011000000000000000000000101010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst10|ALT_INV_SEL~combout\,
	datac => \inst3|ALT_INV_Q\(2),
	datad => \inst4|ALT_INV_Q[2]~_Duplicate_1_q\,
	dataf => \inst5|ALT_INV_Q\(2),
	cin => \inst|Add0~26\,
	sumout => \inst|Add0~21_sumout\,
	cout => \inst|Add0~22\);

-- Location: LABCELL_X67_Y15_N3
\inst|ALU_RESULT~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|ALU_RESULT~5_combout\ = ( !\inst4|Q[2]~_Duplicate_1_q\ & ( \inst2|DATA_OUT[2]~5_combout\ ) ) # ( \inst4|Q[2]~_Duplicate_1_q\ & ( !\inst2|DATA_OUT[2]~5_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111111111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst4|ALT_INV_Q[2]~_Duplicate_1_q\,
	dataf => \inst2|ALT_INV_DATA_OUT[2]~5_combout\,
	combout => \inst|ALU_RESULT~5_combout\);

-- Location: IOIBUF_X68_Y27_N38
\A[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(3),
	o => \A[3]~input_o\);

-- Location: FF_X63_Y15_N14
\inst5|Q[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \A[3]~input_o\,
	clrn => \ALT_INV_RST_A~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|Q\(3));

-- Location: LABCELL_X63_Y15_N18
\inst|ALU_RESULT~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|ALU_RESULT~4_combout\ = ( \inst2|DATA_OUT[3]~4_combout\ & ( !\inst4|Q[3]~_Duplicate_1_q\ ) ) # ( !\inst2|DATA_OUT[3]~4_combout\ & ( \inst4|Q[3]~_Duplicate_1_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst4|ALT_INV_Q[3]~_Duplicate_1_q\,
	dataf => \inst2|ALT_INV_DATA_OUT[3]~4_combout\,
	combout => \inst|ALU_RESULT~4_combout\);

-- Location: LABCELL_X64_Y15_N42
\inst|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Add0~17_sumout\ = SUM(( (!\inst10|SEL~combout\ & ((\inst5|Q\(3)))) # (\inst10|SEL~combout\ & (\inst3|Q\(3))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[3]~_Duplicate_1_q\) ) + ( \inst|Add0~22\ ))
-- \inst|Add0~18\ = CARRY(( (!\inst10|SEL~combout\ & ((\inst5|Q\(3)))) # (\inst10|SEL~combout\ & (\inst3|Q\(3))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[3]~_Duplicate_1_q\) ) + ( \inst|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010100101010100000000000000000000001111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst10|ALT_INV_SEL~combout\,
	datac => \inst3|ALT_INV_Q\(3),
	datad => \inst5|ALT_INV_Q\(3),
	dataf => \inst4|ALT_INV_Q[3]~_Duplicate_1_q\,
	cin => \inst|Add0~22\,
	sumout => \inst|Add0~17_sumout\,
	cout => \inst|Add0~18\);

-- Location: IOIBUF_X50_Y0_N1
\A[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(4),
	o => \A[4]~input_o\);

-- Location: FF_X65_Y15_N26
\inst5|Q[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \A[4]~input_o\,
	clrn => \ALT_INV_RST_A~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|Q\(4));

-- Location: LABCELL_X64_Y15_N45
\inst|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Add0~13_sumout\ = SUM(( (!\inst10|SEL~combout\ & ((\inst5|Q\(4)))) # (\inst10|SEL~combout\ & (\inst3|Q\(4))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[4]~_Duplicate_1_q\) ) + ( \inst|Add0~18\ ))
-- \inst|Add0~14\ = CARRY(( (!\inst10|SEL~combout\ & ((\inst5|Q\(4)))) # (\inst10|SEL~combout\ & (\inst3|Q\(4))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[4]~_Duplicate_1_q\) ) + ( \inst|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010100101010100000000000000000000001111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst10|ALT_INV_SEL~combout\,
	datac => \inst3|ALT_INV_Q\(4),
	datad => \inst5|ALT_INV_Q\(4),
	dataf => \inst4|ALT_INV_Q[4]~_Duplicate_1_q\,
	cin => \inst|Add0~18\,
	sumout => \inst|Add0~13_sumout\,
	cout => \inst|Add0~14\);

-- Location: LABCELL_X65_Y14_N30
\inst|ALU_RESULT~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|ALU_RESULT~3_combout\ = !\inst4|Q[4]~_Duplicate_1_q\ $ (!\inst2|DATA_OUT[4]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010110101010010101011010101001010101101010100101010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[4]~_Duplicate_1_q\,
	datad => \inst2|ALT_INV_DATA_OUT[4]~3_combout\,
	combout => \inst|ALU_RESULT~3_combout\);

-- Location: IOIBUF_X68_Y24_N55
\A[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(5),
	o => \A[5]~input_o\);

-- Location: FF_X65_Y15_N23
\inst5|Q[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|current_state.fetch~q\,
	asdata => \A[5]~input_o\,
	clrn => \ALT_INV_RST_A~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|Q\(5));

-- Location: LABCELL_X64_Y14_N54
\inst2|DATA_OUT[5]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|DATA_OUT[5]~2_combout\ = ( \inst5|Q\(5) & ( (!\inst10|SEL~combout\) # (\inst3|Q\(5)) ) ) # ( !\inst5|Q\(5) & ( (\inst10|SEL~combout\ & \inst3|Q\(5)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111111110000111111111111000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst10|ALT_INV_SEL~combout\,
	datad => \inst3|ALT_INV_Q\(5),
	dataf => \inst5|ALT_INV_Q\(5),
	combout => \inst2|DATA_OUT[5]~2_combout\);

-- Location: DSP_X66_Y13_N0
\inst|Mult0~8\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 8,
	ay_scan_in_clock => "0",
	ay_scan_in_width => 8,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	by_clock => "none",
	by_use_scan_in => "false",
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m9x9",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "false",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	aclr => \inst|Mult0~8_ACLR_bus\,
	clk => \inst|Mult0~8_CLK_bus\,
	ena => \inst|Mult0~8_ENA_bus\,
	ax => \inst|Mult0~8_AX_bus\,
	ay => \inst|Mult0~8_AY_bus\,
	resulta => \inst|Mult0~8_RESULTA_bus\);

-- Location: LABCELL_X65_Y14_N36
\inst|Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux3~0_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( (!\OP[0]~input_o\ & \inst|Mult0~12\) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( (!\OP[0]~input_o\ & ((\inst|Mult0~12\))) # (\OP[0]~input_o\ & 
-- (!\inst|Div0|auto_generated|divider|divider|sel\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000011111010010100001111101000000000101010100000000010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(3),
	datad => \inst|ALT_INV_Mult0~12\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	combout => \inst|Mux3~0_combout\);

-- Location: IOIBUF_X68_Y16_N55
\OP[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_OP(1),
	o => \OP[1]~input_o\);

-- Location: LABCELL_X63_Y15_N54
\inst|Mux3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux3~1_combout\ = ( \inst|Mux3~0_combout\ & ( \OP[1]~input_o\ & ( !\OP[2]~input_o\ ) ) ) # ( \inst|Mux3~0_combout\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ & (((\inst|Add0~13_sumout\)))) # (\OP[2]~input_o\ & (!\OP[0]~input_o\ $ 
-- (((!\inst|ALU_RESULT~3_combout\))))) ) ) ) # ( !\inst|Mux3~0_combout\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ & (((\inst|Add0~13_sumout\)))) # (\OP[2]~input_o\ & (!\OP[0]~input_o\ $ (((!\inst|ALU_RESULT~3_combout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001110100101110000111010010111000000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \ALT_INV_OP[2]~input_o\,
	datac => \inst|ALT_INV_Add0~13_sumout\,
	datad => \inst|ALT_INV_ALU_RESULT~3_combout\,
	datae => \inst|ALT_INV_Mux3~0_combout\,
	dataf => \ALT_INV_OP[1]~input_o\,
	combout => \inst|Mux3~1_combout\);

-- Location: IOIBUF_X68_Y19_N21
\RST_C~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_RST_C,
	o => \RST_C~input_o\);

-- Location: FF_X63_Y15_N50
\inst3|Q[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|WRC~combout\,
	asdata => \inst|Mux3~1_combout\,
	clrn => \ALT_INV_RST_C~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst3|Q\(4));

-- Location: LABCELL_X65_Y14_N54
\inst2|DATA_OUT[4]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|DATA_OUT[4]~3_combout\ = ( \inst5|Q\(4) & ( (!\inst10|SEL~combout\) # (\inst3|Q\(4)) ) ) # ( !\inst5|Q\(4) & ( (\inst10|SEL~combout\ & \inst3|Q\(4)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001111001111110011111100111111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst10|ALT_INV_SEL~combout\,
	datac => \inst3|ALT_INV_Q\(4),
	dataf => \inst5|ALT_INV_Q\(4),
	combout => \inst2|DATA_OUT[4]~3_combout\);

-- Location: LABCELL_X65_Y15_N27
\inst|Div0|auto_generated|divider|divider|StageOut[9]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[9]~2_combout\ = ( \inst|Div0|auto_generated|divider|divider|StageOut[0]~0_combout\ & ( (\inst|Div0|auto_generated|divider|divider|sel\(1)) # 
-- (\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001110111011101110111011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(1),
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[0]~0_combout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[9]~2_combout\);

-- Location: LABCELL_X65_Y14_N33
\inst|Div0|auto_generated|divider|divider|sel[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|sel\(2) = ( \inst4|Q[3]~_Duplicate_1_q\ ) # ( !\inst4|Q[3]~_Duplicate_1_q\ & ( ((\inst4|Q[5]~_Duplicate_1_q\) # (\inst|Div0|auto_generated|divider|divider|sel\(5))) # (\inst4|Q[4]~_Duplicate_1_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111111111111010111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[4]~_Duplicate_1_q\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datad => \inst4|ALT_INV_Q[5]~_Duplicate_1_q\,
	dataf => \inst4|ALT_INV_Q[3]~_Duplicate_1_q\,
	combout => \inst|Div0|auto_generated|divider|divider|sel\(2));

-- Location: LABCELL_X65_Y16_N9
\inst|Div0|auto_generated|divider|divider|StageOut[18]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[18]~3_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ( (\inst|Div0|auto_generated|divider|divider|StageOut[9]~2_combout\) # 
-- (\inst|Div0|auto_generated|divider|divider|StageOut[9]~1_combout\) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(2) & (((\inst|Div0|auto_generated|divider|divider|op_3~5_sumout\)))) 
-- # (\inst|Div0|auto_generated|divider|divider|sel\(2) & (((\inst|Div0|auto_generated|divider|divider|StageOut[9]~2_combout\)) # (\inst|Div0|auto_generated|divider|divider|StageOut[9]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101110111000011110111011101110111011101110111011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~1_combout\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~2_combout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~5_sumout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(2),
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[18]~3_combout\);

-- Location: LABCELL_X65_Y15_N21
\inst|Div0|auto_generated|divider|divider|selnose[18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|selnose\(18) = ( \inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ ) # ( !\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ( \inst|Div0|auto_generated|divider|divider|sel\(2) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(2),
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|selnose\(18));

-- Location: LABCELL_X65_Y15_N6
\inst|Div0|auto_generated|divider|divider|op_3~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_3~9_sumout\ = SUM(( !\inst4|Q[1]~_Duplicate_1_q\ ) + ( (!\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ((!\inst|Div0|auto_generated|divider|divider|sel\(1) & 
-- (\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\)) # (\inst|Div0|auto_generated|divider|divider|sel\(1) & ((\inst2|DATA_OUT[6]~1_combout\))))) # (\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & 
-- (((\inst2|DATA_OUT[6]~1_combout\)))) ) + ( \inst|Div0|auto_generated|divider|divider|op_3~14\ ))
-- \inst|Div0|auto_generated|divider|divider|op_3~10\ = CARRY(( !\inst4|Q[1]~_Duplicate_1_q\ ) + ( (!\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ((!\inst|Div0|auto_generated|divider|divider|sel\(1) & 
-- (\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\)) # (\inst|Div0|auto_generated|divider|divider|sel\(1) & ((\inst2|DATA_OUT[6]~1_combout\))))) # (\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & 
-- (((\inst2|DATA_OUT[6]~1_combout\)))) ) + ( \inst|Div0|auto_generated|divider|divider|op_3~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111101111000000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(1),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[0]~9_sumout\,
	datad => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	dataf => \inst2|ALT_INV_DATA_OUT[6]~1_combout\,
	cin => \inst|Div0|auto_generated|divider|divider|op_3~14\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_3~9_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_3~10\);

-- Location: LABCELL_X65_Y15_N18
\inst|Div0|auto_generated|divider|divider|StageOut[8]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\ = ( \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ( \inst2|DATA_OUT[6]~1_combout\ ) ) # ( 
-- !\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(1) & (\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[0]~9_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|sel\(1) & ((\inst2|DATA_OUT[6]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100011101000111010001110100011100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[0]~9_sumout\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(1),
	datac => \inst2|ALT_INV_DATA_OUT[6]~1_combout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\);

-- Location: LABCELL_X65_Y15_N0
\inst|Div0|auto_generated|divider|divider|op_3~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_3~18_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \inst|Div0|auto_generated|divider|divider|op_3~18_cout\);

-- Location: LABCELL_X65_Y15_N3
\inst|Div0|auto_generated|divider|divider|op_3~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_3~13_sumout\ = SUM(( (!\inst10|SEL~combout\ & ((\inst5|Q\(5)))) # (\inst10|SEL~combout\ & (\inst3|Q\(5))) ) + ( !\inst4|Q[0]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_3~18_cout\ ))
-- \inst|Div0|auto_generated|divider|divider|op_3~14\ = CARRY(( (!\inst10|SEL~combout\ & ((\inst5|Q\(5)))) # (\inst10|SEL~combout\ & (\inst3|Q\(5))) ) + ( !\inst4|Q[0]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_3~18_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100000000000000000000001111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	datab => \inst3|ALT_INV_Q\(5),
	datac => \inst10|ALT_INV_SEL~combout\,
	datad => \inst5|ALT_INV_Q\(5),
	cin => \inst|Div0|auto_generated|divider|divider|op_3~18_cout\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_3~13_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_3~14\);

-- Location: LABCELL_X65_Y15_N42
\inst|Div0|auto_generated|divider|divider|op_4~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_4~22_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \inst|Div0|auto_generated|divider|divider|op_4~22_cout\);

-- Location: LABCELL_X65_Y15_N45
\inst|Div0|auto_generated|divider|divider|op_4~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_4~17_sumout\ = SUM(( !\inst4|Q[0]~_Duplicate_1_q\ ) + ( (!\inst10|SEL~combout\ & ((\inst5|Q\(4)))) # (\inst10|SEL~combout\ & (\inst3|Q\(4))) ) + ( \inst|Div0|auto_generated|divider|divider|op_4~22_cout\ ))
-- \inst|Div0|auto_generated|divider|divider|op_4~18\ = CARRY(( !\inst4|Q[0]~_Duplicate_1_q\ ) + ( (!\inst10|SEL~combout\ & ((\inst5|Q\(4)))) # (\inst10|SEL~combout\ & (\inst3|Q\(4))) ) + ( \inst|Div0|auto_generated|divider|divider|op_4~22_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110010101100101000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|ALT_INV_Q\(4),
	datab => \inst5|ALT_INV_Q\(4),
	datac => \inst10|ALT_INV_SEL~combout\,
	datad => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_4~22_cout\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_4~17_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_4~18\);

-- Location: LABCELL_X65_Y15_N48
\inst|Div0|auto_generated|divider|divider|op_4~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_4~13_sumout\ = SUM(( !\inst4|Q[1]~_Duplicate_1_q\ ) + ( (!\inst|Div0|auto_generated|divider|divider|sel\(2) & ((!\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & 
-- (\inst|Div0|auto_generated|divider|divider|op_3~13_sumout\)) # (\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ((\inst2|DATA_OUT[5]~2_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(2) & (((\inst2|DATA_OUT[5]~2_combout\)))) ) + 
-- ( \inst|Div0|auto_generated|divider|divider|op_4~18\ ))
-- \inst|Div0|auto_generated|divider|divider|op_4~14\ = CARRY(( !\inst4|Q[1]~_Duplicate_1_q\ ) + ( (!\inst|Div0|auto_generated|divider|divider|sel\(2) & ((!\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & 
-- (\inst|Div0|auto_generated|divider|divider|op_3~13_sumout\)) # (\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ((\inst2|DATA_OUT[5]~2_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(2) & (((\inst2|DATA_OUT[5]~2_combout\)))) ) + 
-- ( \inst|Div0|auto_generated|divider|divider|op_4~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111101111000000000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(2),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~13_sumout\,
	datad => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	dataf => \inst2|ALT_INV_DATA_OUT[5]~2_combout\,
	cin => \inst|Div0|auto_generated|divider|divider|op_4~18\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_4~13_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_4~14\);

-- Location: LABCELL_X65_Y15_N51
\inst|Div0|auto_generated|divider|divider|op_4~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_4~9_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(2) & ((!\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_3~9_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(2) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\)))) ) + ( !\inst4|Q[2]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_4~14\ ))
-- \inst|Div0|auto_generated|divider|divider|op_4~10\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(2) & ((!\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_3~9_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(2) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\)))) ) + ( !\inst4|Q[2]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_4~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(2),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~9_sumout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[8]~9_combout\,
	dataf => \inst4|ALT_INV_Q[2]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_4~14\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_4~9_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_4~10\);

-- Location: LABCELL_X65_Y15_N54
\inst|Div0|auto_generated|divider|divider|op_4~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_4~5_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|selnose\(18) & (((\inst|Div0|auto_generated|divider|divider|op_3~5_sumout\)))) # (\inst|Div0|auto_generated|divider|divider|selnose\(18) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[9]~2_combout\)) # (\inst|Div0|auto_generated|divider|divider|StageOut[9]~1_combout\))) ) + ( !\inst4|Q[3]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_4~10\ ))
-- \inst|Div0|auto_generated|divider|divider|op_4~6\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|selnose\(18) & (((\inst|Div0|auto_generated|divider|divider|op_3~5_sumout\)))) # (\inst|Div0|auto_generated|divider|divider|selnose\(18) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[9]~2_combout\)) # (\inst|Div0|auto_generated|divider|divider|StageOut[9]~1_combout\))) ) + ( !\inst4|Q[3]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_4~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000011010100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~1_combout\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~5_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_selnose\(18),
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[9]~2_combout\,
	dataf => \inst4|ALT_INV_Q[3]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_4~10\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_4~5_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_4~6\);

-- Location: LABCELL_X65_Y14_N9
\inst|Div0|auto_generated|divider|divider|StageOut[17]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[17]~10_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_3~9_sumout\ & ( \inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ( 
-- \inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\ ) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_3~9_sumout\ & ( \inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ( 
-- \inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\ ) ) ) # ( \inst|Div0|auto_generated|divider|divider|op_3~9_sumout\ & ( !\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(2)) # 
-- (\inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\) ) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_3~9_sumout\ & ( !\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ( (\inst|Div0|auto_generated|divider|divider|sel\(2) & 
-- \inst|Div0|auto_generated|divider|divider|StageOut[8]~9_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101101011111010111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(2),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[8]~9_combout\,
	datae => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~9_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[17]~10_combout\);

-- Location: LABCELL_X65_Y14_N51
\inst|Div0|auto_generated|divider|divider|StageOut[16]~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ( \inst2|DATA_OUT[5]~2_combout\ ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\ & ( 
-- (!\inst|Div0|auto_generated|divider|divider|sel\(2) & (\inst|Div0|auto_generated|divider|divider|op_3~13_sumout\)) # (\inst|Div0|auto_generated|divider|divider|sel\(2) & ((\inst2|DATA_OUT[5]~2_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101001011111000010100101111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(2),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~13_sumout\,
	datad => \inst2|ALT_INV_DATA_OUT[5]~2_combout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\);

-- Location: LABCELL_X65_Y16_N12
\inst|Div0|auto_generated|divider|divider|op_5~26\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_5~26_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \inst|Div0|auto_generated|divider|divider|op_5~26_cout\);

-- Location: LABCELL_X65_Y16_N15
\inst|Div0|auto_generated|divider|divider|op_5~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_5~21_sumout\ = SUM(( (!\inst10|SEL~combout\ & ((\inst5|Q\(3)))) # (\inst10|SEL~combout\ & (\inst3|Q\(3))) ) + ( !\inst4|Q[0]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_5~26_cout\ ))
-- \inst|Div0|auto_generated|divider|divider|op_5~22\ = CARRY(( (!\inst10|SEL~combout\ & ((\inst5|Q\(3)))) # (\inst10|SEL~combout\ & (\inst3|Q\(3))) ) + ( !\inst4|Q[0]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_5~26_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100000000000000000000001111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	datab => \inst10|ALT_INV_SEL~combout\,
	datac => \inst3|ALT_INV_Q\(3),
	datad => \inst5|ALT_INV_Q\(3),
	cin => \inst|Div0|auto_generated|divider|divider|op_5~26_cout\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_5~21_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_5~22\);

-- Location: LABCELL_X65_Y16_N18
\inst|Div0|auto_generated|divider|divider|op_5~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_5~17_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(3) & ((!\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_4~17_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((\inst2|DATA_OUT[4]~3_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(3) & (((\inst2|DATA_OUT[4]~3_combout\)))) ) + ( !\inst4|Q[1]~_Duplicate_1_q\ ) + ( 
-- \inst|Div0|auto_generated|divider|divider|op_5~22\ ))
-- \inst|Div0|auto_generated|divider|divider|op_5~18\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(3) & ((!\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_4~17_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((\inst2|DATA_OUT[4]~3_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(3) & (((\inst2|DATA_OUT[4]~3_combout\)))) ) + ( !\inst4|Q[1]~_Duplicate_1_q\ ) + ( 
-- \inst|Div0|auto_generated|divider|divider|op_5~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(3),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~17_sumout\,
	datad => \inst2|ALT_INV_DATA_OUT[4]~3_combout\,
	dataf => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_5~22\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_5~17_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_5~18\);

-- Location: LABCELL_X65_Y16_N21
\inst|Div0|auto_generated|divider|divider|op_5~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_5~13_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(3) & ((!\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_4~13_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(3) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\)))) ) + ( !\inst4|Q[2]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_5~18\ ))
-- \inst|Div0|auto_generated|divider|divider|op_5~14\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(3) & ((!\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_4~13_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(3) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\)))) ) + ( !\inst4|Q[2]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_5~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(3),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~13_sumout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[16]~14_combout\,
	dataf => \inst4|ALT_INV_Q[2]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_5~18\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_5~13_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_5~14\);

-- Location: LABCELL_X65_Y16_N24
\inst|Div0|auto_generated|divider|divider|op_5~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_5~9_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(3) & ((!\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_4~9_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[17]~10_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(3) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[17]~10_combout\)))) ) + ( !\inst4|Q[3]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_5~14\ ))
-- \inst|Div0|auto_generated|divider|divider|op_5~10\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(3) & ((!\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_4~9_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[17]~10_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(3) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[17]~10_combout\)))) ) + ( !\inst4|Q[3]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_5~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(3),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[17]~10_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~9_sumout\,
	dataf => \inst4|ALT_INV_Q[3]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_5~14\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_5~9_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_5~10\);

-- Location: LABCELL_X65_Y16_N27
\inst|Div0|auto_generated|divider|divider|op_5~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_5~5_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(3) & ((!\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_4~5_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[18]~3_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(3) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[18]~3_combout\)))) ) + ( !\inst4|Q[4]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_5~10\ ))
-- \inst|Div0|auto_generated|divider|divider|op_5~6\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(3) & ((!\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_4~5_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[18]~3_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(3) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[18]~3_combout\)))) ) + ( !\inst4|Q[4]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_5~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(3),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[18]~3_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~5_sumout\,
	dataf => \inst4|ALT_INV_Q[4]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_5~10\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_5~5_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_5~6\);

-- Location: LABCELL_X65_Y16_N30
\inst|Div0|auto_generated|divider|divider|op_5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ = SUM(( VCC ) + ( GND ) + ( \inst|Div0|auto_generated|divider|divider|op_5~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \inst|Div0|auto_generated|divider|divider|op_5~6\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_5~1_sumout\);

-- Location: LABCELL_X65_Y14_N39
\inst|Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux4~0_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( (!\OP[0]~input_o\ & \inst|Mult0~11\) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( (!\OP[0]~input_o\ & ((\inst|Mult0~11\))) # (\OP[0]~input_o\ & 
-- (!\inst|Div0|auto_generated|divider|divider|sel\(4))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000011111010010100001111101000000000101010100000000010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	datad => \inst|ALT_INV_Mult0~11\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	combout => \inst|Mux4~0_combout\);

-- Location: LABCELL_X63_Y15_N0
\inst|Mux4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux4~1_combout\ = ( \inst|Mux4~0_combout\ & ( \OP[1]~input_o\ & ( !\OP[2]~input_o\ ) ) ) # ( \inst|Mux4~0_combout\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ & (((\inst|Add0~17_sumout\)))) # (\OP[2]~input_o\ & (!\inst|ALU_RESULT~4_combout\ $ 
-- (((!\OP[0]~input_o\))))) ) ) ) # ( !\inst|Mux4~0_combout\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ & (((\inst|Add0~17_sumout\)))) # (\OP[2]~input_o\ & (!\inst|ALU_RESULT~4_combout\ $ (((!\OP[0]~input_o\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001101011010001100110101101000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|ALT_INV_ALU_RESULT~4_combout\,
	datab => \inst|ALT_INV_Add0~17_sumout\,
	datac => \ALT_INV_OP[0]~input_o\,
	datad => \ALT_INV_OP[2]~input_o\,
	datae => \inst|ALT_INV_Mux4~0_combout\,
	dataf => \ALT_INV_OP[1]~input_o\,
	combout => \inst|Mux4~1_combout\);

-- Location: FF_X63_Y15_N11
\inst3|Q[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|WRC~combout\,
	asdata => \inst|Mux4~1_combout\,
	clrn => \ALT_INV_RST_C~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst3|Q\(3));

-- Location: LABCELL_X63_Y15_N48
\inst2|DATA_OUT[3]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|DATA_OUT[3]~4_combout\ = ( \inst10|SEL~combout\ & ( \inst3|Q\(3) ) ) # ( !\inst10|SEL~combout\ & ( \inst5|Q\(3) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst5|ALT_INV_Q\(3),
	datac => \inst3|ALT_INV_Q\(3),
	dataf => \inst10|ALT_INV_SEL~combout\,
	combout => \inst2|DATA_OUT[3]~4_combout\);

-- Location: LABCELL_X63_Y15_N21
\inst|Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux5~0_combout\ = (!\OP[0]~input_o\ & (((\inst|Mult0~10\)))) # (\OP[0]~input_o\ & (!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (!\inst|Div0|auto_generated|divider|divider|sel\(5))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000011101010010000001110101001000000111010100100000011101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datad => \inst|ALT_INV_Mult0~10\,
	combout => \inst|Mux5~0_combout\);

-- Location: LABCELL_X63_Y15_N6
\inst|Mux5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux5~1_combout\ = ( \inst|Mux5~0_combout\ & ( \OP[1]~input_o\ & ( !\OP[2]~input_o\ ) ) ) # ( \inst|Mux5~0_combout\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ & (((\inst|Add0~21_sumout\)))) # (\OP[2]~input_o\ & (!\OP[0]~input_o\ $ 
-- (((!\inst|ALU_RESULT~5_combout\))))) ) ) ) # ( !\inst|Mux5~0_combout\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ & (((\inst|Add0~21_sumout\)))) # (\OP[2]~input_o\ & (!\OP[0]~input_o\ $ (((!\inst|ALU_RESULT~5_combout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001110100101110000111010010111000000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \ALT_INV_OP[2]~input_o\,
	datac => \inst|ALT_INV_Add0~21_sumout\,
	datad => \inst|ALT_INV_ALU_RESULT~5_combout\,
	datae => \inst|ALT_INV_Mux5~0_combout\,
	dataf => \ALT_INV_OP[1]~input_o\,
	combout => \inst|Mux5~1_combout\);

-- Location: FF_X63_Y15_N59
\inst3|Q[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|WRC~combout\,
	asdata => \inst|Mux5~1_combout\,
	clrn => \ALT_INV_RST_C~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst3|Q\(2));

-- Location: LABCELL_X67_Y16_N21
\inst2|DATA_OUT[2]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|DATA_OUT[2]~5_combout\ = ( \inst3|Q\(2) & ( \inst10|SEL~combout\ ) ) # ( \inst3|Q\(2) & ( !\inst10|SEL~combout\ & ( \inst5|Q\(2) ) ) ) # ( !\inst3|Q\(2) & ( !\inst10|SEL~combout\ & ( \inst5|Q\(2) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst5|ALT_INV_Q\(2),
	datae => \inst3|ALT_INV_Q\(2),
	dataf => \inst10|ALT_INV_SEL~combout\,
	combout => \inst2|DATA_OUT[2]~5_combout\);

-- Location: LABCELL_X65_Y16_N3
\inst|Div0|auto_generated|divider|divider|StageOut[27]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[27]~5_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( \inst|Div0|auto_generated|divider|divider|StageOut[18]~3_combout\ ) ) # ( 
-- !\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( (\inst|Div0|auto_generated|divider|divider|sel\(3) & \inst|Div0|auto_generated|divider|divider|StageOut[18]~3_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(3),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[18]~3_combout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[27]~5_combout\);

-- Location: LABCELL_X65_Y16_N6
\inst|Div0|auto_generated|divider|divider|StageOut[27]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[27]~4_combout\ = ( !\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(3) & \inst|Div0|auto_generated|divider|divider|op_4~5_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(3),
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~5_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[27]~4_combout\);

-- Location: LABCELL_X64_Y16_N12
\inst|Div0|auto_generated|divider|divider|StageOut[36]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[36]~6_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_5~5_sumout\ & ( \inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( 
-- (\inst|Div0|auto_generated|divider|divider|StageOut[27]~4_combout\) # (\inst|Div0|auto_generated|divider|divider|StageOut[27]~5_combout\) ) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_5~5_sumout\ & ( 
-- \inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( (\inst|Div0|auto_generated|divider|divider|StageOut[27]~4_combout\) # (\inst|Div0|auto_generated|divider|divider|StageOut[27]~5_combout\) ) ) ) # ( 
-- \inst|Div0|auto_generated|divider|divider|op_5~5_sumout\ & ( !\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(4)) # ((\inst|Div0|auto_generated|divider|divider|StageOut[27]~4_combout\) # 
-- (\inst|Div0|auto_generated|divider|divider|StageOut[27]~5_combout\)) ) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_5~5_sumout\ & ( !\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( (\inst|Div0|auto_generated|divider|divider|sel\(4) & 
-- ((\inst|Div0|auto_generated|divider|divider|StageOut[27]~4_combout\) # (\inst|Div0|auto_generated|divider|divider|StageOut[27]~5_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100110011110011111111111100001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~5_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~4_combout\,
	datae => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~5_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[36]~6_combout\);

-- Location: LABCELL_X65_Y16_N0
\inst|Div0|auto_generated|divider|divider|selnose[36]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|selnose\(36) = ( \inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ ) # ( !\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( \inst|Div0|auto_generated|divider|divider|sel\(4) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|selnose\(36));

-- Location: LABCELL_X65_Y14_N0
\inst|Div0|auto_generated|divider|divider|StageOut[26]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[26]~11_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( \inst|Div0|auto_generated|divider|divider|StageOut[17]~10_combout\ ) ) # ( 
-- !\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(3) & (\inst|Div0|auto_generated|divider|divider|op_4~9_sumout\)) # (\inst|Div0|auto_generated|divider|divider|sel\(3) & 
-- ((\inst|Div0|auto_generated|divider|divider|StageOut[17]~10_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000111111000011000011111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(3),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~9_sumout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[17]~10_combout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[26]~11_combout\);

-- Location: LABCELL_X65_Y14_N24
\inst|Div0|auto_generated|divider|divider|StageOut[25]~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[25]~15_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_4~13_sumout\ & ( \inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( 
-- \inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\ ) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_4~13_sumout\ & ( \inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( 
-- \inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\ ) ) ) # ( \inst|Div0|auto_generated|divider|divider|op_4~13_sumout\ & ( !\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(3)) # 
-- (\inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\) ) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_4~13_sumout\ & ( !\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( (\inst|Div0|auto_generated|divider|divider|sel\(3) 
-- & \inst|Div0|auto_generated|divider|divider|StageOut[16]~14_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011110011111100111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(3),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[16]~14_combout\,
	datae => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~13_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[25]~15_combout\);

-- Location: LABCELL_X65_Y14_N18
\inst|Div0|auto_generated|divider|divider|StageOut[24]~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[24]~18_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( \inst2|DATA_OUT[4]~3_combout\ ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ & ( 
-- (!\inst|Div0|auto_generated|divider|divider|sel\(3) & ((\inst|Div0|auto_generated|divider|divider|op_4~17_sumout\))) # (\inst|Div0|auto_generated|divider|divider|sel\(3) & (\inst2|DATA_OUT[4]~3_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111110011000000111111001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_DATA_OUT[4]~3_combout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(3),
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~17_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_4~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[24]~18_combout\);

-- Location: LABCELL_X65_Y16_N36
\inst|Div0|auto_generated|divider|divider|op_6~30\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_6~30_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \inst|Div0|auto_generated|divider|divider|op_6~30_cout\);

-- Location: LABCELL_X65_Y16_N39
\inst|Div0|auto_generated|divider|divider|op_6~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_6~25_sumout\ = SUM(( (!\inst10|SEL~combout\ & ((\inst5|Q\(2)))) # (\inst10|SEL~combout\ & (\inst3|Q\(2))) ) + ( !\inst4|Q[0]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_6~30_cout\ ))
-- \inst|Div0|auto_generated|divider|divider|op_6~26\ = CARRY(( (!\inst10|SEL~combout\ & ((\inst5|Q\(2)))) # (\inst10|SEL~combout\ & (\inst3|Q\(2))) ) + ( !\inst4|Q[0]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_6~30_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000001110100011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|ALT_INV_Q\(2),
	datab => \inst10|ALT_INV_SEL~combout\,
	datac => \inst5|ALT_INV_Q\(2),
	dataf => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_6~30_cout\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_6~25_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_6~26\);

-- Location: LABCELL_X65_Y16_N42
\inst|Div0|auto_generated|divider|divider|op_6~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_6~21_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(4) & ((!\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_5~21_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((\inst2|DATA_OUT[3]~4_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(4) & (((\inst2|DATA_OUT[3]~4_combout\)))) ) + ( !\inst4|Q[1]~_Duplicate_1_q\ ) + ( 
-- \inst|Div0|auto_generated|divider|divider|op_6~26\ ))
-- \inst|Div0|auto_generated|divider|divider|op_6~22\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(4) & ((!\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_5~21_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((\inst2|DATA_OUT[3]~4_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(4) & (((\inst2|DATA_OUT[3]~4_combout\)))) ) + ( !\inst4|Q[1]~_Duplicate_1_q\ ) + ( 
-- \inst|Div0|auto_generated|divider|divider|op_6~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~21_sumout\,
	datad => \inst2|ALT_INV_DATA_OUT[3]~4_combout\,
	dataf => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_6~26\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_6~21_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_6~22\);

-- Location: LABCELL_X65_Y16_N45
\inst|Div0|auto_generated|divider|divider|op_6~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_6~17_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(4) & ((!\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_5~17_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|StageOut[24]~18_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(4) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[24]~18_combout\)))) ) + ( !\inst4|Q[2]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_6~22\ ))
-- \inst|Div0|auto_generated|divider|divider|op_6~18\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(4) & ((!\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_5~17_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|StageOut[24]~18_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(4) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[24]~18_combout\)))) ) + ( !\inst4|Q[2]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_6~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~17_sumout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[24]~18_combout\,
	dataf => \inst4|ALT_INV_Q[2]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_6~22\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_6~17_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_6~18\);

-- Location: LABCELL_X65_Y16_N48
\inst|Div0|auto_generated|divider|divider|op_6~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_6~13_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(4) & ((!\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_5~13_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[25]~15_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(4) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[25]~15_combout\)))) ) + ( !\inst4|Q[3]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_6~18\ ))
-- \inst|Div0|auto_generated|divider|divider|op_6~14\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(4) & ((!\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_5~13_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[25]~15_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(4) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[25]~15_combout\)))) ) + ( !\inst4|Q[3]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_6~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[25]~15_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~13_sumout\,
	dataf => \inst4|ALT_INV_Q[3]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_6~18\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_6~13_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_6~14\);

-- Location: LABCELL_X65_Y16_N51
\inst|Div0|auto_generated|divider|divider|op_6~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_6~9_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(4) & ((!\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_5~9_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[26]~11_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(4) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[26]~11_combout\)))) ) + ( !\inst4|Q[4]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_6~14\ ))
-- \inst|Div0|auto_generated|divider|divider|op_6~10\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(4) & ((!\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_5~9_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[26]~11_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(4) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[26]~11_combout\)))) ) + ( !\inst4|Q[4]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_6~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[26]~11_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~9_sumout\,
	dataf => \inst4|ALT_INV_Q[4]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_6~14\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_6~9_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_6~10\);

-- Location: LABCELL_X65_Y16_N54
\inst|Div0|auto_generated|divider|divider|op_6~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_6~5_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|selnose\(36) & (\inst|Div0|auto_generated|divider|divider|op_5~5_sumout\)) # (\inst|Div0|auto_generated|divider|divider|selnose\(36) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[27]~5_combout\) # (\inst|Div0|auto_generated|divider|divider|StageOut[27]~4_combout\)))) ) + ( !\inst4|Q[5]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_6~10\ ))
-- \inst|Div0|auto_generated|divider|divider|op_6~6\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|selnose\(36) & (\inst|Div0|auto_generated|divider|divider|op_5~5_sumout\)) # (\inst|Div0|auto_generated|divider|divider|selnose\(36) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[27]~5_combout\) # (\inst|Div0|auto_generated|divider|divider|StageOut[27]~4_combout\)))) ) + ( !\inst4|Q[5]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_6~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000101001101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~5_sumout\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~4_combout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_selnose\(36),
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[27]~5_combout\,
	dataf => \inst4|ALT_INV_Q[5]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_6~10\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_6~5_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_6~6\);

-- Location: LABCELL_X64_Y16_N9
\inst|Div0|auto_generated|divider|divider|StageOut[35]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_5~9_sumout\ & ( ((!\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & !\inst|Div0|auto_generated|divider|divider|sel\(4))) # 
-- (\inst|Div0|auto_generated|divider|divider|StageOut[26]~11_combout\) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_5~9_sumout\ & ( (\inst|Div0|auto_generated|divider|divider|StageOut[26]~11_combout\ & 
-- ((\inst|Div0|auto_generated|divider|divider|sel\(4)) # (\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001110111000000000111011110001000111111111000100011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[26]~11_combout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~9_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\);

-- Location: LABCELL_X64_Y16_N6
\inst|Div0|auto_generated|divider|divider|StageOut[34]~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\ = ( \inst|Div0|auto_generated|divider|divider|StageOut[25]~15_combout\ & ( ((\inst|Div0|auto_generated|divider|divider|op_5~13_sumout\) # 
-- (\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\)) # (\inst|Div0|auto_generated|divider|divider|sel\(4)) ) ) # ( !\inst|Div0|auto_generated|divider|divider|StageOut[25]~15_combout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(4) & 
-- (!\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & \inst|Div0|auto_generated|divider|divider|op_5~13_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011000000000000001100000000111111111111110011111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~13_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[25]~15_combout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\);

-- Location: LABCELL_X64_Y16_N21
\inst|Div0|auto_generated|divider|divider|StageOut[33]~19\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[33]~19_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( \inst|Div0|auto_generated|divider|divider|StageOut[24]~18_combout\ ) ) # ( 
-- !\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(4) & ((\inst|Div0|auto_generated|divider|divider|op_5~17_sumout\))) # (\inst|Div0|auto_generated|divider|divider|sel\(4) & 
-- (\inst|Div0|auto_generated|divider|divider|StageOut[24]~18_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001110100011101000111010001110101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[24]~18_combout\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~17_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[33]~19_combout\);

-- Location: LABCELL_X64_Y16_N0
\inst|Div0|auto_generated|divider|divider|StageOut[32]~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[32]~21_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( \inst2|DATA_OUT[3]~4_combout\ ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_5~1_sumout\ & ( 
-- (!\inst|Div0|auto_generated|divider|divider|sel\(4) & ((\inst|Div0|auto_generated|divider|divider|op_5~21_sumout\))) # (\inst|Div0|auto_generated|divider|divider|sel\(4) & (\inst2|DATA_OUT[3]~4_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111001111000000111100111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(4),
	datac => \inst2|ALT_INV_DATA_OUT[3]~4_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~21_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_5~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[32]~21_combout\);

-- Location: LABCELL_X64_Y16_N24
\inst|Div0|auto_generated|divider|divider|op_7~34\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_7~34_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \inst|Div0|auto_generated|divider|divider|op_7~34_cout\);

-- Location: LABCELL_X64_Y16_N27
\inst|Div0|auto_generated|divider|divider|op_7~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_7~29_sumout\ = SUM(( (!\inst10|SEL~combout\ & ((\inst5|Q\(1)))) # (\inst10|SEL~combout\ & (\inst3|Q\(1))) ) + ( !\inst4|Q[0]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~34_cout\ ))
-- \inst|Div0|auto_generated|divider|divider|op_7~30\ = CARRY(( (!\inst10|SEL~combout\ & ((\inst5|Q\(1)))) # (\inst10|SEL~combout\ & (\inst3|Q\(1))) ) + ( !\inst4|Q[0]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~34_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000001000110111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst10|ALT_INV_SEL~combout\,
	datab => \inst3|ALT_INV_Q\(1),
	datad => \inst5|ALT_INV_Q\(1),
	dataf => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_7~34_cout\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_7~29_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_7~30\);

-- Location: LABCELL_X64_Y16_N30
\inst|Div0|auto_generated|divider|divider|op_7~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_7~25_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_6~25_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst2|DATA_OUT[2]~5_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & (((\inst2|DATA_OUT[2]~5_combout\)))) ) + ( !\inst4|Q[1]~_Duplicate_1_q\ ) + ( 
-- \inst|Div0|auto_generated|divider|divider|op_7~30\ ))
-- \inst|Div0|auto_generated|divider|divider|op_7~26\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_6~25_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst2|DATA_OUT[2]~5_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & (((\inst2|DATA_OUT[2]~5_combout\)))) ) + ( !\inst4|Q[1]~_Duplicate_1_q\ ) + ( 
-- \inst|Div0|auto_generated|divider|divider|op_7~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~25_sumout\,
	datad => \inst2|ALT_INV_DATA_OUT[2]~5_combout\,
	dataf => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_7~30\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_7~25_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_7~26\);

-- Location: LABCELL_X64_Y16_N33
\inst|Div0|auto_generated|divider|divider|op_7~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_7~21_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_6~21_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|StageOut[32]~21_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[32]~21_combout\)))) ) + ( !\inst4|Q[2]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~26\ ))
-- \inst|Div0|auto_generated|divider|divider|op_7~22\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_6~21_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|StageOut[32]~21_combout\))))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[32]~21_combout\)))) ) + ( !\inst4|Q[2]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~21_sumout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[32]~21_combout\,
	dataf => \inst4|ALT_INV_Q[2]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_7~26\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_7~21_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_7~22\);

-- Location: LABCELL_X64_Y16_N36
\inst|Div0|auto_generated|divider|divider|op_7~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_7~17_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_6~17_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[33]~19_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[33]~19_combout\)))) ) + ( !\inst4|Q[3]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~22\ ))
-- \inst|Div0|auto_generated|divider|divider|op_7~18\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_6~17_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[33]~19_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[33]~19_combout\)))) ) + ( !\inst4|Q[3]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[33]~19_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~17_sumout\,
	dataf => \inst4|ALT_INV_Q[3]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_7~22\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_7~17_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_7~18\);

-- Location: LABCELL_X64_Y16_N39
\inst|Div0|auto_generated|divider|divider|op_7~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_7~13_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_6~13_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\)))) ) + ( !\inst4|Q[4]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~18\ ))
-- \inst|Div0|auto_generated|divider|divider|op_7~14\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_6~13_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\)))) ) + ( !\inst4|Q[4]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[34]~16_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~13_sumout\,
	dataf => \inst4|ALT_INV_Q[4]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_7~18\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_7~13_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_7~14\);

-- Location: LABCELL_X64_Y16_N42
\inst|Div0|auto_generated|divider|divider|op_7~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_7~9_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_6~9_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\)))) ) + ( !\inst4|Q[5]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~14\ ))
-- \inst|Div0|auto_generated|divider|divider|op_7~10\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_6~9_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\)))) ) + ( !\inst4|Q[5]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[35]~12_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~9_sumout\,
	dataf => \inst4|ALT_INV_Q[5]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_7~14\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_7~9_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_7~10\);

-- Location: LABCELL_X64_Y16_N45
\inst|Div0|auto_generated|divider|divider|op_7~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_7~5_sumout\ = SUM(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_6~5_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[36]~6_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[36]~6_combout\)))) ) + ( !\inst4|Q[6]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~10\ ))
-- \inst|Div0|auto_generated|divider|divider|op_7~6\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & ((!\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_6~5_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[36]~6_combout\)))) # (\inst|Div0|auto_generated|divider|divider|sel\(5) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[36]~6_combout\)))) ) + ( !\inst4|Q[6]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_7~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[36]~6_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~5_sumout\,
	dataf => \inst4|ALT_INV_Q[6]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_7~10\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_7~5_sumout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_7~6\);

-- Location: LABCELL_X64_Y16_N48
\inst|Div0|auto_generated|divider|divider|op_7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ = SUM(( VCC ) + ( GND ) + ( \inst|Div0|auto_generated|divider|divider|op_7~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \inst|Div0|auto_generated|divider|divider|op_7~6\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_7~1_sumout\);

-- Location: LABCELL_X67_Y16_N33
\inst|Mux6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux6~1_combout\ = ( \inst4|Q[1]~_Duplicate_1_q\ & ( \OP[1]~input_o\ & ( !\OP[2]~input_o\ ) ) ) # ( !\inst4|Q[1]~_Duplicate_1_q\ & ( \OP[1]~input_o\ & ( !\OP[2]~input_o\ ) ) ) # ( \inst4|Q[1]~_Duplicate_1_q\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ 
-- & (\inst|Add0~25_sumout\)) # (\OP[2]~input_o\ & ((!\inst2|DATA_OUT[1]~6_combout\ $ (\OP[0]~input_o\)))) ) ) ) # ( !\inst4|Q[1]~_Duplicate_1_q\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ & (\inst|Add0~25_sumout\)) # (\OP[2]~input_o\ & 
-- ((!\inst2|DATA_OUT[1]~6_combout\ $ (!\OP[0]~input_o\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100011101110100011101000100011111001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|ALT_INV_Add0~25_sumout\,
	datab => \ALT_INV_OP[2]~input_o\,
	datac => \inst2|ALT_INV_DATA_OUT[1]~6_combout\,
	datad => \ALT_INV_OP[0]~input_o\,
	datae => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	dataf => \ALT_INV_OP[1]~input_o\,
	combout => \inst|Mux6~1_combout\);

-- Location: LABCELL_X64_Y16_N54
\inst|Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux6~0_combout\ = ( \OP[1]~input_o\ & ( \inst|Mux6~1_combout\ & ( (!\OP[0]~input_o\ & (\inst|Mult0~9\)) # (\OP[0]~input_o\ & (((!\inst4|Q[7]~_Duplicate_1_q\ & !\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\)))) ) ) ) # ( !\OP[1]~input_o\ & 
-- ( \inst|Mux6~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111110101010111000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|ALT_INV_Mult0~9\,
	datab => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datad => \ALT_INV_OP[0]~input_o\,
	datae => \ALT_INV_OP[1]~input_o\,
	dataf => \inst|ALT_INV_Mux6~1_combout\,
	combout => \inst|Mux6~0_combout\);

-- Location: FF_X64_Y16_N59
\inst3|Q[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|WRC~combout\,
	asdata => \inst|Mux6~0_combout\,
	clrn => \ALT_INV_RST_C~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst3|Q\(1));

-- Location: LABCELL_X67_Y16_N6
\inst2|DATA_OUT[1]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|DATA_OUT[1]~6_combout\ = ( \inst5|Q\(1) & ( (!\inst10|SEL~combout\) # (\inst3|Q\(1)) ) ) # ( !\inst5|Q\(1) & ( (\inst3|Q\(1) & \inst10|SEL~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001111111111001100111111111100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst3|ALT_INV_Q\(1),
	datad => \inst10|ALT_INV_SEL~combout\,
	dataf => \inst5|ALT_INV_Q\(1),
	combout => \inst2|DATA_OUT[1]~6_combout\);

-- Location: LABCELL_X65_Y14_N12
\inst|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux2~0_combout\ = ( \inst|Div0|auto_generated|divider|divider|sel\(2) & ( (\inst|Mult0~13\ & !\OP[0]~input_o\) ) ) # ( !\inst|Div0|auto_generated|divider|divider|sel\(2) & ( (!\OP[0]~input_o\ & ((\inst|Mult0~13\))) # (\OP[0]~input_o\ & 
-- (!\inst|Div0|auto_generated|divider|divider|op_3~1_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111110101010000011111010101000001111000000000000111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_3~1_sumout\,
	datac => \inst|ALT_INV_Mult0~13\,
	datad => \ALT_INV_OP[0]~input_o\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(2),
	combout => \inst|Mux2~0_combout\);

-- Location: LABCELL_X63_Y16_N33
\inst|ALU_RESULT~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|ALU_RESULT~2_combout\ = !\inst4|Q[5]~_Duplicate_1_q\ $ (!\inst2|DATA_OUT[5]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001011010010110100101101001011010010110100101101001011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[5]~_Duplicate_1_q\,
	datac => \inst2|ALT_INV_DATA_OUT[5]~2_combout\,
	combout => \inst|ALU_RESULT~2_combout\);

-- Location: LABCELL_X64_Y15_N48
\inst|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Add0~9_sumout\ = SUM(( (!\inst10|SEL~combout\ & ((\inst5|Q\(5)))) # (\inst10|SEL~combout\ & (\inst3|Q\(5))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[5]~_Duplicate_1_q\) ) + ( \inst|Add0~14\ ))
-- \inst|Add0~10\ = CARRY(( (!\inst10|SEL~combout\ & ((\inst5|Q\(5)))) # (\inst10|SEL~combout\ & (\inst3|Q\(5))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[5]~_Duplicate_1_q\) ) + ( \inst|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010100101010100000000000000000000001111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst10|ALT_INV_SEL~combout\,
	datac => \inst3|ALT_INV_Q\(5),
	datad => \inst5|ALT_INV_Q\(5),
	dataf => \inst4|ALT_INV_Q[5]~_Duplicate_1_q\,
	cin => \inst|Add0~14\,
	sumout => \inst|Add0~9_sumout\,
	cout => \inst|Add0~10\);

-- Location: LABCELL_X63_Y15_N36
\inst|Mux2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux2~1_combout\ = ( \inst|Add0~9_sumout\ & ( \OP[1]~input_o\ & ( (\inst|Mux2~0_combout\ & !\OP[2]~input_o\) ) ) ) # ( !\inst|Add0~9_sumout\ & ( \OP[1]~input_o\ & ( (\inst|Mux2~0_combout\ & !\OP[2]~input_o\) ) ) ) # ( \inst|Add0~9_sumout\ & ( 
-- !\OP[1]~input_o\ & ( (!\OP[2]~input_o\) # (!\OP[0]~input_o\ $ (!\inst|ALU_RESULT~2_combout\)) ) ) ) # ( !\inst|Add0~9_sumout\ & ( !\OP[1]~input_o\ & ( (\OP[2]~input_o\ & (!\OP[0]~input_o\ $ (!\inst|ALU_RESULT~2_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100110000110011111111110001000100010001000100010001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|ALT_INV_Mux2~0_combout\,
	datab => \ALT_INV_OP[2]~input_o\,
	datac => \ALT_INV_OP[0]~input_o\,
	datad => \inst|ALT_INV_ALU_RESULT~2_combout\,
	datae => \inst|ALT_INV_Add0~9_sumout\,
	dataf => \ALT_INV_OP[1]~input_o\,
	combout => \inst|Mux2~1_combout\);

-- Location: FF_X63_Y15_N5
\inst3|Q[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|WRC~combout\,
	asdata => \inst|Mux2~1_combout\,
	clrn => \ALT_INV_RST_C~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst3|Q\(5));

-- Location: LABCELL_X65_Y15_N57
\inst|Div0|auto_generated|divider|divider|op_4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_4~1_sumout\ = SUM(( VCC ) + ( GND ) + ( \inst|Div0|auto_generated|divider|divider|op_4~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \inst|Div0|auto_generated|divider|divider|op_4~6\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_4~1_sumout\);

-- Location: LABCELL_X65_Y16_N57
\inst|Div0|auto_generated|divider|divider|op_6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ = SUM(( VCC ) + ( GND ) + ( \inst|Div0|auto_generated|divider|divider|op_6~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \inst|Div0|auto_generated|divider|divider|op_6~6\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\);

-- Location: LABCELL_X64_Y16_N18
\inst|Div0|auto_generated|divider|divider|StageOut[45]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[45]~7_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_6~5_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & !\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~5_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[45]~7_combout\);

-- Location: LABCELL_X63_Y16_N54
\inst|Div0|auto_generated|divider|divider|selnose[54]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|selnose\(54) = ( \inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ ) # ( !\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & ( \inst4|Q[7]~_Duplicate_1_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|selnose\(54));

-- Location: LABCELL_X64_Y17_N39
\inst|Div0|auto_generated|divider|divider|StageOut[45]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[45]~8_combout\ = ( \inst|Div0|auto_generated|divider|divider|StageOut[36]~6_combout\ & ( \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ ) ) # ( 
-- \inst|Div0|auto_generated|divider|divider|StageOut[36]~6_combout\ & ( !\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( \inst|Div0|auto_generated|divider|divider|sel\(5) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datae => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[36]~6_combout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[45]~8_combout\);

-- Location: LABCELL_X64_Y17_N42
\inst|Div0|auto_generated|divider|divider|StageOut[44]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[44]~13_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_6~9_sumout\ & ( \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( 
-- \inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\ ) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_6~9_sumout\ & ( \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( 
-- \inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\ ) ) ) # ( \inst|Div0|auto_generated|divider|divider|op_6~9_sumout\ & ( !\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(5)) # 
-- (\inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\) ) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_6~9_sumout\ & ( !\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( (\inst|Div0|auto_generated|divider|divider|sel\(5) & 
-- \inst|Div0|auto_generated|divider|divider|StageOut[35]~12_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101101011111010111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[35]~12_combout\,
	datae => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~9_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[44]~13_combout\);

-- Location: LABCELL_X64_Y17_N33
\inst|Div0|auto_generated|divider|divider|StageOut[43]~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[43]~17_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_6~13_sumout\ & ( \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( 
-- \inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\ ) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_6~13_sumout\ & ( \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( 
-- \inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\ ) ) ) # ( \inst|Div0|auto_generated|divider|divider|op_6~13_sumout\ & ( !\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(5)) # 
-- (\inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\) ) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_6~13_sumout\ & ( !\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( (\inst|Div0|auto_generated|divider|divider|sel\(5) 
-- & \inst|Div0|auto_generated|divider|divider|StageOut[34]~16_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101101010101111111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[34]~16_combout\,
	datae => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~13_sumout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[43]~17_combout\);

-- Location: LABCELL_X64_Y16_N3
\inst|Div0|auto_generated|divider|divider|StageOut[42]~20\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[42]~20_combout\ = ( \inst|Div0|auto_generated|divider|divider|op_6~17_sumout\ & ( ((!\inst|Div0|auto_generated|divider|divider|sel\(5) & !\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|StageOut[33]~19_combout\) ) ) # ( !\inst|Div0|auto_generated|divider|divider|op_6~17_sumout\ & ( (\inst|Div0|auto_generated|divider|divider|StageOut[33]~19_combout\ & 
-- ((\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\) # (\inst|Div0|auto_generated|divider|divider|sel\(5)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001011111000000000101111110100000111111111010000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[33]~19_combout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~17_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[42]~20_combout\);

-- Location: LABCELL_X64_Y17_N12
\inst|Div0|auto_generated|divider|divider|StageOut[41]~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[41]~22_combout\ = ( \inst|Div0|auto_generated|divider|divider|StageOut[32]~21_combout\ & ( \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ ) ) # ( 
-- \inst|Div0|auto_generated|divider|divider|StageOut[32]~21_combout\ & ( !\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( (\inst|Div0|auto_generated|divider|divider|op_6~21_sumout\) # (\inst|Div0|auto_generated|divider|divider|sel\(5)) ) ) ) # 
-- ( !\inst|Div0|auto_generated|divider|divider|StageOut[32]~21_combout\ & ( !\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(5) & \inst|Div0|auto_generated|divider|divider|op_6~21_sumout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010010111110101111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~21_sumout\,
	datae => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[32]~21_combout\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[41]~22_combout\);

-- Location: LABCELL_X67_Y16_N3
\inst|Div0|auto_generated|divider|divider|StageOut[40]~23\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|StageOut[40]~23_combout\ = ( \inst|Div0|auto_generated|divider|divider|sel\(5) & ( \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( \inst2|DATA_OUT[2]~5_combout\ ) ) ) # ( 
-- !\inst|Div0|auto_generated|divider|divider|sel\(5) & ( \inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( \inst2|DATA_OUT[2]~5_combout\ ) ) ) # ( \inst|Div0|auto_generated|divider|divider|sel\(5) & ( 
-- !\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( \inst2|DATA_OUT[2]~5_combout\ ) ) ) # ( !\inst|Div0|auto_generated|divider|divider|sel\(5) & ( !\inst|Div0|auto_generated|divider|divider|op_6~1_sumout\ & ( 
-- \inst|Div0|auto_generated|divider|divider|op_6~25_sumout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_DATA_OUT[2]~5_combout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~25_sumout\,
	datae => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(5),
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_6~1_sumout\,
	combout => \inst|Div0|auto_generated|divider|divider|StageOut[40]~23_combout\);

-- Location: LABCELL_X63_Y16_N0
\inst|Div0|auto_generated|divider|divider|op_8~38\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_8~38_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \inst|Div0|auto_generated|divider|divider|op_8~38_cout\);

-- Location: LABCELL_X63_Y16_N3
\inst|Div0|auto_generated|divider|divider|op_8~34\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_8~34_cout\ = CARRY(( !\inst4|Q[0]~_Duplicate_1_q\ ) + ( (!\inst10|SEL~combout\ & (\inst5|Q\(0))) # (\inst10|SEL~combout\ & ((\inst3|Q\(0)))) ) + ( \inst|Div0|auto_generated|divider|divider|op_8~38_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101011001010110000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|ALT_INV_Q\(0),
	datab => \inst3|ALT_INV_Q\(0),
	datac => \inst10|ALT_INV_SEL~combout\,
	datad => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_8~38_cout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_8~34_cout\);

-- Location: LABCELL_X63_Y16_N6
\inst|Div0|auto_generated|divider|divider|op_8~30\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_8~30_cout\ = CARRY(( (!\inst4|Q[7]~_Duplicate_1_q\ & ((!\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_7~29_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((\inst2|DATA_OUT[1]~6_combout\))))) # (\inst4|Q[7]~_Duplicate_1_q\ & (((\inst2|DATA_OUT[1]~6_combout\)))) ) + ( !\inst4|Q[1]~_Duplicate_1_q\ ) + ( 
-- \inst|Div0|auto_generated|divider|divider|op_8~34_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~29_sumout\,
	datad => \inst2|ALT_INV_DATA_OUT[1]~6_combout\,
	dataf => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_8~34_cout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_8~30_cout\);

-- Location: LABCELL_X63_Y16_N9
\inst|Div0|auto_generated|divider|divider|op_8~26\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_8~26_cout\ = CARRY(( (!\inst4|Q[7]~_Duplicate_1_q\ & ((!\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|op_7~25_sumout\)) # 
-- (\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|StageOut[40]~23_combout\))))) # (\inst4|Q[7]~_Duplicate_1_q\ & (((\inst|Div0|auto_generated|divider|divider|StageOut[40]~23_combout\)))) ) + ( 
-- !\inst4|Q[2]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_8~30_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~25_sumout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[40]~23_combout\,
	dataf => \inst4|ALT_INV_Q[2]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_8~30_cout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_8~26_cout\);

-- Location: LABCELL_X63_Y16_N12
\inst|Div0|auto_generated|divider|divider|op_8~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_8~22_cout\ = CARRY(( (!\inst4|Q[7]~_Duplicate_1_q\ & ((!\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_7~21_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[41]~22_combout\)))) # (\inst4|Q[7]~_Duplicate_1_q\ & (((\inst|Div0|auto_generated|divider|divider|StageOut[41]~22_combout\)))) ) + ( 
-- !\inst4|Q[3]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_8~26_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[41]~22_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~21_sumout\,
	dataf => \inst4|ALT_INV_Q[3]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_8~26_cout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_8~22_cout\);

-- Location: LABCELL_X63_Y16_N15
\inst|Div0|auto_generated|divider|divider|op_8~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_8~18_cout\ = CARRY(( (!\inst4|Q[7]~_Duplicate_1_q\ & ((!\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_7~17_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[42]~20_combout\)))) # (\inst4|Q[7]~_Duplicate_1_q\ & (((\inst|Div0|auto_generated|divider|divider|StageOut[42]~20_combout\)))) ) + ( 
-- !\inst4|Q[4]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_8~22_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[42]~20_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~17_sumout\,
	dataf => \inst4|ALT_INV_Q[4]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_8~22_cout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_8~18_cout\);

-- Location: LABCELL_X63_Y16_N18
\inst|Div0|auto_generated|divider|divider|op_8~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_8~14_cout\ = CARRY(( (!\inst4|Q[7]~_Duplicate_1_q\ & ((!\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_7~13_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[43]~17_combout\)))) # (\inst4|Q[7]~_Duplicate_1_q\ & (((\inst|Div0|auto_generated|divider|divider|StageOut[43]~17_combout\)))) ) + ( 
-- !\inst4|Q[5]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_8~18_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[43]~17_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~13_sumout\,
	dataf => \inst4|ALT_INV_Q[5]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_8~18_cout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_8~14_cout\);

-- Location: LABCELL_X63_Y16_N21
\inst|Div0|auto_generated|divider|divider|op_8~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_8~10_cout\ = CARRY(( (!\inst4|Q[7]~_Duplicate_1_q\ & ((!\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & ((\inst|Div0|auto_generated|divider|divider|op_7~9_sumout\))) # 
-- (\inst|Div0|auto_generated|divider|divider|op_7~1_sumout\ & (\inst|Div0|auto_generated|divider|divider|StageOut[44]~13_combout\)))) # (\inst4|Q[7]~_Duplicate_1_q\ & (((\inst|Div0|auto_generated|divider|divider|StageOut[44]~13_combout\)))) ) + ( 
-- !\inst4|Q[6]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_8~14_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~1_sumout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[44]~13_combout\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~9_sumout\,
	dataf => \inst4|ALT_INV_Q[6]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_8~14_cout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_8~10_cout\);

-- Location: LABCELL_X63_Y16_N24
\inst|Div0|auto_generated|divider|divider|op_8~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_8~6_cout\ = CARRY(( (!\inst|Div0|auto_generated|divider|divider|selnose\(54) & (\inst|Div0|auto_generated|divider|divider|op_7~5_sumout\)) # (\inst|Div0|auto_generated|divider|divider|selnose\(54) & 
-- (((\inst|Div0|auto_generated|divider|divider|StageOut[45]~8_combout\) # (\inst|Div0|auto_generated|divider|divider|StageOut[45]~7_combout\)))) ) + ( !\inst4|Q[7]~_Duplicate_1_q\ ) + ( \inst|Div0|auto_generated|divider|divider|op_8~10_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000101001101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_7~5_sumout\,
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~7_combout\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_selnose\(54),
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_StageOut[45]~8_combout\,
	dataf => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	cin => \inst|Div0|auto_generated|divider|divider|op_8~10_cout\,
	cout => \inst|Div0|auto_generated|divider|divider|op_8~6_cout\);

-- Location: LABCELL_X63_Y16_N27
\inst|Div0|auto_generated|divider|divider|op_8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Div0|auto_generated|divider|divider|op_8~1_sumout\ = SUM(( VCC ) + ( GND ) + ( \inst|Div0|auto_generated|divider|divider|op_8~6_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \inst|Div0|auto_generated|divider|divider|op_8~6_cout\,
	sumout => \inst|Div0|auto_generated|divider|divider|op_8~1_sumout\);

-- Location: LABCELL_X63_Y16_N57
\inst|Mux7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux7~0_combout\ = ( \inst4|Q[0]~_Duplicate_1_q\ & ( !\OP[0]~input_o\ $ (((!\inst10|SEL~combout\ & (\inst5|Q\(0))) # (\inst10|SEL~combout\ & ((\inst3|Q\(0)))))) ) ) # ( !\inst4|Q[0]~_Duplicate_1_q\ & ( !\OP[0]~input_o\ $ (((!\inst10|SEL~combout\ & 
-- (!\inst5|Q\(0))) # (\inst10|SEL~combout\ & ((!\inst3|Q\(0)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110011001011010011001100101101010011001101001011001100110100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst5|ALT_INV_Q\(0),
	datac => \inst3|ALT_INV_Q\(0),
	datad => \inst10|ALT_INV_SEL~combout\,
	dataf => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	combout => \inst|Mux7~0_combout\);

-- Location: LABCELL_X65_Y14_N3
\inst|LessThan0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|LessThan0~0_combout\ = ( \inst2|DATA_OUT[1]~6_combout\ & ( (!\inst4|Q[1]~_Duplicate_1_q\) # ((!\inst4|Q[0]~_Duplicate_1_q\ & \inst2|DATA_OUT[0]~7_combout\)) ) ) # ( !\inst2|DATA_OUT[1]~6_combout\ & ( (!\inst4|Q[0]~_Duplicate_1_q\ & 
-- (!\inst4|Q[1]~_Duplicate_1_q\ & \inst2|DATA_OUT[0]~7_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010100000000000001010000011110000111110101111000011111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	datac => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	datad => \inst2|ALT_INV_DATA_OUT[0]~7_combout\,
	dataf => \inst2|ALT_INV_DATA_OUT[1]~6_combout\,
	combout => \inst|LessThan0~0_combout\);

-- Location: LABCELL_X65_Y14_N45
\inst|LessThan0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|LessThan0~1_combout\ = ( \inst2|DATA_OUT[3]~4_combout\ & ( (!\inst4|Q[3]~_Duplicate_1_q\) # ((!\inst4|Q[2]~_Duplicate_1_q\ & ((\inst|LessThan0~0_combout\) # (\inst2|DATA_OUT[2]~5_combout\))) # (\inst4|Q[2]~_Duplicate_1_q\ & 
-- (\inst2|DATA_OUT[2]~5_combout\ & \inst|LessThan0~0_combout\))) ) ) # ( !\inst2|DATA_OUT[3]~4_combout\ & ( (!\inst4|Q[3]~_Duplicate_1_q\ & ((!\inst4|Q[2]~_Duplicate_1_q\ & ((\inst|LessThan0~0_combout\) # (\inst2|DATA_OUT[2]~5_combout\))) # 
-- (\inst4|Q[2]~_Duplicate_1_q\ & (\inst2|DATA_OUT[2]~5_combout\ & \inst|LessThan0~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100010001010000010001000101010101110111011111010111011101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[3]~_Duplicate_1_q\,
	datab => \inst4|ALT_INV_Q[2]~_Duplicate_1_q\,
	datac => \inst2|ALT_INV_DATA_OUT[2]~5_combout\,
	datad => \inst|ALT_INV_LessThan0~0_combout\,
	dataf => \inst2|ALT_INV_DATA_OUT[3]~4_combout\,
	combout => \inst|LessThan0~1_combout\);

-- Location: LABCELL_X65_Y14_N21
\inst|LessThan0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|LessThan0~2_combout\ = ( \inst2|DATA_OUT[5]~2_combout\ & ( (!\inst4|Q[5]~_Duplicate_1_q\) # ((!\inst2|DATA_OUT[4]~3_combout\ & (\inst|LessThan0~1_combout\ & !\inst4|Q[4]~_Duplicate_1_q\)) # (\inst2|DATA_OUT[4]~3_combout\ & 
-- ((!\inst4|Q[4]~_Duplicate_1_q\) # (\inst|LessThan0~1_combout\)))) ) ) # ( !\inst2|DATA_OUT[5]~2_combout\ & ( (!\inst4|Q[5]~_Duplicate_1_q\ & ((!\inst2|DATA_OUT[4]~3_combout\ & (\inst|LessThan0~1_combout\ & !\inst4|Q[4]~_Duplicate_1_q\)) # 
-- (\inst2|DATA_OUT[4]~3_combout\ & ((!\inst4|Q[4]~_Duplicate_1_q\) # (\inst|LessThan0~1_combout\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010101000000010001010100000001010111111101010111011111110101011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|ALT_INV_Q[5]~_Duplicate_1_q\,
	datab => \inst2|ALT_INV_DATA_OUT[4]~3_combout\,
	datac => \inst|ALT_INV_LessThan0~1_combout\,
	datad => \inst4|ALT_INV_Q[4]~_Duplicate_1_q\,
	dataf => \inst2|ALT_INV_DATA_OUT[5]~2_combout\,
	combout => \inst|LessThan0~2_combout\);

-- Location: LABCELL_X64_Y14_N42
\inst|Mux7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux7~1_combout\ = ( \OP[0]~input_o\ & ( \inst4|Q[7]~_Duplicate_1_q\ & ( (\inst2|DATA_OUT[7]~0_combout\ & ((!\inst2|DATA_OUT[6]~1_combout\ & (\inst|LessThan0~2_combout\ & !\inst4|Q[6]~_Duplicate_1_q\)) # (\inst2|DATA_OUT[6]~1_combout\ & 
-- ((!\inst4|Q[6]~_Duplicate_1_q\) # (\inst|LessThan0~2_combout\))))) ) ) ) # ( \OP[0]~input_o\ & ( !\inst4|Q[7]~_Duplicate_1_q\ & ( ((!\inst2|DATA_OUT[6]~1_combout\ & (\inst|LessThan0~2_combout\ & !\inst4|Q[6]~_Duplicate_1_q\)) # 
-- (\inst2|DATA_OUT[6]~1_combout\ & ((!\inst4|Q[6]~_Duplicate_1_q\) # (\inst|LessThan0~2_combout\)))) # (\inst2|DATA_OUT[7]~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000011111110011011100000000000000000001001100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_DATA_OUT[6]~1_combout\,
	datab => \inst2|ALT_INV_DATA_OUT[7]~0_combout\,
	datac => \inst|ALT_INV_LessThan0~2_combout\,
	datad => \inst4|ALT_INV_Q[6]~_Duplicate_1_q\,
	datae => \ALT_INV_OP[0]~input_o\,
	dataf => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	combout => \inst|Mux7~1_combout\);

-- Location: LABCELL_X64_Y14_N36
\inst|ALU_RESULT~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|ALU_RESULT~0_combout\ = ( \inst2|DATA_OUT[7]~0_combout\ & ( !\inst4|Q[7]~_Duplicate_1_q\ ) ) # ( !\inst2|DATA_OUT[7]~0_combout\ & ( \inst4|Q[7]~_Duplicate_1_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	dataf => \inst2|ALT_INV_DATA_OUT[7]~0_combout\,
	combout => \inst|ALU_RESULT~0_combout\);

-- Location: LABCELL_X64_Y14_N39
\inst|Mux7~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux7~3_combout\ = (!\inst|ALU_RESULT~1_combout\ & !\inst|ALU_RESULT~0_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000101000001010000010100000101000001010000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|ALT_INV_ALU_RESULT~1_combout\,
	datac => \inst|ALT_INV_ALU_RESULT~0_combout\,
	combout => \inst|Mux7~3_combout\);

-- Location: LABCELL_X63_Y16_N48
\inst|ALU_RESULT~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|ALU_RESULT~6_combout\ = ( !\inst2|DATA_OUT[1]~6_combout\ & ( \inst4|Q[1]~_Duplicate_1_q\ ) ) # ( \inst2|DATA_OUT[1]~6_combout\ & ( !\inst4|Q[1]~_Duplicate_1_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111111111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_DATA_OUT[1]~6_combout\,
	dataf => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	combout => \inst|ALU_RESULT~6_combout\);

-- Location: LABCELL_X65_Y14_N48
\inst|ALU_RESULT~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|ALU_RESULT~7_combout\ = !\inst2|DATA_OUT[0]~7_combout\ $ (!\inst4|Q[0]~_Duplicate_1_q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011110000111100001111000011110000111100001111000011110000111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_DATA_OUT[0]~7_combout\,
	datac => \inst4|ALT_INV_Q[0]~_Duplicate_1_q\,
	combout => \inst|ALU_RESULT~7_combout\);

-- Location: LABCELL_X63_Y15_N30
\inst|Mux7~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux7~2_combout\ = ( !\inst|ALU_RESULT~7_combout\ & ( !\inst|ALU_RESULT~3_combout\ & ( (!\inst|ALU_RESULT~4_combout\ & (!\inst|ALU_RESULT~6_combout\ & (!\OP[0]~input_o\ & !\inst|ALU_RESULT~5_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|ALT_INV_ALU_RESULT~4_combout\,
	datab => \inst|ALT_INV_ALU_RESULT~6_combout\,
	datac => \ALT_INV_OP[0]~input_o\,
	datad => \inst|ALT_INV_ALU_RESULT~5_combout\,
	datae => \inst|ALT_INV_ALU_RESULT~7_combout\,
	dataf => \inst|ALT_INV_ALU_RESULT~3_combout\,
	combout => \inst|Mux7~2_combout\);

-- Location: LABCELL_X63_Y16_N39
\inst|Mux7~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux7~4_combout\ = ( \OP[1]~input_o\ & ( \inst|ALU_RESULT~2_combout\ & ( \inst|Mux7~1_combout\ ) ) ) # ( !\OP[1]~input_o\ & ( \inst|ALU_RESULT~2_combout\ & ( \inst|Mux7~0_combout\ ) ) ) # ( \OP[1]~input_o\ & ( !\inst|ALU_RESULT~2_combout\ & ( 
-- ((\inst|Mux7~3_combout\ & \inst|Mux7~2_combout\)) # (\inst|Mux7~1_combout\) ) ) ) # ( !\OP[1]~input_o\ & ( !\inst|ALU_RESULT~2_combout\ & ( \inst|Mux7~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101001100110011111101010101010101010011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|ALT_INV_Mux7~0_combout\,
	datab => \inst|ALT_INV_Mux7~1_combout\,
	datac => \inst|ALT_INV_Mux7~3_combout\,
	datad => \inst|ALT_INV_Mux7~2_combout\,
	datae => \ALT_INV_OP[1]~input_o\,
	dataf => \inst|ALT_INV_ALU_RESULT~2_combout\,
	combout => \inst|Mux7~4_combout\);

-- Location: LABCELL_X63_Y16_N42
\inst|Mux7~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux7~6_combout\ = ( !\OP[0]~input_o\ & ( (!\OP[2]~input_o\ & ((!\OP[1]~input_o\ & (\inst|Add0~29_sumout\)) # (\OP[1]~input_o\ & (((\inst|Mult0~8_resulta\)))))) # (\OP[2]~input_o\ & ((((\inst|Mux7~4_combout\))))) ) ) # ( \OP[0]~input_o\ & ( 
-- (!\OP[2]~input_o\ & ((!\OP[1]~input_o\ & (\inst|Add0~29_sumout\)) # (\OP[1]~input_o\ & (((!\inst|Div0|auto_generated|divider|divider|op_8~1_sumout\)))))) # (\OP[2]~input_o\ & ((((\inst|Mux7~4_combout\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0100010000001100010001001100000001110111001111110111011111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|ALT_INV_Add0~29_sumout\,
	datab => \ALT_INV_OP[2]~input_o\,
	datac => \inst|Div0|auto_generated|divider|divider|ALT_INV_op_8~1_sumout\,
	datad => \ALT_INV_OP[1]~input_o\,
	datae => \ALT_INV_OP[0]~input_o\,
	dataf => \inst|ALT_INV_Mux7~4_combout\,
	datag => \inst|ALT_INV_Mult0~8_resulta\,
	combout => \inst|Mux7~6_combout\);

-- Location: FF_X63_Y16_N32
\inst3|Q[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|WRC~combout\,
	asdata => \inst|Mux7~6_combout\,
	clrn => \ALT_INV_RST_C~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst3|Q\(0));

-- Location: LABCELL_X65_Y14_N57
\inst2|DATA_OUT[0]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|DATA_OUT[0]~7_combout\ = ( \inst3|Q\(0) & ( (\inst10|SEL~combout\) # (\inst5|Q\(0)) ) ) # ( !\inst3|Q\(0) & ( (\inst5|Q\(0) & !\inst10|SEL~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010001000100010001000100010001110111011101110111011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|ALT_INV_Q\(0),
	datab => \inst10|ALT_INV_SEL~combout\,
	dataf => \inst3|ALT_INV_Q\(0),
	combout => \inst2|DATA_OUT[0]~7_combout\);

-- Location: LABCELL_X65_Y13_N27
\inst|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux1~0_combout\ = ( \inst|Mult0~14\ & ( \inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ( !\OP[0]~input_o\ ) ) ) # ( \inst|Mult0~14\ & ( !\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ( 
-- (!\inst|Div0|auto_generated|divider|divider|sel\(1)) # (!\OP[0]~input_o\) ) ) ) # ( !\inst|Mult0~14\ & ( !\inst|Div0|auto_generated|divider|divider|add_sub_1_result_int[2]~1_sumout\ & ( (!\inst|Div0|auto_generated|divider|divider|sel\(1) & 
-- \OP[0]~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100111111001111110000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(1),
	datac => \ALT_INV_OP[0]~input_o\,
	datae => \inst|ALT_INV_Mult0~14\,
	dataf => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_1_result_int[2]~1_sumout\,
	combout => \inst|Mux1~0_combout\);

-- Location: LABCELL_X64_Y15_N51
\inst|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Add0~5_sumout\ = SUM(( (!\inst10|SEL~combout\ & ((\inst5|Q\(6)))) # (\inst10|SEL~combout\ & (\inst3|Q\(6))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[6]~_Duplicate_1_q\) ) + ( \inst|Add0~10\ ))
-- \inst|Add0~6\ = CARRY(( (!\inst10|SEL~combout\ & ((\inst5|Q\(6)))) # (\inst10|SEL~combout\ & (\inst3|Q\(6))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[6]~_Duplicate_1_q\) ) + ( \inst|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010100101010100000000000000000000001111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst10|ALT_INV_SEL~combout\,
	datac => \inst3|ALT_INV_Q\(6),
	datad => \inst5|ALT_INV_Q\(6),
	dataf => \inst4|ALT_INV_Q[6]~_Duplicate_1_q\,
	cin => \inst|Add0~10\,
	sumout => \inst|Add0~5_sumout\,
	cout => \inst|Add0~6\);

-- Location: LABCELL_X63_Y15_N24
\inst|Mux1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux1~1_combout\ = ( !\OP[2]~input_o\ & ( \OP[1]~input_o\ & ( \inst|Mux1~0_combout\ ) ) ) # ( \OP[2]~input_o\ & ( !\OP[1]~input_o\ & ( !\OP[0]~input_o\ $ (!\inst|ALU_RESULT~1_combout\) ) ) ) # ( !\OP[2]~input_o\ & ( !\OP[1]~input_o\ & ( 
-- \inst|Add0~5_sumout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111011001100110011000001111000011110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst|ALT_INV_ALU_RESULT~1_combout\,
	datac => \inst|ALT_INV_Mux1~0_combout\,
	datad => \inst|ALT_INV_Add0~5_sumout\,
	datae => \ALT_INV_OP[2]~input_o\,
	dataf => \ALT_INV_OP[1]~input_o\,
	combout => \inst|Mux1~1_combout\);

-- Location: FF_X63_Y15_N44
\inst3|Q[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|WRC~combout\,
	asdata => \inst|Mux1~1_combout\,
	clrn => \ALT_INV_RST_C~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst3|Q\(6));

-- Location: LABCELL_X64_Y15_N54
\inst|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Add0~1_sumout\ = SUM(( (!\inst10|SEL~combout\ & ((\inst5|Q\(7)))) # (\inst10|SEL~combout\ & (\inst3|Q\(7))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[7]~_Duplicate_1_q\) ) + ( \inst|Add0~6\ ))
-- \inst|Add0~2\ = CARRY(( (!\inst10|SEL~combout\ & ((\inst5|Q\(7)))) # (\inst10|SEL~combout\ & (\inst3|Q\(7))) ) + ( !\OP[0]~input_o\ $ (!\inst4|Q[7]~_Duplicate_1_q\) ) + ( \inst|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010100101010100000000000000000000001111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst10|ALT_INV_SEL~combout\,
	datac => \inst3|ALT_INV_Q\(7),
	datad => \inst5|ALT_INV_Q\(7),
	dataf => \inst4|ALT_INV_Q[7]~_Duplicate_1_q\,
	cin => \inst|Add0~6\,
	sumout => \inst|Add0~1_sumout\,
	cout => \inst|Add0~2\);

-- Location: LABCELL_X63_Y15_N42
\inst|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux0~0_combout\ = ( \inst4|Q[1]~_Duplicate_1_q\ & ( (\inst|Mult0~15\ & !\OP[0]~input_o\) ) ) # ( !\inst4|Q[1]~_Duplicate_1_q\ & ( (!\OP[0]~input_o\ & (((\inst|Mult0~15\)))) # (\OP[0]~input_o\ & (!\inst|Div0|auto_generated|divider|divider|sel\(1) & 
-- ((!\inst|Div0|auto_generated|divider|divider|add_sub_0_result_int[1]~1_sumout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011101000110000001110100011000000110000001100000011000000110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Div0|auto_generated|divider|divider|ALT_INV_sel\(1),
	datab => \inst|ALT_INV_Mult0~15\,
	datac => \ALT_INV_OP[0]~input_o\,
	datad => \inst|Div0|auto_generated|divider|divider|ALT_INV_add_sub_0_result_int[1]~1_sumout\,
	dataf => \inst4|ALT_INV_Q[1]~_Duplicate_1_q\,
	combout => \inst|Mux0~0_combout\);

-- Location: LABCELL_X63_Y15_N15
\inst|Mux0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux0~1_combout\ = ( \inst|ALU_RESULT~0_combout\ & ( \OP[1]~input_o\ & ( (!\OP[2]~input_o\ & \inst|Mux0~0_combout\) ) ) ) # ( !\inst|ALU_RESULT~0_combout\ & ( \OP[1]~input_o\ & ( (!\OP[2]~input_o\ & \inst|Mux0~0_combout\) ) ) ) # ( 
-- \inst|ALU_RESULT~0_combout\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ & ((\inst|Add0~1_sumout\))) # (\OP[2]~input_o\ & (!\OP[0]~input_o\)) ) ) ) # ( !\inst|ALU_RESULT~0_combout\ & ( !\OP[1]~input_o\ & ( (!\OP[2]~input_o\ & ((\inst|Add0~1_sumout\))) # 
-- (\OP[2]~input_o\ & (\OP[0]~input_o\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011010100110101001110100011101000000000111100000000000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	datab => \inst|ALT_INV_Add0~1_sumout\,
	datac => \ALT_INV_OP[2]~input_o\,
	datad => \inst|ALT_INV_Mux0~0_combout\,
	datae => \inst|ALT_INV_ALU_RESULT~0_combout\,
	dataf => \ALT_INV_OP[1]~input_o\,
	combout => \inst|Mux0~1_combout\);

-- Location: FF_X63_Y15_N20
\inst3|Q[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst10|WRC~combout\,
	asdata => \inst|Mux0~1_combout\,
	clrn => \ALT_INV_RST_C~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst3|Q\(7));

-- Location: LABCELL_X64_Y15_N57
\inst|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Add0~33_sumout\ = SUM(( \OP[0]~input_o\ ) + ( GND ) + ( \inst|Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP[0]~input_o\,
	cin => \inst|Add0~2\,
	sumout => \inst|Add0~33_sumout\);

-- Location: LABCELL_X63_Y15_N45
\inst|Mux7~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|Mux7~5_combout\ = (!\OP[2]~input_o\ & !\OP[1]~input_o\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_OP[2]~input_o\,
	datad => \ALT_INV_OP[1]~input_o\,
	combout => \inst|Mux7~5_combout\);

-- Location: LABCELL_X64_Y15_N12
\inst|temp[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|temp\(8) = ( \inst|Mux7~5_combout\ & ( \inst|Add0~33_sumout\ ) ) # ( !\inst|Mux7~5_combout\ & ( \inst|temp\(8) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|ALT_INV_Add0~33_sumout\,
	datac => \inst|ALT_INV_temp\(8),
	dataf => \inst|ALT_INV_Mux7~5_combout\,
	combout => \inst|temp\(8));

-- Location: LABCELL_X64_Y14_N30
\inst10|STATE_OUT[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|STATE_OUT\(1) = ( \inst10|current_state.procc~q\ & ( \inst10|current_state.accumulate~q\ ) ) # ( !\inst10|current_state.procc~q\ & ( \inst10|current_state.accumulate~q\ ) ) # ( \inst10|current_state.procc~q\ & ( 
-- !\inst10|current_state.accumulate~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst10|ALT_INV_current_state.procc~q\,
	dataf => \inst10|ALT_INV_current_state.accumulate~q\,
	combout => \inst10|STATE_OUT\(1));

-- Location: LABCELL_X64_Y14_N57
\inst10|STATE_OUT[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst10|STATE_OUT\(0) = ( \inst10|current_state.fetch~q\ ) # ( !\inst10|current_state.fetch~q\ & ( \inst10|current_state.procc~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst10|ALT_INV_current_state.procc~q\,
	dataf => \inst10|ALT_INV_current_state.fetch~q\,
	combout => \inst10|STATE_OUT\(0));

-- Location: MLABCELL_X14_Y57_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


