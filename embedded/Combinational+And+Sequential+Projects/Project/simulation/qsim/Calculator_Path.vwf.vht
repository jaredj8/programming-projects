-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "06/17/2021 11:37:18"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          Calculator_Path
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY Calculator_Path_vhd_vec_tst IS
END Calculator_Path_vhd_vec_tst;
ARCHITECTURE Calculator_Path_arch OF Calculator_Path_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL A : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL RA_OUTPUT : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL RST_A : STD_LOGIC;
SIGNAL WR_A : STD_LOGIC;
COMPONENT Calculator_Path
	PORT (
	A : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	RA_OUTPUT : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	RST_A : IN STD_LOGIC;
	WR_A : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : Calculator_Path
	PORT MAP (
-- list connections between master ports and signals
	A => A,
	RA_OUTPUT => RA_OUTPUT,
	RST_A => RST_A,
	WR_A => WR_A
	);
-- A[7]
t_prcs_A_7: PROCESS
BEGIN
	A(7) <= '0';
WAIT;
END PROCESS t_prcs_A_7;
-- A[6]
t_prcs_A_6: PROCESS
BEGIN
	A(6) <= '0';
WAIT;
END PROCESS t_prcs_A_6;
-- A[5]
t_prcs_A_5: PROCESS
BEGIN
	A(5) <= '1';
WAIT;
END PROCESS t_prcs_A_5;
-- A[4]
t_prcs_A_4: PROCESS
BEGIN
	A(4) <= '0';
WAIT;
END PROCESS t_prcs_A_4;
-- A[3]
t_prcs_A_3: PROCESS
BEGIN
	A(3) <= '0';
WAIT;
END PROCESS t_prcs_A_3;
-- A[2]
t_prcs_A_2: PROCESS
BEGIN
	A(2) <= '0';
WAIT;
END PROCESS t_prcs_A_2;
-- A[1]
t_prcs_A_1: PROCESS
BEGIN
	A(1) <= '0';
WAIT;
END PROCESS t_prcs_A_1;
-- A[0]
t_prcs_A_0: PROCESS
BEGIN
	A(0) <= '0';
WAIT;
END PROCESS t_prcs_A_0;

-- RST_A
t_prcs_RST_A: PROCESS
BEGIN
	RST_A <= '0';
	WAIT FOR 200000 ps;
	RST_A <= '1';
	WAIT FOR 80000 ps;
	RST_A <= '0';
WAIT;
END PROCESS t_prcs_RST_A;

-- WR_A
t_prcs_WR_A: PROCESS
BEGIN
	WR_A <= '1';
	WAIT FOR 200000 ps;
	WR_A <= '0';
	WAIT FOR 80000 ps;
	WR_A <= '1';
	WAIT FOR 440000 ps;
	WR_A <= '0';
WAIT;
END PROCESS t_prcs_WR_A;
END Calculator_Path_arch;
