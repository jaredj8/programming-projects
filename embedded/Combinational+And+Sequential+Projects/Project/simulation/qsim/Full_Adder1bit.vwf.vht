-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "06/11/2021 19:19:04"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          Full_Adder1bit
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY Full_Adder1bit_vhd_vec_tst IS
END Full_Adder1bit_vhd_vec_tst;
ARCHITECTURE Full_Adder1bit_arch OF Full_Adder1bit_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL A : STD_LOGIC;
SIGNAL B : STD_LOGIC;
SIGNAL CIN : STD_LOGIC;
SIGNAL CO : STD_LOGIC;
SIGNAL SUM : STD_LOGIC;
COMPONENT Full_Adder1bit
	PORT (
	A : IN STD_LOGIC;
	B : IN STD_LOGIC;
	CIN : IN STD_LOGIC;
	CO : OUT STD_LOGIC;
	SUM : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : Full_Adder1bit
	PORT MAP (
-- list connections between master ports and signals
	A => A,
	B => B,
	CIN => CIN,
	CO => CO,
	SUM => SUM
	);

-- A
t_prcs_A: PROCESS
BEGIN
	A <= '0';
	WAIT FOR 360000 ps;
	A <= '1';
	WAIT FOR 420000 ps;
	A <= '0';
WAIT;
END PROCESS t_prcs_A;

-- B
t_prcs_B: PROCESS
BEGIN
	B <= '0';
	WAIT FOR 170000 ps;
	B <= '1';
	WAIT FOR 190000 ps;
	B <= '0';
	WAIT FOR 200000 ps;
	B <= '1';
	WAIT FOR 220000 ps;
	B <= '0';
WAIT;
END PROCESS t_prcs_B;

-- CIN
t_prcs_CIN: PROCESS
BEGIN
	CIN <= '0';
	WAIT FOR 80000 ps;
	CIN <= '1';
	WAIT FOR 90000 ps;
	CIN <= '0';
	WAIT FOR 100000 ps;
	CIN <= '1';
	WAIT FOR 90000 ps;
	CIN <= '0';
	WAIT FOR 100000 ps;
	CIN <= '1';
	WAIT FOR 100000 ps;
	CIN <= '0';
	WAIT FOR 120000 ps;
	CIN <= '1';
	WAIT FOR 100000 ps;
	CIN <= '0';
WAIT;
END PROCESS t_prcs_CIN;
END Full_Adder1bit_arch;
