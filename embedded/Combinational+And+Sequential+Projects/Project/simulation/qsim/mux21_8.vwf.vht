-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "06/09/2021 19:25:14"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          mux21_8
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY mux21_8_vhd_vec_tst IS
END mux21_8_vhd_vec_tst;
ARCHITECTURE mux21_8_arch OF mux21_8_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL d0 : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL d1 : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL DATA_OUT : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL SEL : STD_LOGIC;
COMPONENT mux21_8
	PORT (
	d0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	d1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	DATA_OUT : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	SEL : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : mux21_8
	PORT MAP (
-- list connections between master ports and signals
	d0 => d0,
	d1 => d1,
	DATA_OUT => DATA_OUT,
	SEL => SEL
	);
-- d0[7]
t_prcs_d0_7: PROCESS
BEGIN
	d0(7) <= '0';
WAIT;
END PROCESS t_prcs_d0_7;
-- d0[6]
t_prcs_d0_6: PROCESS
BEGIN
	d0(6) <= '0';
WAIT;
END PROCESS t_prcs_d0_6;
-- d0[5]
t_prcs_d0_5: PROCESS
BEGIN
	d0(5) <= '0';
WAIT;
END PROCESS t_prcs_d0_5;
-- d0[4]
t_prcs_d0_4: PROCESS
BEGIN
	d0(4) <= '1';
WAIT;
END PROCESS t_prcs_d0_4;
-- d0[3]
t_prcs_d0_3: PROCESS
BEGIN
	d0(3) <= '1';
WAIT;
END PROCESS t_prcs_d0_3;
-- d0[2]
t_prcs_d0_2: PROCESS
BEGIN
	d0(2) <= '0';
WAIT;
END PROCESS t_prcs_d0_2;
-- d0[1]
t_prcs_d0_1: PROCESS
BEGIN
	d0(1) <= '0';
WAIT;
END PROCESS t_prcs_d0_1;
-- d0[0]
t_prcs_d0_0: PROCESS
BEGIN
	d0(0) <= '1';
WAIT;
END PROCESS t_prcs_d0_0;
-- d1[7]
t_prcs_d1_7: PROCESS
BEGIN
	d1(7) <= '0';
WAIT;
END PROCESS t_prcs_d1_7;
-- d1[6]
t_prcs_d1_6: PROCESS
BEGIN
	d1(6) <= '1';
WAIT;
END PROCESS t_prcs_d1_6;
-- d1[5]
t_prcs_d1_5: PROCESS
BEGIN
	d1(5) <= '0';
WAIT;
END PROCESS t_prcs_d1_5;
-- d1[4]
t_prcs_d1_4: PROCESS
BEGIN
	d1(4) <= '1';
WAIT;
END PROCESS t_prcs_d1_4;
-- d1[3]
t_prcs_d1_3: PROCESS
BEGIN
	d1(3) <= '0';
WAIT;
END PROCESS t_prcs_d1_3;
-- d1[2]
t_prcs_d1_2: PROCESS
BEGIN
	d1(2) <= '1';
WAIT;
END PROCESS t_prcs_d1_2;
-- d1[1]
t_prcs_d1_1: PROCESS
BEGIN
	d1(1) <= '1';
WAIT;
END PROCESS t_prcs_d1_1;
-- d1[0]
t_prcs_d1_0: PROCESS
BEGIN
	d1(0) <= '1';
WAIT;
END PROCESS t_prcs_d1_0;

-- SEL
t_prcs_SEL: PROCESS
BEGIN
	SEL <= '1';
	WAIT FOR 440000 ps;
	SEL <= '0';
WAIT;
END PROCESS t_prcs_SEL;
END mux21_8_arch;
