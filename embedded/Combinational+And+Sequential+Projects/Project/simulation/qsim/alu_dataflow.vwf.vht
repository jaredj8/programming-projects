-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "06/17/2021 17:48:39"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          alu_dataflow
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY alu_dataflow_vhd_vec_tst IS
END alu_dataflow_vhd_vec_tst;
ARCHITECTURE alu_dataflow_arch OF alu_dataflow_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL A : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL acc : STD_LOGIC;
SIGNAL ALU_OUT : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL B : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL carryout : STD_LOGIC;
SIGNAL clock : STD_LOGIC;
SIGNAL OP : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL RA_OUTPUT : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL RB_OUTPUT : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL RC_OUTPUT : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL reset : STD_LOGIC;
SIGNAL RST_A : STD_LOGIC;
SIGNAL RST_B : STD_LOGIC;
SIGNAL RST_C : STD_LOGIC;
SIGNAL start : STD_LOGIC;
SIGNAL STATE_OUT : STD_LOGIC_VECTOR(2 DOWNTO 0);
COMPONENT alu_dataflow
	PORT (
	A : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	acc : IN STD_LOGIC;
	ALU_OUT : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	B : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	carryout : OUT STD_LOGIC;
	clock : IN STD_LOGIC;
	OP : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
	RA_OUTPUT : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	RB_OUTPUT : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	RC_OUTPUT : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	reset : IN STD_LOGIC;
	RST_A : IN STD_LOGIC;
	RST_B : IN STD_LOGIC;
	RST_C : IN STD_LOGIC;
	start : IN STD_LOGIC;
	STATE_OUT : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
	);
END COMPONENT;
BEGIN
	i1 : alu_dataflow
	PORT MAP (
-- list connections between master ports and signals
	A => A,
	acc => acc,
	ALU_OUT => ALU_OUT,
	B => B,
	carryout => carryout,
	clock => clock,
	OP => OP,
	RA_OUTPUT => RA_OUTPUT,
	RB_OUTPUT => RB_OUTPUT,
	RC_OUTPUT => RC_OUTPUT,
	reset => reset,
	RST_A => RST_A,
	RST_B => RST_B,
	RST_C => RST_C,
	start => start,
	STATE_OUT => STATE_OUT
	);

-- clock
t_prcs_clock: PROCESS
BEGIN
LOOP
	clock <= '0';
	WAIT FOR 100000 ps;
	clock <= '1';
	WAIT FOR 100000 ps;
	IF (NOW >= 2000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_clock;
-- OP[2]
t_prcs_OP_2: PROCESS
BEGIN
	OP(2) <= '0';
WAIT;
END PROCESS t_prcs_OP_2;
-- OP[1]
t_prcs_OP_1: PROCESS
BEGIN
	OP(1) <= '0';
WAIT;
END PROCESS t_prcs_OP_1;
-- OP[0]
t_prcs_OP_0: PROCESS
BEGIN
	OP(0) <= '0';
WAIT;
END PROCESS t_prcs_OP_0;

-- reset
t_prcs_reset: PROCESS
BEGIN
	reset <= '1';
	WAIT FOR 120000 ps;
	reset <= '0';
WAIT;
END PROCESS t_prcs_reset;

-- start
t_prcs_start: PROCESS
BEGIN
	start <= '0';
	WAIT FOR 260000 ps;
	start <= '1';
	WAIT FOR 160000 ps;
	start <= '0';
	WAIT FOR 700000 ps;
	start <= '1';
	WAIT FOR 140000 ps;
	start <= '0';
WAIT;
END PROCESS t_prcs_start;

-- acc
t_prcs_acc: PROCESS
BEGIN
	acc <= '0';
	WAIT FOR 1120000 ps;
	acc <= '1';
	WAIT FOR 140000 ps;
	acc <= '0';
WAIT;
END PROCESS t_prcs_acc;
-- A[7]
t_prcs_A_7: PROCESS
BEGIN
	A(7) <= '0';
WAIT;
END PROCESS t_prcs_A_7;
-- A[6]
t_prcs_A_6: PROCESS
BEGIN
	A(6) <= '0';
WAIT;
END PROCESS t_prcs_A_6;
-- A[5]
t_prcs_A_5: PROCESS
BEGIN
	A(5) <= '1';
WAIT;
END PROCESS t_prcs_A_5;
-- A[4]
t_prcs_A_4: PROCESS
BEGIN
	A(4) <= '0';
WAIT;
END PROCESS t_prcs_A_4;
-- A[3]
t_prcs_A_3: PROCESS
BEGIN
	A(3) <= '1';
WAIT;
END PROCESS t_prcs_A_3;
-- A[2]
t_prcs_A_2: PROCESS
BEGIN
	A(2) <= '1';
WAIT;
END PROCESS t_prcs_A_2;
-- A[1]
t_prcs_A_1: PROCESS
BEGIN
	A(1) <= '1';
WAIT;
END PROCESS t_prcs_A_1;
-- A[0]
t_prcs_A_0: PROCESS
BEGIN
	A(0) <= '1';
WAIT;
END PROCESS t_prcs_A_0;
-- B[7]
t_prcs_B_7: PROCESS
BEGIN
	B(7) <= '0';
WAIT;
END PROCESS t_prcs_B_7;
-- B[6]
t_prcs_B_6: PROCESS
BEGIN
	B(6) <= '1';
WAIT;
END PROCESS t_prcs_B_6;
-- B[5]
t_prcs_B_5: PROCESS
BEGIN
	B(5) <= '0';
WAIT;
END PROCESS t_prcs_B_5;
-- B[4]
t_prcs_B_4: PROCESS
BEGIN
	B(4) <= '1';
WAIT;
END PROCESS t_prcs_B_4;
-- B[3]
t_prcs_B_3: PROCESS
BEGIN
	B(3) <= '1';
WAIT;
END PROCESS t_prcs_B_3;
-- B[2]
t_prcs_B_2: PROCESS
BEGIN
	B(2) <= '0';
WAIT;
END PROCESS t_prcs_B_2;
-- B[1]
t_prcs_B_1: PROCESS
BEGIN
	B(1) <= '1';
WAIT;
END PROCESS t_prcs_B_1;
-- B[0]
t_prcs_B_0: PROCESS
BEGIN
	B(0) <= '1';
WAIT;
END PROCESS t_prcs_B_0;

-- RST_B
t_prcs_RST_B: PROCESS
BEGIN
	RST_B <= '0';
WAIT;
END PROCESS t_prcs_RST_B;

-- RST_C
t_prcs_RST_C: PROCESS
BEGIN
	RST_C <= '0';
WAIT;
END PROCESS t_prcs_RST_C;

-- RST_A
t_prcs_RST_A: PROCESS
BEGIN
	RST_A <= '0';
WAIT;
END PROCESS t_prcs_RST_A;
END alu_dataflow_arch;
