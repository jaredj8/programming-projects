-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "06/11/2021 18:27:26"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          MUX_GATES21
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY MUX_GATES21_vhd_vec_tst IS
END MUX_GATES21_vhd_vec_tst;
ARCHITECTURE MUX_GATES21_arch OF MUX_GATES21_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL Input0 : STD_LOGIC;
SIGNAL Input1 : STD_LOGIC;
SIGNAL MUX_OUT : STD_LOGIC;
SIGNAL Sel : STD_LOGIC;
COMPONENT MUX_GATES21
	PORT (
	Input0 : IN STD_LOGIC;
	Input1 : IN STD_LOGIC;
	MUX_OUT : OUT STD_LOGIC;
	Sel : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : MUX_GATES21
	PORT MAP (
-- list connections between master ports and signals
	Input0 => Input0,
	Input1 => Input1,
	MUX_OUT => MUX_OUT,
	Sel => Sel
	);

-- Input0
t_prcs_Input0: PROCESS
BEGIN
	Input0 <= '0';
	WAIT FOR 110000 ps;
	Input0 <= '1';
	WAIT FOR 130000 ps;
	Input0 <= '0';
WAIT;
END PROCESS t_prcs_Input0;

-- Input1
t_prcs_Input1: PROCESS
BEGIN
	Input1 <= '1';
	WAIT FOR 110000 ps;
	Input1 <= '0';
WAIT;
END PROCESS t_prcs_Input1;

-- Sel
t_prcs_Sel: PROCESS
BEGIN
	Sel <= '1';
	WAIT FOR 110000 ps;
	Sel <= '0';
WAIT;
END PROCESS t_prcs_Sel;
END MUX_GATES21_arch;
