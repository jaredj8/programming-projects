import sys
param = sys.argv[1]

splitFileList = []

question = ""
questionFlag = 0
answerFlag = 0
testDictionary = {}
answerDictionary = {}
answerList = []
testDictionaryTempValue = 0
answerDictionaryTempValue = 0
answer=""
elementCount=0
outputFile=""

#create output file
for i in param.split('.'):
	if i[0:5] == "input":
		outputFile+=i+"Output"
	else:
		outputFile+="."+i

outputFile1 = outputFile.replace("in","out",1)
print(outputFile1)

#unnecessary data in the file is deleted
matches = ["You left the correct answer unselected.",
			"You chose wrong answer.",
			"You have selected the correct answer.",
			"Your answer",
			"to this question is incorrect or incomplete.",
			"Correct answer",
			"You correctly answered this",
			"\t"]

with open(param, 'r') as file:
	data = file.read()
	for i in matches:
		data = data.replace(i, "")
	data = data.replace("<span", " <span")
	data = data.replace("fa", "")

with open(param, 'w') as file:
	file.write(data)


with open(param,"r+") as file:
	splitFileList = file.readlines()

#loop through each index of the list
for j in range(len(splitFileList)):
	#loop through each index of the splitFileList which has all of the words split
	for i in splitFileList[j].split(" "):
		if i.strip() == 'class="panel-heading">' and elementCount == 0:
			elementCount+=1
			continue
		#find all words and the punctuation to form the question /// add to the question string /// if punctuation is found exit the if statement
		if i.isalpha() and  questionFlag == 0 or i[len(i)-8:len(i)-1] == ":</div>" or i[len(i)-8:len(i)-1] == ".</div>" or i[len(i)-8:len(i)-1] == "?</div>" or i[len(i)-8:len(i)-1] == ")</div>":
			if questionFlag != 1:
				if i[len(i)-8:len(i)-1] == ":</div>" or i[len(i)-8:len(i)-1] == ".</div>" or i[len(i)-8:len(i)-1] == "?</div>" or i[len(i)-8:len(i)-1] == ")</div>":
					questionFlag = 1
					i = i[0:len(i)-7]
					question+=i + " "
					testDictionary[question] = testDictionaryTempValue
					testDictionaryTempValue+=1
					question = ""
				else:	
					question+=i + " "
		elif questionFlag == 1:
			if i.strip().isalpha() or i[len(i)-6:len(i)-1] == "</li>":
				if i.strip().isalpha():
					answer+=i + " "
				elif i[len(i)-6:len(i)-1] == "</li>":
					formatAnswer = i[0:len(i)-6]
					answer+=formatAnswer
					answerList.append(answer)
					answer=""
				
		#this element indicates a new set will begin /// reset variables for new question and answer
		if i.strip() == 'class="panel-heading">':
			questionFlag = 0
			answerDictionary[answerDictionaryTempValue] = answerList
			answerList=[]
			answerDictionaryTempValue+=1


# find the value that matches the key in both dictionaries /// place the answers into the test dictionary
for k,v in testDictionary.items():
	for key,value in answerDictionary.items():
		# once found break the for loop
		if v == key:
			# get the value of answerDictionary and put it into testDictionary
			testDictionary[k] = value

i=0
with open(outputFile1, 'w') as f:
	sys.stdout = f 
	for key,value in testDictionary.items():
		print(str(key) + "\n")
		getLength = len(value)
		while i < getLength:
			print(str(i+1) + ". " + str(value[i]) + "\n")
			i+=1

		getLength=0
		i=0