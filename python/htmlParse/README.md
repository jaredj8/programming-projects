# HTML Security+ Test Parser
- ### Project Significance
    - needed to study for the Security+ Exam and came across a test website
    - website did not save taken tests
    - used python to create an I/O script that takes in the html from the website
    - stored the test in a txt file for later use

- ### Project Timeline
    - 2 days

- ### Project Details
    - focus was on I/O control in python
    - import data from file export to another file
    - program takes first command line param as the input file
    - clean html data first
    - run loop on input file and test for certain html elements
    - Use of dictionaries and lists

- ### Screenshots of HTML before and after clean up
| Before | After |
| -------- | --------- |
| ![before](img/beforeCleanUp.png) | ![before](img/afterCleanUp.png) |

- ### Test output file
![test](img/exam.png)

- ### Script Link
[Python Script](securityPlusParser.py)
