#!/usr/bin/python

import sys
i=0
k=0
j=0
t=0

my_dict = {} ## key = count of orig position, value = word at orig position
vowel_list = [] ## contains all vowels sorted
wordVowel_dict = {} ## key = word at orig position, value = vowel matched to word
final_list = []
word_list = []
moreOcc = {}
moreOccList = []
resLength = 0
res = []
locations = []
locationsMin = []
final_dict = {}
returnValue = 0;
BIGlist = []


cmdArgFile = sys.argv[1]

file = open(cmdArgFile)
lines = file.readlines()
length = len(lines)

while i < length:
	my_dict[i] = str.rstrip(lines[i])
	i+=1

file.close()

i=0

##take the values from my_dict and find all vowels##
for value in my_dict.itervalues():
	vowelString = ""
	for letter in value:
		if letter == "a" or letter == "e" or letter == "i" or letter == "o" or letter == "u":
			vowelString += letter
	vowel_list.append(vowelString)

for value in my_dict.itervalues():
	wordVowel_dict[value] = vowel_list[k]
	k+=1

vowel_list.sort()

i=0
j=0
while i < length:  ##loop through sorted vowel array (vowel_list)
	returnValue = 0
	for key, value in wordVowel_dict.items(): ##loop through wordVowel dict (wordVowel_dict)
		## if vowel_list[i] equals the value of wordVowel_dict
		if vowel_list[i] == value:
			while j < length:
				if vowel_list[j] == value:
					returnValue+=1
				j+=1

			##find the matches location
			if returnValue > 1:
				moreOccList.append(key)
				locations.append(i)
				moreOccList.sort()
				for word in moreOccList:
					if word not in res:
						res.append(word)		
				for num in locations:
					if num not in locationsMin:
						locationsMin.append(num)

			word_list.append(key)

	i+=1
	j=0

i=0

resLength = len(res)
for word in word_list:
	if word not in final_list:
		final_list.append(word)

i=0
previous=0

while i < resLength:
	##make a hash with the incoming i
	##if i == previous+1
	if previous == 0:
		previous = locationsMin[i] ###previous = 5 at 0 temp = 0 at 0
		temp = i #starts at 5
	elif locationsMin[i] == previous+1 and i == 1:
		##add to hash
		BIGlist.append(res[0])
		BIGlist.append(res[i])
		previous = locationsMin[i]
	elif locationsMin[i] == previous+1:
		BIGlist.append(res[i])
		previous = locationsMin[i]
		if i == resLength - 1:
			BIGlist.sort()
			ListLength = len(BIGlist)
			while j < resLength:
				final_dict[locationsMin[j]] = BIGlist[t]
				j+=1
				t+=1

		##append location of word as key and word as value

		#print i
	else:
		BIGlist.sort()
		ListLength = len(BIGlist)
		##at this point i need to put data in hash
		while j < ListLength:
			final_dict[locationsMin[j]] = BIGlist[j]
			j+=1
		previous = locationsMin[i]
		BIGlist = []
		BIGlist.append(res[i])

	
	i+=1

i=0
while i < length:
	for key, value in final_dict.items():
		if i == key:
			final_list[i] = value
			
	i+=1

i=0
# print final_list[7]
while i < length:
	print final_list[i]
	i+=1