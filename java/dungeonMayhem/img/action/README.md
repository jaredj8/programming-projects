### Actions Method
- #### Functionality
    - checks how many actions are left
    - if the card has more actions, add to actions
    - if actions gets to 0, find the next players turn
- #### Screenshot
![actions](./actions_method.png)

