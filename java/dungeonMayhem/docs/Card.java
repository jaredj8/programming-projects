package com.example.dungeon_mayhem;

public class Card {
    Card(){
        drawCardIcons = 0;
        healCardIcons = 0;//
        playAgainIcons = 0;//
        attackIcons = 0;//
        shieldIcons = 0;//
        cardStats = 0;
        max = 4;
        min = 0;
    }

    void setCardStats(int stats){cardStats = stats;}
    void setDrawCard(){drawCardIcons++;}

    int getDrawCard(){
        return drawCardIcons;
    }

    void setHealCard(){
        healCardIcons++;
    }

    int getHealCard(){
        return healCardIcons;
    }

    void setPlayCard(){
        playAgainIcons++;
    }

    int getPlayAgainIcons(){
        return playAgainIcons;
    }

    void setAttackIcons(){
        attackIcons++;
    }

    int getAttackIcons(){
        return attackIcons;
    }

    void setShieldIcons(){
        shieldIcons++;
    }

    int getShieldIcons(){
        return shieldIcons;
    }


    int drawCardIcons;
    int healCardIcons;
    int playAgainIcons;
    int attackIcons;
    int shieldIcons;
    int cardStats;
    int max;
    int min;

}
