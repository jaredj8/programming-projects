package com.example;

public class Analyst extends Employee{
    private int salary;

    Analyst(String name, int salary) {
        super(name);
        this.salary = salary;
    }
    
}
