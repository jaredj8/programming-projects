# Number Array Program
- ### Program Overview
    - Working with java arrays
    - Adding numbers to array with user entry
    - Looping through array to find matches 

- ### Program screenshots 
| Methods.java |
|     :--------:     |
| ![methods](../img/numberArray.png) |
    
    | Terminal Run |
    |     :--------:     |
    | ![terminal](../img/numArrayRun.png) |
