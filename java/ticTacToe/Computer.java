public class Computer {

    boolean isCenterAvailable(char[][] arr){
        return arr[1][1] == ' ';
    }

    //checks every winning move, cpu use to find winning move
    //returns that winning move
    String winningMove(char[][] arr, char symbol){
        //cycle through array
        if(arr[0][0] == ' ' && arr[0][1] == symbol && arr[0][2] == symbol){
            return "00";
        }if(arr[0][0] == ' ' && arr[1][0] == symbol && arr[2][0] == symbol){
            return "00";
        }if(arr[0][0] == ' ' && arr[1][1] == symbol && arr[2][2] == symbol){
            return "00";
        }if(arr[0][1] == ' ' && arr[0][0] == symbol && arr[0][2] == symbol){
            return "01";
        }if(arr[0][1] == ' ' && arr[1][1] == symbol && arr[2][1] == symbol){
            return "01";
        }if(arr[0][2] == ' ' && arr[0][0] == symbol && arr[0][1] == symbol){
            return "02";
        }if(arr[0][2] == ' ' && arr[1][2] == symbol && arr[2][2] == symbol){
            return "02";
        }if(arr[0][2] == ' ' && arr[1][1] == symbol && arr[2][0] == symbol){
            return "02";
        }if(arr[1][0] == ' ' && arr[0][0] == symbol && arr[2][0] == symbol){
            return "10";
        }if(arr[1][0] == ' ' && arr[1][1] == symbol && arr[1][2] == symbol){
            return "10";
        }if(arr[1][1] == ' ' && arr[0][0] == symbol && arr[2][2] == symbol){
            return "11";
        }if(arr[1][1] == ' ' && arr[0][1] == symbol && arr[2][1] == symbol){
            return "11";
        }if(arr[1][1] == ' ' && arr[0][2] == symbol && arr[2][0] == symbol){
            return "11";
        }if(arr[1][1] == ' ' && arr[1][0] == symbol && arr[1][2] == symbol){
            return "11";
        }if(arr[1][2] == ' ' && arr[0][2] == symbol && arr[2][2] == symbol){
            return "12";
        }if(arr[1][2] == ' ' && arr[1][1] == symbol && arr[1][0] == symbol){
            return "12";
        }if(arr[2][0] == ' ' && arr[1][0] == symbol && arr[0][0] == symbol){
            return "20";
        }if(arr[2][0] == ' ' && arr[2][1] == symbol && arr[2][2] == symbol){
            return "20";
        }if(arr[2][0] == ' ' && arr[1][1] == symbol && arr[0][2] == symbol){
            return "20";
        }
        if(arr[2][1] == ' ' && arr[1][1] == symbol && arr[0][1] == symbol){
            return "21";
        }
        if(arr[2][1] == ' ' && arr[2][0] == symbol && arr[2][2] == symbol){
            return "21";
        }if(arr[2][2] == ' ' && arr[1][2] == symbol && arr[0][2] == symbol){
            return "22";
        }if(arr[2][2] == ' ' && arr[2][1] == symbol && arr[2][0] == symbol){
            return "22";
        }if(arr[2][2] == ' ' && arr[1][1] == symbol && arr[0][0] == symbol){
            return "22";
        }

        return "not found";
    }

    //loops through gameboard and finds all empty spaces
    //if an empty space is found, place in separate array
    //random select an index from the separate array and return it
    String getRandomLocation(char[][] arr){
        String[] occ = new String[9];
        for (int i = 0; i < occ.length; i++){
            occ[i] = "";
        }
        int randCount = 0;
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                if(arr[i][j] == ' '){
                    occ[randCount] = "" + i + j;
                    randCount++;
                }
            }
        }
        int randNum = (int) ((Math.random() * (randCount - 0)) + 0);
        return occ[randNum];

    }

    //used for print to find what position the cpu used
    String getPosition(String position){
        switch (position) {
            case "00": return "1";
            case "01": return "2";
            case "02": return "3";
            case "10": return "4";
            case "11": return "5";
            case "12": return "6";
            case "20": return "7";
            case "21": return "8";
            case "22": return "9";
            default: return "Error";
        }
    }

}
