import java.util.Objects;

public class TicTacToe {
    public static void main(String[] args){
        //could be two args else take in one
        //player vs player
        if(args.length == 0){
            GameControl game = new GameControl(2, 0);
            game.draw();
            game.start();
        }else if(args.length == 1 && Objects.equals(args[0], "-c")){
            //two computers
            GameControl game = new GameControl(0, 2);
            game.draw();
            game.start();

        }else if(Objects.equals(args[0], "-c") && args.length > 1){
            if(Objects.equals(args[1], "1")){
                //computer is player 1
                GameControl game = new GameControl(2, 1);
                game.draw();
                game.start();
            }else if(Objects.equals(args[1], "2")){
                //computer is player 2
                GameControl game = new GameControl(1, 2);
                game.draw();
                game.start();
            }

        }
    }
}
