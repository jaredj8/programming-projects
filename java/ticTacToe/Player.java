import java.util.Objects;
import java.util.Scanner;

public class Player {
    Player(){
        playerSelection = 0;
        sc = new Scanner(System.in);
    }
    //prompts player for their choice
    //return choice as the position on the board
    String prompt(int p){
        System.out.print("Player " + p + ", please enter a move (1-9): ");
        playerSelection = sc.nextInt();

        switch (playerSelection) {
            case 1: return "00";
            case 2: return "01";
            case 3: return "02";
            case 4: return "10";
            case 5: return "11";
            case 6: return "12";
            case 7: return "20";
            case 8: return "21";
            case 9: return "22";
            default: return "Error";
        }

    }

    //takes in the user selected position and returns if empty or not
    boolean isEmptyLocation(char[][] arr, String l){
        int firstIndex = Integer.parseInt(l.substring(0,1));
        int secondIndex = Integer.parseInt(l.substring(1));

        return arr[firstIndex][secondIndex] == ' ';
    }

    //checks every winning move, cpu use to find winning move
    //returns that winning move
    String winningMove(char[][] arr, char symbol){
        //cycle through array
        if(arr[0][0] == ' ' && arr[0][1] == symbol && arr[0][2] == symbol){
            return "00";
        }if(arr[0][0] == ' ' && arr[1][0] == symbol && arr[2][0] == symbol){
            return "00";
        }if(arr[0][0] == ' ' && arr[1][1] == symbol && arr[2][2] == symbol){
            return "00";
        }if(arr[0][1] == ' ' && arr[0][0] == symbol && arr[0][2] == symbol){
            return "01";
        }if(arr[0][1] == ' ' && arr[1][1] == symbol && arr[2][1] == symbol){
            return "01";
        }if(arr[0][2] == ' ' && arr[0][0] == symbol && arr[0][1] == symbol){
            return "02";
        }if(arr[0][2] == ' ' && arr[1][2] == symbol && arr[2][2] == symbol){
            return "02";
        }if(arr[0][2] == ' ' && arr[1][1] == symbol && arr[2][0] == symbol){
            return "02";
        }if(arr[1][0] == ' ' && arr[0][0] == symbol && arr[2][0] == symbol){
            return "10";
        }if(arr[1][0] == ' ' && arr[1][1] == symbol && arr[1][2] == symbol){
            return "10";
        }if(arr[1][1] == ' ' && arr[0][0] == symbol && arr[2][2] == symbol){
            return "11";
        }if(arr[1][1] == ' ' && arr[0][1] == symbol && arr[2][1] == symbol){
            return "11";
        }if(arr[1][1] == ' ' && arr[0][2] == symbol && arr[2][0] == symbol){
            return "11";
        }if(arr[1][1] == ' ' && arr[1][0] == symbol && arr[1][2] == symbol){
            return "11";
        }if(arr[1][2] == ' ' && arr[0][2] == symbol && arr[2][2] == symbol){
            return "12";
        }if(arr[1][2] == ' ' && arr[1][1] == symbol && arr[1][0] == symbol){
            return "12";
        }if(arr[2][0] == ' ' && arr[1][0] == symbol && arr[0][0] == symbol){
            return "20";
        }if(arr[2][0] == ' ' && arr[2][1] == symbol && arr[2][2] == symbol){
            return "20";
        }if(arr[2][0] == ' ' && arr[1][1] == symbol && arr[0][2] == symbol){
            return "20";
        }
        if(arr[2][1] == ' ' && arr[1][1] == symbol && arr[0][1] == symbol){
            return "21";
        }
        if(arr[2][1] == ' ' && arr[2][0] == symbol && arr[2][2] == symbol){
            return "21";
        }if(arr[2][2] == ' ' && arr[1][2] == symbol && arr[0][2] == symbol){
            return "22";
        }if(arr[2][2] == ' ' && arr[2][1] == symbol && arr[2][0] == symbol){
            return "22";
        }if(arr[2][2] == ' ' && arr[1][1] == symbol && arr[0][0] == symbol){
            return "22";
        }

        return "not found";
    }

    //finds every winning move
    //if symbols are find in a row return true
    boolean pvpWinningMove(char[][] arr, char symbol, String location){
        if(arr[0][0] == symbol && arr[0][1] == symbol && arr[0][2] == symbol){
            return true;
        }if(arr[1][0] == symbol && arr[1][1] == symbol && arr[1][2] == symbol){
            return true;
        }if(arr[2][0] == symbol && arr[2][1] == symbol && arr[2][2] == symbol){
            return true;
        }if(arr[0][0] == symbol && arr[1][0] == symbol && arr[2][0] == symbol){
            return true;
        }if(arr[0][1] == symbol && arr[1][1] == symbol && arr[2][1] == symbol){
            return true;
        }if(arr[0][2] == symbol && arr[1][2] == symbol && arr[2][2] == symbol){
            return true;
        }if(arr[2][0] == symbol && arr[1][1] == symbol && arr[0][2] == symbol){
            return true;
        }if(arr[0][0] == symbol && arr[1][1] == symbol && arr[2][2] == symbol){
            return true;
        }
        return false;
    }

    int playerSelection;
    Scanner sc;
}
