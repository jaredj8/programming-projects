import java.util.Scanner;

public class GameControl {
    GameControl(int p1, int c1){
        //init member data
        players = p1;
        computers = c1;
        playerTurn = 1;
        gameIsOver = false;
        player1Char = 'X';
        player2Char = 'O';
        countPlayerTurns = 0;
        cpu1 = new Computer();
        cpu2 = new Computer();
        player1 = new Player();
        player2 = new Player();
        gameBoard = new char[3][3];

        //init tic tac toe board
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                gameBoard[i][j] = ' ';
            }
        }
    }

    //start game
    void start() {
        if (players == 0) {
            do {
                if (playerTurn == 1) {
                    //find winning move by looping through array, returns winning move
                    String wM = cpu1.winningMove(gameBoard, player1Char);
                    //find opponent winning move by looping through array, returns opponent winning move
                    String oWM = cpu2.winningMove(gameBoard, player2Char);

                    //wM holds either winning move string or "not found"
                    //if wM has the length of 2 split winning move and make them ints
                    //place the ints in the proper position on the board
                    if (wM.length() == 2) {
                        int firstIndex = Integer.parseInt(wM.substring(0, 1));
                        int secondIndex = Integer.parseInt(wM.substring(1));
                        gameBoard[firstIndex][secondIndex] = player1Char;
                        gameIsOver = true;
                        draw();
                        System.out.println("Player 1 chooses position " + cpu1.getPosition(wM) + "!");
                        System.out.println("Player " + playerTurn + " wins!");
                    }

                    //same as wM but returns the opponents winning move
                    //use oWM to block the opponent
                    else if (oWM.length() == 2) {
                        int firstIndex = Integer.parseInt(oWM.substring(0, 1));
                        int secondIndex = Integer.parseInt(oWM.substring(1));
                        gameBoard[firstIndex][secondIndex] = player1Char;
                        draw();
                        System.out.println("Player 1 chooses position " + cpu1.getPosition(oWM) + "!");
                    }

                    //returns a bool to see if center is empty, if so place playerChar in that position
                    else if (cpu1.isCenterAvailable(gameBoard)) {
                        gameBoard[1][1] = player1Char;
                        draw();
                        System.out.println("Player 1 chooses position 5!");
                    }

                    //getRandomLocation loops through the array finding empty positions
                    //if empty position is found, place in separate array
                    //randomly select from the separate array
                    //position is returned as a string
                    //use that position to place on the gameboard
                    else {
                        String randLocation = cpu1.getRandomLocation(gameBoard);
                        int firstIndex = Integer.parseInt(randLocation.substring(0, 1));
                        int secondIndex = Integer.parseInt(randLocation.substring(1));
                        gameBoard[firstIndex][secondIndex] = player1Char;
                        draw();
                        System.out.println("Player 1 chooses position " + cpu1.getPosition(randLocation) + "!");
                    }
                    System.out.println();
                    countPlayerTurns++;
                    playerTurn = 2;
                } else if (playerTurn == 2) {
                    //find winning move by looping through array, returns winning move
                    String wM = cpu2.winningMove(gameBoard, player2Char);
                    //find opponent winning move by looping through array, returns opponent winning move
                    String oWM = cpu1.winningMove(gameBoard, player1Char);

                    //wM holds either winning move string or "not found"
                    //if wM has the length of 2 split winning move and make them ints
                    //place the ints in the proper position on the board
                    if (wM.length() == 2) {
                        int firstIndex = Integer.parseInt(wM.substring(0, 1));
                        int secondIndex = Integer.parseInt(wM.substring(1));
                        gameBoard[firstIndex][secondIndex] = player2Char;
                        gameIsOver = true;
                        draw();
                        System.out.println("Player 2 chooses position " + cpu1.getPosition(wM) + "!");
                        System.out.println("Player " + playerTurn + " wins!");
                    }

                    //same as wM but returns the opponents winning move
                    //use oWM to block the opponent
                    else if (oWM.length() == 2) {
                        int firstIndex = Integer.parseInt(oWM.substring(0, 1));
                        int secondIndex = Integer.parseInt(oWM.substring(1));
                        gameBoard[firstIndex][secondIndex] = player2Char;
                        draw();
                        System.out.println("Player 2 chooses position " + cpu1.getPosition(oWM) + "!");
                    }

                    //returns a bool to see if center is empty, if so place playerChar in that position
                    else if (cpu2.isCenterAvailable(gameBoard)) {
                        gameBoard[1][1] = player2Char;
                        draw();
                        System.out.println("Player 2 chooses position 5!");
                    }

                    //getRandomLocation loops through the array finding empty positions
                    //if empty position is found, place in separate array
                    //randomly select from the separate array
                    //position is returned as a string
                    //use that position to place on the gameboard
                    else {
                        String randLocation = cpu2.getRandomLocation(gameBoard);
                        int firstIndex = Integer.parseInt(randLocation.substring(0, 1));
                        int secondIndex = Integer.parseInt(randLocation.substring(1));
                        gameBoard[firstIndex][secondIndex] = player2Char;
                        draw();
                        System.out.println("Player 2 chooses position " + cpu1.getPosition(randLocation) + "!");
                    }

                    System.out.println();
                    playerTurn = 1;
                    countPlayerTurns++;
                }
                if (countPlayerTurns == 9) {
                    gameIsOver = true;
                    System.out.println("Draw");
                }
            } while (!gameIsOver);
        } else if (computers == 0) {
            do {
                //player is prompted for their selection
                //the location is returned as a string
                //if that location is not empty continue to prompt player for a new position
                //split location into ints and place in position
                //pvpWinningMove returns a bool, if true the player wins
                if (playerTurn == 1) {
                    String location = player1.prompt(1);
                    while(!player1.isEmptyLocation(gameBoard, location)){
                        System.out.println("Not an empty position, please try again.");
                        location = player1.prompt(1);
                    }
                    int firstIndex = Integer.parseInt(location.substring(0, 1));
                    int secondIndex = Integer.parseInt(location.substring(1));
                    gameBoard[firstIndex][secondIndex] = player1Char;
                    if(player1.pvpWinningMove(gameBoard, player1Char, location)){
                        gameIsOver = true;
                        draw();
                        System.out.println("Player 1 wins!");
                    }else{
                        gameBoard[firstIndex][secondIndex] = player1Char;
                        draw();
                    }


                    playerTurn = 2;
                    countPlayerTurns++;
                    System.out.println();
                }

                //player is prompted for their selection
                //the location is returned as a string
                //if that location is not empty continue to prompt player for a new position
                //split location into ints and place in position
                //pvpWinningMove returns a bool, if true the player wins
                else if (playerTurn == 2) {
                    String location = player2.prompt(2);
                    while(!player2.isEmptyLocation(gameBoard, location)){
                        System.out.println("Not an empty position, please try again.");
                        location = player2.prompt(2);
                    }
                    int firstIndex = Integer.parseInt(location.substring(0, 1));
                    int secondIndex = Integer.parseInt(location.substring(1));
                    gameBoard[firstIndex][secondIndex] = player2Char;
                    if(player2.pvpWinningMove(gameBoard, player2Char, location)){
                        gameIsOver = true;
                        draw();
                        System.out.println("Player 2 wins!");
                    }else{
                        gameBoard[firstIndex][secondIndex] = player2Char;
                        draw();
                    }


                    playerTurn = 1;
                    countPlayerTurns++;
                    System.out.println();
                }
                if (countPlayerTurns == 9) {
                    gameIsOver = true;
                    System.out.println("Draw");
                }

            } while (!gameIsOver);
        } else {
            if (players == 1 && computers == 2) {
                do {
                    if (playerTurn == 1) {
                        String location = player1.prompt(1);
                        while(!player1.isEmptyLocation(gameBoard, location)){
                            System.out.println("Not an empty position, please try again.");
                            location = player1.prompt(1);
                        }
                        int firstIndex = Integer.parseInt(location.substring(0, 1));
                        int secondIndex = Integer.parseInt(location.substring(1));
                        gameBoard[firstIndex][secondIndex] = player1Char;
                        if(player1.pvpWinningMove(gameBoard, player1Char, location)){
                            gameIsOver = true;
                            draw();
                            System.out.println("Player 1 wins!");
                        }else{
                            gameBoard[firstIndex][secondIndex] = player1Char;
                            draw();
                        }



                        playerTurn = 2;
                        countPlayerTurns++;
                        System.out.println();
                    } else if (playerTurn == 2) {
                        String wM = cpu1.winningMove(gameBoard, player2Char);
                        String oWM = player1.winningMove(gameBoard, player1Char);
                        if (wM.length() == 2) {
                            int firstIndex = Integer.parseInt(wM.substring(0, 1));
                            int secondIndex = Integer.parseInt(wM.substring(1));
                            gameBoard[firstIndex][secondIndex] = player2Char;
                            gameIsOver = true;
                            draw();
                            System.out.println("Player 2 chooses position " + cpu1.getPosition(wM) + "!");
                            System.out.println("Player " + playerTurn + " wins!");
                        } else if (oWM.length() == 2) {
                            int firstIndex = Integer.parseInt(oWM.substring(0, 1));
                            int secondIndex = Integer.parseInt(oWM.substring(1));
                            gameBoard[firstIndex][secondIndex] = player2Char;
                            draw();
                            System.out.println("Player 2 chooses position " + cpu1.getPosition(oWM) + "!");
                        } else if (cpu1.isCenterAvailable(gameBoard)) {
                            gameBoard[1][1] = player2Char;
                            draw();
                            System.out.println("Player 2 chooses position 5!");
                        } else {
                            String randLocation = cpu1.getRandomLocation(gameBoard);
                            int firstIndex = Integer.parseInt(randLocation.substring(0, 1));
                            int secondIndex = Integer.parseInt(randLocation.substring(1));
                            gameBoard[firstIndex][secondIndex] = player2Char;
                            System.out.println("Player 2 chooses position " + cpu1.getPosition(randLocation) + "!");
                        }

                        System.out.println();
                        countPlayerTurns++;
                        playerTurn = 1;
                    }

                    if (countPlayerTurns == 9) {
                        gameIsOver = true;
                        System.out.println("Draw");
                    }


                } while (!gameIsOver);
            } else if (computers == 1 && players == 2) {
                do {
                    if (playerTurn == 1) {
                        String wM = cpu1.winningMove(gameBoard, player1Char);
                        String oWM = player1.winningMove(gameBoard, player2Char);
                        if (wM.length() == 2) {
                            int firstIndex = Integer.parseInt(wM.substring(0, 1));
                            int secondIndex = Integer.parseInt(wM.substring(1));
                            gameBoard[firstIndex][secondIndex] = player1Char;
                            gameIsOver = true;
                            draw();
                            System.out.println("Player 1 chooses position " + cpu1.getPosition(wM) + "!");
                            System.out.println("Player " + playerTurn + " wins!");
                        } else if (oWM.length() == 2) {
                            int firstIndex = Integer.parseInt(oWM.substring(0, 1));
                            int secondIndex = Integer.parseInt(oWM.substring(1));
                            gameBoard[firstIndex][secondIndex] = player1Char;
                            draw();
                            System.out.println("Player 1 chooses position " + cpu1.getPosition(oWM) + "!");
                        } else if (cpu1.isCenterAvailable(gameBoard)) {
                            gameBoard[1][1] = player1Char;
                            draw();
                            System.out.println("Player 1 chooses position 5!");
                        } else {
                            String randLocation = cpu1.getRandomLocation(gameBoard);
                            int firstIndex = Integer.parseInt(randLocation.substring(0, 1));
                            int secondIndex = Integer.parseInt(randLocation.substring(1));
                            gameBoard[firstIndex][secondIndex] = player1Char;
                            draw();
                            System.out.println("Player 1 chooses position " + cpu1.getPosition(randLocation) + "!");
                        }
                        System.out.println();
                        countPlayerTurns++;
                        playerTurn = 2;
                    } else if (playerTurn == 2) {
                        String location = player1.prompt(2);
                        while(!player1.isEmptyLocation(gameBoard, location)){
                            System.out.println("Not an empty position, please try again.");
                            location = player1.prompt(2);
                        }
                        int firstIndex = Integer.parseInt(location.substring(0, 1));
                        int secondIndex = Integer.parseInt(location.substring(1));
                        gameBoard[firstIndex][secondIndex] = player2Char;
                        if(player1.pvpWinningMove(gameBoard, player2Char, location)){
                            gameIsOver = true;
                            draw();
                            System.out.println("Player 1 wins!");
                        }else{
                            gameBoard[firstIndex][secondIndex] = player2Char;
                            draw();
                        }


                        playerTurn = 1;
                        countPlayerTurns++;
                        System.out.println();
                    }

                    if (countPlayerTurns == 9) {
                        gameIsOver = true;
                        System.out.println("Draw");
                    }
                } while (!gameIsOver);

            }
        }
    }





    void draw(){
        System.out.println("Game Board: Positions:");

        int p = 1;

        for( int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++){
                System.out.print(" " + gameBoard[i][j] + " ");
                if(j < 2)
                    System.out.print("|");
            }

            System.out.print(" ");

            for(int j = 0; j < 3; j++) {
                System.out.print(" " + p + " ");
                p++;

                if(j < 2)
                    System.out.print("|");
            }

            System.out.println();

            if(i < 2){
                System.out.print("-----------");
                System.out.print(" ");
                System.out.println("-----------");
            }
        }
    }

    char player1Char;
    char player2Char;
    int countPlayerTurns;
    boolean gameIsOver;
    char[][] gameBoard;
    int playerTurn;
    int players;
    int computers;
    Computer cpu1;
    Computer cpu2;
    Player player1;
    Player player2;
}
